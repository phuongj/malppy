<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');


if ( ! function_exists('generate_image'))
{
    function generate_image($img, $width, $height){

        $obj =& get_instance();

        $obj->load->library('image_lib');
        $obj->load->helper('url');

        $config['image_library'] = 'gd2';

        $config['source_image'] = $img;
        $config['new_image'] = 'tmp_img.jpg' ;
        $config['width'] = $width;
        $config['height'] = $height;

        $obj->image_lib->initialize($config);

        $obj->image_lib->resize();


        return $config['new_image'];
    }
}
