<?php 
session_start();
$additional_header_info = '<style type="text/css">#page_title {text-align:center;}</style>';
include('header.php'); ?>
<?php
if (isset($_SESSION['id'])) {
	// Redirection to login page twitter or facebook
	//header("location: home.php");
	//Replace with codeigniter authentication
	if ($_GET['login'] == 'twitter') {
		header("Location: ".base_url()."oauth/login-twitter.php");
	}
}
if (array_key_exists("login", $_GET)) {
	$oauth_provider = $_GET['oauth_provider'];
	if ($oauth_provider == 'twitter') {
		header("Location: ".base_url()."oauth/login-twitter.php");
	}
}
?>

<?php
	if ($this->session->flashdata('message'))
	{
		//echo '<div class="gmessage">'.$this->session->flashdata('message').'</div>';
		?>
		<script>
		alert('<?php echo $this->session->flashdata('message');?>');
		</script>
		<?php
	}
	if ($this->session->flashdata('error'))
	{
		//echo '<div class="error">'.$this->session->flashdata('error').'</div>';
		?>
		<script>
		alert('<?php echo $this->session->flashdata('error');?>');
		</script>
		<?php
	}
	if (!empty($error))
	{
		//echo '<div class="error">'.$error.'</div>';
		?>
		<script>
		alert('<?php echo $error;?>');
		</script>
		<?php
	}
?>

<div class="main_wrap">

	<div id="static_container">
    	<div id="static_first_portion">
        
        
        <div id="login_wrap">          
		
			<?php if (!empty($error)) { ?>
				<div id="errmsg">
				<?php
				if ($this->session->flashdata('message'))
				{
					echo '<div class="gmessage">'.$this->session->flashdata('message').'</div>';
				}
				if ($this->session->flashdata('error'))
				{
					echo '<div class="error">'.$this->session->flashdata('error').'</div>';
				}
				if (!empty($error))
				{
					echo '<div class="error">'.$error.'</div>';
				}
				?>
				</div>
				<?php } ?>
		
        	<div id="login">
        		
        		<?php echo form_open('secure/login') ?>				<input type="hidden" value="<?php echo $redirect; ?>" name="redirect"/>						<input type="hidden" value="submitted" name="submitted"/>  				<div id="reg_customer">                	<h1>Registered Customers</h1>                    <p>If you have an account with us, please log in.</p>                  <input type="text" name="email" value="Email address" onfocus="if(this.value == 'Email address') {this.value = '';}" onblur="if (this.value == '') {this.value = 'Email address';}" />                    <input type="password"name="password" value="" />                    <div id="forgot_pas"><a href="<?php echo site_url('secure/forgot_password'); ?>">Forgot Your Password?</a></div>                    <!-- <a class="alignright" href="#"><img src="<?php echo base_url();?>images/login.png" width="70" height="30" alt="login" /></a> -->					<input id="login_btn" name="submit" type="submit" value="<?php echo lang('form_login');?>" />                </form> 
					<div class="clear"></div>
					<div style="height:5px;"></div>
					<!-- <p style="float:left;">Or log in with </p> -->
					<div style="float:left;margin-left:15px;">
					<!-- <?php echo form_open('secure/login_facebook', array('style'=>'float:left;margin-right:10px;')) ?>
						<input type="hidden" value="<?php echo $redirect; ?>" name="redirect"/>
						<input type="hidden" value="submitted" name="submitted"/>
						<input type="hidden" value="facebook" name="social"/>
						<input type="submit" value="" name="submit" style="background : url('<?php echo base_url('images/ico_f.gif'); ?>');width:32px; height: 32px; cursor:pointer;"/>
					</form> -->
					<!-- <?php echo form_open('secure/login_twitter', array('style'=>'float:left')) ?>
						<input type="hidden" value="<?php echo $redirect; ?>" name="redirect"/>
						<input type="hidden" value="submitted" name="submitted"/>
						<input type="hidden" value="twitter" name="social"/>
						<input type="submit" value="" name="submit" style="background : url('<?php echo base_url('images/ico_t.gif'); ?>');width:32px; height: 32px; cursor:pointer;"/>
					</form> -->
					<!-- <a href="?login&oauth_provider=facebook"><img src="<?php echo base_url();?>images/ico_f.gif" border="0" alt=""></a>&nbsp;-->
					<!-- <a href="?login&oauth_provider=twitter"><img src="<?php echo base_url();?>images/ico_t.gif" border="0" alt=""></a>  -->
					</div>
					<div class="clear"></div>
				</div>   
				
				<div id="login_customer">
                	<h1>New Customers</h1>
                    <p>By creating an account with our store, you will be able to move through the checkout process faster, store multiple shipping addresses, view and track your orders in your account and more.</p>
                    
					<a href="<?php echo base_url();?>secure/register"><img src="<?php echo base_url();?>images/create_account.png" width="136" height="30" alt="btn" /></a>
               
			   	<?php echo form_open('secure/login') ?>
				<input type="hidden" value="<?php echo $redirect; ?>" name="redirect"/>
						<input type="hidden" value="submitted" name="submitted"/>
  				  
				             

				              
                

        	</div><!-- End of static -->  
        </div><!-- End of static_wrap -->
	</div><!-- End of First Portion -->
    
    
    
		<div class="clear"></div>
     </div><!-- End of Container -->	
	 
</div> 
	 
	<!-- <div class="container">

	<div id="login_container_wrap">
		<div id="login_container">
		
			<?php echo form_open('secure/login') ?>
				<table>
					<tr>
						<td><?php echo lang('email');?></td>
						<td><input type="text" name="email" class="gc_login_input"/></td>
					</tr>
					<tr>
						<td><?php echo lang('password');?></td>
						<td><input type="password" name="password" class="gc_login_input"/></td>
					</tr>
				</table>
				<div class="center">
						<input name="remember" value="true" type="checkbox" /> <?php echo lang('keep_me_logged_in');?><br/>
						<input type="hidden" value="<?php echo $redirect; ?>" name="redirect"/>
						<input type="hidden" value="submitted" name="submitted"/>
						<input type="submit" value="<?php echo lang('form_login');?>" name="submit" class="gc_login_button"/>
				</div>
			</form>
		
			<div id="login_form_links">
				<a href="<?php echo site_url('secure/forgot_password'); ?>"><?php echo lang('forgot_password')?></a> | <a href="<?php echo site_url('secure/register'); ?>"><?php echo lang('register');?></a>
			</div>
		</div>
	</div>

	</div> -->
</div>
<script language="Javascript">
 
jQuery(document).ready(function() {
 
    // cache references to the input elements into variables
    var passwordField = jQuery('input[name=password]');
    
    // add a password placeholder field to the html
    passwordField.after('<input id="passwordPlaceholder" type="text" value="Password" autocomplete="off" />');
    var passwordPlaceholder = jQuery('#passwordPlaceholder');
 
    // show the placeholder with the prompt text and hide the actual password field
    passwordPlaceholder.show();
    passwordField.hide();

    // when focus is placed on the placeholder hide the placeholder and show the actual password field
    passwordPlaceholder.focus(function() {
        passwordPlaceholder.hide();
        passwordField.show();
        passwordField.focus();
    });
    // and vice versa: hide the actual password field if no password has yet been entered
    passwordField.blur(function() {
        if(passwordField.val() == '') {
            passwordPlaceholder.show();
            passwordField.hide();
        }
    });
});
 
</script>
<?php include('footer.php'); ?>