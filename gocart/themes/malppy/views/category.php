<?php include('header.php'); ?>
<style>
.top_border {
    border-bottom: 1px solid #D5D5D5;
    margin: 0px 20px 20px;
}
#breadcrumb {
    margin: 0 20px;
    padding-bottom: 5px;
    padding-top: 10px;
}
#cat_container {
    margin-top: -31px;
	width: 999px;
}

#shop_by {
    float: left;
    padding-top: 0;
    width: 180px;
}

#slider img {
    margin-top: 0;
}
.right_main_pics {
    float: right;
    margin-right: 19px;
    margin-top: -7px;
}
</style>
	<div class="main_wrap">
	<div id="cat_container">
		<div id="breadcrumb">
            <ul style="float:left;">
				<li><a href="<?php echo base_url();?>">Home</a></li>
            	<li class="last"><a href="<?php echo $this->Category_model->get_category($product->categories[0])->slug; ?>" style="color:#006699"><?php echo $category->name; ?></a></li>
        	</ul>

			<span style="float:right;"><img src="<?php echo base_url();?>images/header_fb.png" width="100" height="25" alt="fb" /></span>
        	<div class="clear"></div>
        	<div class="category_name"><?php echo $category->name; ?></div>
        </div><!-- End of breadcrumb --> 

		<div class="top_border"></div>

    	<div id="cat_first_portion">
		<div id="shop_by">
			<?php
		function display_sidecategories($cats, $layer, $first='',$exclude='')
		{
			if($first)
			{
				echo '<ul '.$first.'>';
			}
			
			foreach ($cats as $cat)							
			{				//print_r($cat['category']->name);
								echo '<li><a href="'.site_url($cat['category']->slug).'">'.$cat['category']->name.'</a></li>';
				//echo '<h1><a href="'.site_url($cat['category']->slug).'">'.$cat['category']->name.'</a>'."\n";
				//echo '</h1>';
				
				//echo '<ul class="side"><li><a href="'.site_url(sales).'" style="color:#CC0000;font-weight:bold">SALE - Last Chance</a></li></ul>';

				
				//echo '<div class="leftsidebar_border"></div>';
			}
			if($first)
			{
				echo '</ul>';
			}	
		}
			
		display_sidecategories($this->categories[$category->id]['children'], 1,'class="side"',$category->id); ?>
            
            
      </div><!-- End of shop_by -->
        
        <div id="slider_wrap" style="width:550px;float:left;">
        	<div id="slconlinks_wrap">
         		<div id="slconlinks_controler" style="margin: 270px 0 0 30px;">
        			<div id="slconlinks"></div>
	            </div><!-- End of slconlinks_controler -->
    	    </div><!-- End of slconlinks_wrap -->
        
        	<div id="slider">
				
                            	<?php 
	$banner_count	= 1;
	foreach ($banners as $banner)
	{
		//echo '<div class="banner_container">';
		
		if($banner->link != '')
		{
			$target	= false;
			if($banner->new_window)
			{
				$target = 'target="_blank"';
			}
			echo '<a href="'.$banner->link.'" '.$target.' >';
		}
		
		echo '<img class="banners_img'.$banner_count.'" src="'.base_url().'phpthumb/phpThumb.php?src='.base_url('uploads/'.$banner->image).'&w=550&h=360&zc=1" />';
		
		
		if($banner->link != '')
		{
			echo '</a>';
		}

		//echo '</div>';

		$banner_count++;
	}
	?>
        	</div><!-- End of slider -->  
        </div><!-- End of slider_wrap -->
		<div class="right_main_pics">
				<?php 
				$smallbanner_count	= 1;
				foreach ($smallbanners as $banner)
				{
					if ($smallbanner_count>2) break;
					echo '<div class="small_pic">';
		
					if($banner->link != '')
					{
						$target	= false;
						if($banner->new_window)
						{
							$target = 'target="_blank"';
						}
						echo '<a href="'.$banner->link.'" '.$target.' >';
					}
					echo '<img src="'.base_url().'phpthumb/phpThumb.php?src='.base_url('uploads/'.$banner->image).'&w=240&h=120&zc=1" width="240" height="120" alt="" border="0" />';
	
					if($banner->link != '')
					{
						echo '</a>';
					}
	
					echo '</div>';
	
					$banner_count++;
				}
				?> 
                  <!-- <div class="small_pic"><a href="#"><img src="images/item_small_protion1.jpg" width="240" height="140" alt="" border="0" /></a></div>
                  <div class="small_pic"><a href="#"><img src="images/item_small_protion2.jpg" width="240" height="140" alt="" border="0" /></a></div>
                  <div class="small_pic"><a href="#"><img src="images/item_small_protion2.jpg" width="240" height="140" alt="" border="0" /></a></div> -->
         </div>
        
        
        <div class="clear"></div>
        
        
        <div class="category_new_arrival_wrap" >
    <div class="category_new_arrival" style="position:absolute;">
             <div id="category_new_arival_top">
				<p>New Arrivals</p>
           	</div><!-- End of new_arival_top -->
            
        <div id="category_new_product_wrap">
            <div id="products_list_portion"  style="padding:10px 20px;margin:0 auto;text-align:center;width:100%;"> <!-- Products -->
	
		<?php if(count($products) > 0):?>
		<div class="clear"></div>
		
		<?php		
		$cat_counter = 1;
		foreach($products as $product):
			if($cat_counter == 1):
			?>
			
			<div class="category_container">
			
			<?php endif;?>
			
			<div class="product_box">
				<div class="thumbnail">
					<?php
					$photo	= '<img src="'.base_url('images/nopicture.png').'" alt="'.lang('no_image_available').'"/>';
					$product->images	= array_values($product->images);
						
					if(!empty($product->images[0]))
					{
						$primary	= $product->images[0];
						foreach($product->images as $photo)
						{
							if(isset($photo->primary))
							{
								$primary	= $photo;
							}
						}
						if (strpos($primary->filename, 'http') === 0) {
						$photo	= '<img src="'.base_url().'phpthumb/phpThumb.php?src='.$primary->filename.'&w=170&h=220&zc=1&f=png" width="170" height="220" alt="'.$product->seo_title.'"/>';
						} else {
						$photo	= '<img src="'.base_url().'phpthumb/phpThumb.php?src='.base_url('uploads/images/small/'.$primary->filename).'&w=170&h=220&zc=1&f=png" width="170" height="220" alt="'.$product->seo_title.'"/>';
						}

						//$photo	= '<img src="'.base_url('uploads/images/thumbnails/'.$primary->filename).'" alt="'.$product->seo_title.'"/>';
					}
					?>
					<a href="<?php echo site_url($product->slug); ?>">
						<?php echo $photo; ?>
					</a>
				</div>
				<div class="product_name">
					<a href="<?php echo site_url($product->slug); ?>"><?php echo $product->name;?></a>
				</div>
				
				<div>
				<div class="price_container">
					<?php if($product->promoprice > 0):?>
						<span class="price_slash"><?php echo format_currency($product->saleprice); ?></span>
						<span class="price_sale">Now <?php echo format_currency($product->promoprice); ?></span>
					<?php else: ?>
						<span class="price_reg"><?php echo format_currency($product->saleprice); ?></span>
					<?php endif; ?>
				</div>
                    
				</div>
			</div>
			
			<?php 
			$cat_counter++;
			if($cat_counter == 5):?>

			</div>

			<?php 
			$cat_counter = 1;
			endif;
		endforeach;
			
		if($cat_counter != 1):?>
				<br class="clear"/>
			</div>
		<?php endif;?>
		
		
	<?php endif; ?>

		</div>
        </div>  <!-- End of new_product_wrap -->  	
    </div><!-- End of new_arrival -->
</div><!-- End of new_arrival_wap -->
        
	</div><!-- End of First Portion -->

<div class="clear"></div>


 
	
	<div style="height:80px;"></div>
  <div class="clear"></div>
  
  
     </div><!-- End of Portion -->	
  <div class="clear"></div>
  <div id="sale_wrap" style="margin-bottom:10px;">
  	<div id="sale" style="position:absolute;">
   	  	<a href="<?php echo base_url();?>sales"><img src="<?php echo base_url();?>images/sale.png" width="1030" height="120" alt="sale" /></a>
      </div><!-- End of Sales -->	
  </div>  <!-- End of sale_wrap -->	
</div><!-- End of Main Wrapper -->
		
<?php include('footer.php'); ?>