<?php
$additional_header_info = '<style type="text/css">#gc_page_title {text-align:center;}</style>';
include('header.php'); ?>
<?php
$company	= array('id'=>'bill_company', 'class'=>'bill input', 'name'=>'company', 'value'=> set_value('company'));
$first		= array('id'=>'bill_firstname', 'class'=>'bill input bill_req', 'name'=>'firstname', 'value'=> set_value('firstname'));
$last		= array('id'=>'bill_lastname', 'class'=>'bill input bill_req', 'name'=>'lastname', 'value'=> set_value('lastname'));
$email		= array('id'=>'bill_email', 'class'=>'bill input bill_req', 'name'=>'email', 'value'=>set_value('email'));
$phone		= array('id'=>'bill_phone', 'class'=>'bill input bill_req', 'name'=>'phone', 'value'=> set_value('phone'));
?>
<link rel="stylesheet" href="<?php echo base_url();?>css/dropkick.css" type="text/css"/>
<script src="<?php echo base_url();?>js/jquery.dropkick-1.0.0.js" type="text/javascript"></script>
<link type="text/css" href="http://projects2.boxedge.com/malppy/js/jquery/theme/smoothness/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
  <script type="text/javascript">
  var slct_box = jQuery.noConflict();
   slct_box(document).ready(function() {
      slct_box('#gender').dropkick();
    });
  </script>
<style>
#ui-datepicker-div .ui-datepicker-month, #ui-datepicker-div .ui-datepicker-year {
	top:0px;
	visibility:visible;
}
</style>
<div class="main_wrap">

<div id="static_container">
    	<div id="static_first_portion">
			<div id="static_left">
				<h1>Login or Create an Account</h1>
			</div><!-- End of static_left -->
			
				
        
        <div id="reg_wrap">          
        	<div id="registration">
				
				<?php if (!empty($error)) { ?>
				<div id="errmsg">
				<?php
				if ($this->session->flashdata('message'))
				{
					echo '<div class="gmessage">'.$this->session->flashdata('message').'</div>';
				}
				if ($this->session->flashdata('error'))
				{
					echo '<div class="error">'.$this->session->flashdata('error').'</div>';
				}
				if (!empty($error))
				{
					echo '<div class="error">'.$error.'</div>';
				}
				?>
				</div>
				<?php } ?>
			
  				<div id="reg">
				<?php echo form_open('secure/register'); ?>
				<input type="hidden" name="submitted" value="submitted" />
				<input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
                	<h1>Register a new account</h1>
                    
					<div class="form_label"> <?php echo lang('address_firstname');?> *</div>
					<div class="form_input">
						<?php echo form_input($first);?>
					</div>	
                   	
					<div class="clear"></div>
					<div class="form_label"><?php echo lang('address_lastname');?> *</div>
					<div class="form_input">
						<?php echo form_input($last);?>
					</div>
					
					<div class="clear"></div>
					<div class="form_label">Email *</div>
					<div class="form_input">
						<?php echo form_input($email);?>
					</div>
					
					
					<div class="clear"></div>
					<div class="form_label">Gender</div>
					<div class="form_input">
						<select name="gender" id="gender">
                                  <option value="">Select</option>
                                  <option value="Male">Male</option>
                                  <option value="Female">Female</option>
                                </select>
					</div>	
					
					
					<div class="clear"></div>
					<div class="form_label_birth">Birthday</div>
					<div class="form_input">
						<!-- <div class="birth_col">Day<input type="text" value="" /></div>
						<div class="birth_col">Month<input type="text" value="" /></div>
						<div class="birth_col">Year<input type="text" value="" /></div> -->
						<div class="birth_col"><input type="text" value="" id="birth_col" /></div>
					</div>	

					<div class="clear"></div>
					<div class="form_label">Password *</div>
					<div class="form_input">
						<input type="password" name="password" value="" />
					</div>
					
					<div class="clear"></div>
					<div class="form_label">Retype Password *</div>
					<div class="form_input">
						<input type="password" name="confirm" value="" />
					</div>				
					
					<div class="clear"></div>
					
					<div id="form_submit">
						<p>* Required fields</p>
						<!-- <input type="submit" value="" /> -->
						<input id="register" type="submit" value="" />
						
					</div>
					<br><br>
					<div id="login_form_links" class="clear">
					<a href="<?php echo site_url('secure/login'); ?>">Already registered? Login here.</a>
					</div>
					
                    
                </div>                                  

                    
                              

        	</div><!-- End of login -->  
        </div><!-- End of login_wrap -->
		
		
		
		
	</div><!-- End of First Portion -->
    
    
    
		<div class="clear"></div>
     </div><!-- End of Container -->	
  <div class="clear"></div>
 <div id="banner_wrap">
<div style="position:absolute;" id="banner">
	<div id="free_shipping">
    	<div class="banner_image">
        	<a href="#"><img width="40" height="40" alt="free shipping" src="<?php echo base_url();?>images/free_ship.png"/></a>
        </div>
        <div class="banner_link">
        	<a href="#">Free Shipping </a>
        </div>
  </div><!-- End of free_shipping -->
	<div id="free_return">
    	<div class="banner_image">
        	<a href="#"><img width="40" height="40" alt="free shipping" src="<?php echo base_url();?>images/free_return.png"/></a>
        </div>
        <div class="banner_link">
        	<a href="#">Free Returns </a>
        </div>
  </div><!-- End of free_shipping -->  
  <div id="customer_suport">
    	<div class="banner_image">
        	<a href="#"><img width="40" height="40" alt="free shipping" src="<?php echo base_url();?>images/customer_suport.png"/></a>
        </div>
        <div class="banner_link">
        	<a href="#">24/7 Customer Service   1-800-333-001 </a>
        </div>
  </div><!-- End of free_shipping --> 
</div><!-- End of banner -->
</div> 

<!-- <div class="container">
<div id="login_container_wrap">
	<div id="login_container">
	<?php echo form_open('secure/register'); ?>
		<input type="hidden" name="submitted" value="submitted" />
		<input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />

		<div class="form_wrap">
			<div>
				<?php echo lang('address_company');?><br/>
				<?php echo form_input($company);?>
			</div>
			<div>
				<?php echo lang('address_firstname');?><b class="r"> *</b><br/>
				<?php echo form_input($first);?>
			</div>
			<div>
				<?php echo lang('address_lastname');?><b class="r"> *</b><br/>
				<?php echo form_input($last);?>
			</div>
		</div>
	
		<div class="form_wrap">
			<div>
				<?php echo lang('address_email');?><b class="r"> *</b><br/>
				<?php echo form_input($email);?>
			</div>
			<div>
				<?php echo lang('address_phone');?><b class="r"> *</b><br/>
				<?php echo form_input($phone);?>
			</div>
		</div>
		<div class="form_wrap">
			<input type="checkbox" name="email_subscribe" value="1" <?php echo set_radio('email_subscribe', '1', TRUE); ?>/> <?php echo lang('account_newsletter_subscribe');?>
		</div>
		<div class="form_wrap">
			<div>
				<?php echo lang('account_password');?><b class="r"> *</b><br/>
				<input type="password" name="password" value="" />
			</div>
			<div>
				<?php echo lang('account_confirm');?><b class="r"> *</b><br/>
				<input type="password" name="confirm" value="" />
			</div>
		</div>
	
		<div class="form_wrap">
			<input id="register" type="submit" value="<?php echo lang('form_register');?>" />
		</div>
	</form>
	
	<div id="login_form_links">
		<a href="<?php echo site_url('secure/login'); ?>"><?php echo lang('go_to_login');?></a>
	</div>
	
	</div>
</div>
</div>
</div> -->
<script type="text/javascript">

slct_box(document).ready(function(){
	slct_box('#birth_col').datepicker({ dateFormat: 'mm-dd-yy', changeMonth: true, changeYear: true, yearRange: '-100y:c+nn',
            maxDate: '-1d', altFormat: 'yy-mm-dd' });
});

</script>
<?php include('footer.php');