<?php include('header.php'); ?>
<style>
.top_border {
    border-bottom: 1px solid #D5D5D5;
    margin: 10px 20px 30px;
}
#breadcrumb {
    margin: 0 30px;
    padding-bottom: 5px;
    padding-top: 10px;
}
#cat_container {
    margin-top: -31px;
	width: 999px;
}
</style>
	<div class="main_wrap">
	<div id="cat_container">
		<div id="breadcrumb">
            <ul style="float:left;">
				<li><a href="<?php echo base_url();?>">Home</a></li>
            	<li class="last"><a href="<?php echo $this->Category_model->get_category($product->categories[0])->slug; ?>"><?php echo $category->name; ?></a></li>
            </ul>

			<span style="float:right;"><img src="<?php echo base_url();?>images/header_fb.png" width="100" height="25" alt="fb" /></span>
        	<div class="clear"></div>
		</div><!-- End of breadcrumb --> 

		<div class="top_border"></div>

		<div id="slider" style="margin:0 auto;text-align:center;width:100%;height:auto;">				
            <?php 
			$banner_count	= 1;
			foreach ($banners as $banner)
			{
				if($banner->link != '')
				{
					$target	= false;
					if($banner->new_window)
					{
						$target = 'target="_blank"';
					}
					echo '<a href="'.$banner->link.'" '.$target.' >';
				}
				echo '<img class="banners_img'.$banner_count.'" src="'.base_url().'phpthumb/phpThumb.php?src='.base_url('uploads/'.$banner->image).'&w=928&zc=1" />';
				
				if($banner->link != '')
				{
					echo '</a>';
				}

				$banner_count++;
			}
			?>
        </div><!-- End of slider -->  

    	<div id="sale_main_menu">
			<ul class="nav_menu">
				<?php
					foreach ($this->categories as $cat)
					{
						if ($cat['category']->name == $category->name) continue;
						echo '<li><a href="'.site_url($cat['category']->slug).'">'.$cat['category']->name.'</a>'."</li>";
					}
				?>
        	</ul>
		</div>

		<div id="products_list_portion"  style="padding:10px 20px;margin:0 auto;text-align:center;width:100%;"> <!-- Products -->
	
		<?php if(count($products) > 0):?>
		<div class="clear"></div>
		<div class="pagination">
		<?php echo $this->pagination->create_links();?>
		</div>
		<?php		
		$cat_counter = 1;
		foreach($products as $product):
			if($cat_counter == 1):
			?>
			
			<div class="category_container">
			
			<?php endif;?>
			
			<div class="product_box">
				<div class="thumbnail">
					<?php
					$photo	= '<img src="'.base_url('images/nopicture.png').'" alt="'.lang('no_image_available').'"/>';
					$product->images	= array_values($product->images);
						
					if(!empty($product->images[0]))
					{
						$primary	= $product->images[0];
						foreach($product->images as $photo)
						{
							if(isset($photo->primary))
							{
								$primary	= $photo;
							}
						}
						if (strpos($primary->filename, 'http') === 0) {
						$photo	= '<img src="'.base_url().'phpthumb/phpThumb.php?src='.$primary->filename.'&w=170&h=220&zc=1&f=png" width="170" height="220" alt="'.$product->seo_title.'"/>';
						} else {
						$photo	= '<img src="'.base_url().'phpthumb/phpThumb.php?src='.base_url('uploads/images/small/'.$primary->filename).'&w=170&h=220&zc=1&f=png" width="170" height="220" alt="'.$product->seo_title.'"/>';
						}

						//$photo	= '<img src="'.base_url('uploads/images/thumbnails/'.$primary->filename).'" alt="'.$product->seo_title.'"/>';
					}
					?>
					<a href="<?php echo site_url($product->slug); ?>">
						<?php echo $photo; ?>
					</a>
				</div>
				<div class="product_name">
					<a href="<?php echo site_url($product->slug); ?>"><?php echo strip_tags($product->name);?></a>
				</div>
				
				<div>
				<div class="price_container">
					<?php if($product->promoprice > 0):?>
						<span class="price_slash"><?php echo format_currency($product->saleprice); ?></span>
						<span class="price_sale">Now <?php echo format_currency($product->promoprice); ?></span>
					<?php else: ?>
						<span class="price_reg"><?php echo format_currency($product->saleprice); ?></span>
					<?php endif; ?>
				</div>
                    
				</div>
			</div>
			
			<?php 
			$cat_counter++;
			if($cat_counter == 5):?>

			</div>

			<?php 
			$cat_counter = 1;
			endif;
		endforeach;
			
		if($cat_counter != 1):?>
				<br class="clear"/>
			</div>
		<?php endif;?>
		
		<div class="gc_pagination">
		<?php echo $this->pagination->create_links();?>
		</div>
	<?php endif; ?>

		</div> <!-- End of Products -->


 
  <div class="clear"></div>
     </div><!-- End of Portion -->	
  <div class="clear"></div>
</div><!-- End of Main Wrapper -->
		
<?php include('footer.php'); ?>