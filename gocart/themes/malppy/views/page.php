<?php include('header.php');?>

<style>
#static_wrap {
    margin-top: -1px;
}
</style>

<div class="main_wrap">
	<div id="static_container">
    	<div id="static_first_portion">
        <div id="static_left">
        	<h1><?php echo $page_title; ?></h1>
            <!-- <ul>                      
               <li><a href="#">Who we are</a></li>
               <li><a href="#">Careers at Malppy</a></li>
               <li><a href="#">Media</a></li>

			</ul> -->        
        
      	</div><!-- End of static_left -->
        
        <div id="static_wrap">          
        	<div id="static">
			
				<?php echo html_entity_decode($page->content); ?>

        	</div><!-- End of static -->  
        </div><!-- End of static_wrap -->
	</div><!-- End of First Portion -->
    
    
    
		<div class="clear"></div>
     </div><!-- End of Container -->	

	 <div class="clear"></div>
 <div id="banner_wrap">
<div id="banner" style="position:absolute;">
	<div id="free_shipping">
    	<div class="banner_image">
        	<a href="<?php echo base_url();?>free-shipping"><img src="<?php echo base_url();?>images/free_ship.png" width="40" height="40" alt="free shipping" /></a>
        </div>
        <div class="banner_link">
        	<a href="<?php echo base_url();?>free-shipping">Free Shipping </a>
        </div>
  </div><!-- End of free_shipping -->
	<div id="free_return">
    	<div class="banner_image">
        	<a href="<?php echo base_url();?>free-returns"><img src="<?php echo base_url();?>images/free_return.png" width="40" height="40" alt="free shipping" /></a>
        </div>
        <div class="banner_link">
        	<a href="<?php echo base_url();?>free-returns">Free Returns </a>
        </div>
  </div><!-- End of free_shipping -->  
  <div id="customer_suport">
    	<div class="banner_image">
        	<a href="<?php echo base_url();?>customer-service-center"><img src="<?php echo base_url();?>images/customer_suport.png" width="40" height="40" alt="free shipping" /></a>
        </div>
        <div class="banner_link">
        	<a href="<?php echo base_url();?>customer-service-center">24/7 Customer Service   1-800-333-001 </a>
        </div>
  </div><!-- End of free_shipping --> 
</div><!-- End of banner -->
	
	

	
</div>


<?php include('footer.php');?>