<?php include('header.php'); ?>
<style>
.top_border {
    border-bottom: 1px solid #D5D5D5;
    margin: 10px 20px 30px;
}
#breadcrumb {
    margin: 0 20px;
    padding-bottom: 5px;
    padding-top: 10px;
}
#single_container {
    margin-top: -31px;
	width: 999px;
}
#single_product_essential_wrap {
    margin-top: 0;
    width: 750px;
	padding:0px;
	float: left;
}
#single_product_essential {
    padding-left: 20px;
}
#single_product_detail {
    padding-left: 15px;
    padding-right: 20px;
}
#single_product_desciption {
    float: left;
    padding-right: 20px;
    width: 360px;
}
#new_recomd ul li {
    float: left;
    list-style: none outside none;
    width: 100px;
}
.flexiwrap {
	height: 250px;
	width: auto;
}
#we_recomd_wrap {
    border-top: none;
    margin: 0 auto;
    padding-bottom: 20px;
    width: 220px;
}
#we_recomd_wrap div#new_recomd div#new_product_wrap div.flexiwrap {
    height: 290px;
    width: 220px;
}
.single_product_carousel_detail {
    line-height: 12px;
    margin-bottom: 20px;
    padding: 0;
    width: 90px;
}
#single_product_detail {
    padding-left: 15px;
    padding-right: 20px;
    width: 680px;
}
#new_product_wrap {
    margin-left: 0;
}
#new_recomd {
    height: auto;
    padding-top: 0;
    width: 990px;
}

#gc_tabs {
border:1px solid #FFF; 
width:100%;
background:#c2c2c2 none; padding:0px;
}
.ui-widget-header {
border:0; background:#FFF none; font-family:Verdana;
border-radius: 0px;
font-size:12px;
}
#gc_tabs .ui-widget-header h2 {
	font-size:12px;
	padding:0px;
}
#gc_tabs .ui-widget-content {
border:1px solid #aaaaaa; 
background:#ffffff none;
padding: 20px 40px;
}
.ui-state-default, .ui-widget-content .ui-state-default {
background:#f6f6f6 none; border:1px solid #cccccc;
}
.ui-state-active, .ui-widget-content .ui-state-active {
background:#ffffff none; border:1px solid #aaaaaa;
}
.ui-tabs .ui-tabs-nav {
    padding: 0.2em 1em 0;
}
.ui-tabs .ui-tabs-nav li a {
	padding: 0.5em 2em;
}
.ui-tabs-panel {
	min-height:100px;
}

</style>
<script type="text/javascript">
jQuery(document).ready(function(){	
	jQuery("#gc_tabs").tabs();
}); 
</script>
<div class="main_wrap">
	<div id="single_container">
    	<div id="single_first_portion">
        
        
        <!-- <div id="single_product_essential_wrap">    -->       
        	<div id="single_product_essential">
            	<div id="breadcrumb">
                	<ul>
                    	<li class="last">Preview</li>
                    </ul>
                </div><!-- End of breadcrumb --> 
                
                <div id="single_product_image_wrap">
                	<div id="single_product_image">
               	    	
                    		
    <div class="mygallery">
	<div class="tn3 album">
	    <ol>

		
	<?php

	$img_counter	= 1;
	if(count($product->images) > 0):?>
		<?php foreach($product->images as $image): 
			if (!isset($image->wanted)) continue;
			if($image != $primary):
		?>
			<li>
			<h4>Hohensalzburg Castle</h4>
		 <div class="tn3 description"><?php echo $image->caption;?></div>
				<?php if (strpos($image->filename, 'http') === 0) { ?>
				<a rel="gallery" href="<?php echo base_url(); ?>phpthumb/phpThumb.php?src=<?php echo $image->filename;?>&w=800" title="<?php echo $image->caption;?>"><img src="<?php echo base_url(); ?>phpthumb/phpThumb.php?src=<?php echo $image->filename;?>&w=310&h=310&far=1" /></a>
				<?php } else { ?>
				<a rel="gallery" href="<?php echo base_url('uploads/images/medium/'.$image->filename);?>" title="<?php echo $image->caption;?>"><img src="<?php echo base_url('uploads/images/thumbnails/'.$image->filename);?>"/></a>
				<?php } ?>
				
			</li>
		<?php endif;
		endforeach;?>


	<?php endif;?>
       
	    </ol>
	</div>

</div>
                        
                    </div><!-- End of single_product_image --> 
                </div><!-- End of single_product_image_wrap --> 
                	<?php echo form_open('cart/add_to_cart');?>
								
                <div id="single_product_desciption">
                    <h1><span class="color333333"><?php echo $product->product_name; ?></span></h1>
                    <h1><span class="colorcc0000"><?php echo format_currency($product->saleprice); ?></span></h1>
                    <p><?php echo $product->excerpt; ?></p>
                     <div class="clear"></div>
							<div id="product_size" style="width:100%">
                          
								
								<?php if(count($options) > 0): ?>
		<div class="product_section">
		<?php	
		foreach($options as $option):
			$required	= '';
			if($option->required)
			{
				$required = ' <span class="red">*</span>';
			}
			?>
			<div class="option_container" style="float:left;margin-right:20px;">
				<h2><?php echo $option->name.$required;?></h2>
				<?php
				/*
				this is where we generate the options and either use default values, or previously posted variables
				that we either returned for errors, or in some other releases of Go Cart the user may be editing
				and entry in their cart.
				*/
						
				//if we're dealing with a textfield or text area, grab the option value and store it in value
				if($option->type == 'checklist')
				{
					$value	= array();
					if($posted_options && isset($posted_options[$option->id]))
					{
						$value	= $posted_options[$option->id];
					}
				}
				else
				{
					$value	= $option->values[0]->value;
					if($posted_options && isset($posted_options[$option->id]))
					{
						$value	= $posted_options[$option->id];
					}
				}
						
				if($option->type == 'textfield'):?>
				
					<input type="textfield" id="input_<?php echo $option->id;?>" name="option[<?php echo $option->id;?>]" value="<?php echo $value;?>" />
				
				<?php elseif($option->type == 'textarea'):?>
					
					<textarea id="input_<?php echo $option->id;?>" name="option[<?php echo $option->id;?>]"><?php echo $value;?></textarea>
				
				<?php elseif($option->type == 'droplist'):?>
					<select name="option[<?php echo $option->id;?>]">
						<option value=""><?php echo lang('choose_option');?></option>
				
					<?php foreach ($option->values as $values):
						$selected	= '';
						if($value == $values->id)
						{
							$selected	= ' selected="selected"';
						}?>
						
						<option<?php echo $selected;?> value="<?php echo $values->id;?>">
							<?php echo($values->price != 0)?'('.format_currency($values->price).') ':''; echo $values->name;?>
						</option>
						
					<?php endforeach;?>
					</select>
					<div class="clear" style="height:10px;"></div>
				<?php elseif($option->type == 'radiolist'):
						foreach ($option->values as $values):

							$checked = '';
							if($value == $values->id)
							{
								$checked = ' checked="checked"';
							}?>
							
							<div>
							<input<?php echo $checked;?> type="radio" name="option[<?php echo $option->id;?>]" value="<?php echo $values->id;?>"/>
							<?php echo($values->price != 0)?'('.format_currency($values->price).') ':''; echo $values->name;?>
							</div>
						<?php endforeach;?>
				
				<?php elseif($option->type == 'checklist'):
					foreach ($option->values as $values):

						$checked = '';
						if(in_array($values->id, $value))
						{
							$checked = ' checked="checked"';
						}?>
						<div class="gc_option_list">
						<input<?php echo $checked;?> type="checkbox" name="option[<?php echo $option->id;?>][]" value="<?php echo $values->id;?>"/>
						<?php echo($values->price != 0)?'('.format_currency($values->price).') ':''; echo $values->name;?>
						</div>
					<?php endforeach ?>
				<?php endif;?>
				</div>
		<?php endforeach;?>
	</div>
	<?php endif; ?>		
								
						</div>

						<div class="clear"></div>

                        <div id="product_size">
                           <!--  <h2>Choose your size</h2>                   
                                 <select name="country" id="size">
                                  <option value="">Select you size</option>
                                  <option value="AU">8</option>
                                  <option value="CA">9</option>
                                  <option value="DE">10</option>
                                </select> -->
								
								
								<input type="hidden" name="cartkey" value="<?php echo $this->session->flashdata('cartkey');?>" />
								<input type="hidden" name="id" value="<?php echo $product->id?>"/>
								
								</form>
								
                            
                        </div>            
                </div><!-- End of single_product_detail --> 
                
                <div class="clear"></div>
                
              <div id="single_product_detail">

				<div id="gc_tabs">
					<ul>
						<li><a href="#gc_info"><h2><span class="color006699">INFO & CARE</span></h2></a></li>
						<li><a href="#gc_specification"><h2><span class="color006699">SPECIFICATION</span></h2></a></li>
					</ul>

					<div id="gc_info">
						<?php echo $product->info; ?>				
					</div>
					<div id="gc_specification">
						<?php echo $product->specification; ?>				
					</div>
				</div>
				
				


				<p>&nbsp;</p>



                <h2><span class="color006699">PRODUCTS DETAILS</span></h2>
				<?php if ($product->description==$product->original_description) $product->description = ""; ?>
               	<?php echo $product->description; ?>

				<p><br><br></p>
                
                </div><!-- End of single_product_detail -->
        	</div><!-- End of single_product_essential -->  
        <!-- </div> --><!-- End of single_product_essential_wrap -->
	</div><!-- End of First Portion -->
		<div class="clear"></div>
     </div><!-- End of Container -->	
  <div class="clear"></div>
</div><!-- End of Main Wrapper -->
<div class="clear"></div>

<div id="we_recomd_wrap">
        <?php if(!empty($related)):?>
        <div id="new_recomd" >       
		    <div id="new_product_wrap">
                <ul class="recomd">

					<?php
					$cat_counter=1;
					foreach($related as $product):
					?>
					<?php
						$photo	= '<img src="'.base_url('images/nopicture.png').'" alt="'.lang('no_image_available').'"/>';
						$product->images	= array_values($product->images);

						if(!empty($product->images[0]))
						{
							$primary	= $product->images[0];
							foreach($product->images as $photo)
							{
								if(isset($photo->primary))
								{
									$primary	= $photo;
								}
							}

							if (strpos($primary->filename, 'http') === 0) {
								$photo	= '<img src="'.base_url().'phpthumb/phpThumb.php?src='.$primary->filename.'&w=800" alt="'.$product->seo_title.'"/>';
							} else {
								$photo	= '<img src="'.base_url().'phpthumb/phpThumb.php?src='.base_url('uploads/images/small/'.$primary->filename).'&w=220&h=220&far=1" alt="'.$product->seo_title.'"/>';
							}

							//$photo	= '<img src="'.base_url('uploads/images/medium/'.$primary->filename).'" alt="'.$product->seo_title.'" width="220" height="220" />';
						}
					?>
                    <li>
						<a href="<?php echo site_url($product->slug); ?>">
							<?php echo $photo; ?>
						</a><!-- <img src="images/single_carousel.jpg" width="220" height="220"  alt="We Recommend" /> -->
                    	<div class="single_product_carousel_detail">
                        	<h2><?php echo $product->name;?></h2>
                            <h3><span class="color333333"><?php echo $product->sku;?></span></h3>
                            <h3><span class="color000"><strong><?php echo format_currency($product->saleprice); ?></strong></span></h3>
                        </div>
                    </li>
					<?php endforeach; ?>    
                    
                </ul>
            </div>  <!-- End of new_product_wrap -->  	
			
        </div><!-- End of new_arrival -->
		<?php endif; ?>
</div>


  
<div class="clear"></div>
        <div class="gap"></div>


<?php include('footer.php'); ?>
<?php exit; ?>