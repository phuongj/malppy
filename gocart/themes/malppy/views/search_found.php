<?php include('header.php'); ?>

	<div class="main_wrap">
	<div id="container">
    	<div class="clear"></div>


<div id="new_arrival_wap" >
    <div id="new_arrival" style="position:absolute;">
             <div id="new_arival_top">
				<h1>SEARCH RESULT</h1>
           	</div><!-- End of new_arival_top -->
            
        <div id="new_product_wrap">
            <ul class="arival">
				<?php if(count($products) > 0):?>
				<?php foreach($products as $product): ?>
				<?php
					if (!$product->enabled) continue;
					if ($product->saleprice=="0.00") continue;
					$photo	= '<img src="'.base_url('<?php echo base_url();?>images/nopicture.png').'" alt="'.lang('no_image_available').'"/>';
					$product->images	= array_values($product->images);
					
					if(!empty($product->images[0]))
					{
						$primary	= $product->images[0];
						foreach($product->images as $photo)
						{
							if(isset($photo->primary))
							{
								$primary	= $photo;
							}
						}
						if (strpos($primary->filename, 'http') === 0) {
						$photo	= '<img src="'.base_url().'phpthumb/phpThumb.php?src='.$primary->filename.'&w=180&h=250&far=1" alt="'.$product->seo_title.'"/>';
						} else {
						$photo	= '<img src="'.base_url().'phpthumb/phpThumb.php?src='.base_url('uploads/images/small/'.$primary->filename).'&w=180&h=250&far=1" alt="'.$product->seo_title.'"/>';
						}
						//$photo	= '<img src="'.base_url('uploads/images/medium/'.$primary->filename).'" alt="'.$product->seo_title.'"/>';
					}
					?>
					<li><a href="<?php echo site_url($product->slug); ?>">
						<?php echo $photo; ?>
					</a><div class="price_tag"><?php echo format_currency($product->saleprice); ?></div></li>
				<?php endforeach; ?>
				<?php endif; ?>
              </ul>
        </div>  <!-- End of new_product_wrap -->  	
    </div><!-- End of new_arrival -->
</div><!-- End of new_arrival_wap -->

  <div class="clear"></div>
  
  
        
</div><!-- End of Main Wrapper -->
		
<?php include('footer.php'); ?>