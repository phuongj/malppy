<?php include('header.php'); ?>

<script type="text/javascript">
	$(document).ready(function(){
		$('.customer_info_box').equalHeights();
	});
</script>
<style>
h2.title {
	color: #333333;
    font-family: "AdelleBasic_B";
    font-size: 14px;
    font-weight: normal;
    padding-bottom: 5px;
}
</style>


<div class="main_wrap">
	<div class="container">

	<div class="breadCrumbHolder module" style="margin-top:-20px;margin-bottom:30px;">
				<div class="breadCrumb module" id="breadCrumb">
					<div style="overflow: hidden; position: relative;">
						<div>
							<ul style="width: 5000px;">
                    		    <li class="first"><a href="index.php">Home</a></li>
								<li>Shopping Cart</li>
                	   	     	<li>Shipping</li>
								<li class="last"><b>Order Confirmation</b></li>
        	          	  </ul>
						</div>
					</div>
				</div>
			</div>

	<br><br>
	<p align="center">Thank You for shopping with us, your Order ID is <b><?php echo $order_id;?></b></p>
	<br><br>
	<p align="center"><a href="<?php echo base_url();?>"><img src="<?php echo base_url();?>images/continue_shoping.png" alt="continue_shoping" /></a></p>
	<br><br>
<?php
		// content defined in canned messages
	 echo $download_section ?>

<div class="checkout_block">	 
<div class="customer_info_box" style="font-size:12px;">
	<h2 class="title"><?php echo lang('account_information');?></h2>
	<strong><?php echo (!empty($customer['company']))?$customer['company'].'<br>':'';?>
	<?php echo $customer['firstname'];?> <?php echo $customer['lastname'];?></strong> <br/>
	<?php echo $customer['email'];?> <br/>
	<?php echo $customer['phone'];?>
</div>
<div class="clear"></div>
</div>
<?php
$ship = $customer['ship_address'];
$bill = $customer['bill_address'];
?>
<div class="checkout_block">

<div class="customer_info_box" style="float:left;width:50%; font-size:12px;">
	<h2 class="title"><?php echo ($ship != $bill)?'Shipping Information':'Billing &amp; Shipping Information';?></h2>
	<strong><?php echo (!empty($ship['company']))?$ship['company'].'<br/>':'';?></strong>
	<?php echo $ship['firstname'].' '.$ship['lastname'];?> <br/>
	<?php echo $ship['address1'];?><br>
	<?php echo (!empty($ship['address2']))?$ship['address2'].'<br/>':'';?>
	<?php echo $ship['city'].', '.$ship['zone'].' '.$ship['zip'];?><br/>
	
	<?php echo $ship['email'];?><br/>
	<?php echo $ship['phone'];?><br/>
</div>
<?php if($ship != $bill):?>
<div class="customer_info_box" style="float:left;width:50%; font-size:12px;">
	<h2 class="title">Billing Information</h2>
	<strong><?php echo (!empty($bill['company']))?$bill['company'].'<br/>':'';?></strong>
	<?php echo $bill['firstname'].' '.$bill['lastname'];?> <br/>
	<?php echo $bill['address1'];?><br>
	<?php echo (!empty($bill['address2']))?$bill['address2'].'<br/>':'';?>
	<?php echo $bill['city'].', '.$bill['zone'].' '.$bill['zip'];?><br/>
	
	<?php echo $bill['email'];?><br/>
	<?php echo $bill['phone'];?>
</div>
<?php endif;?>
<!-- <div class="clear"></div>
<div class="customer_info_box">
	<h2 class="title"<?php echo lang('payment_information');?></h2>
	<?php echo $payment['description']; ?><br/>
	<h2 class="title" style="padding-top:10px;"><?php echo lang('shipping_method');?></h2>
	<?php echo $shipping['method']; ?>
</div>
<br> -->
<div class="clear"></div>
</div>

<!-- <div class="customer_info_box">
	<h2 class="title"><?php echo lang('additional_details');?></h2>
	<?php
	extract($additional_details);
	if(!empty($referral)):?><div style="margin-top:10px;"><strong><?php echo lang('heard_about');?></strong> <?php echo $referral;?></div><?php endif;?>
	<?php if(!empty($shipping_notes)):?><div style="margin-top:10px;"><strong><?php echo lang('shipping_instructions');?></strong> <?php echo $shipping_notes;?></div><?php endif;?>
		
</div> -->
<!-- 
<table class="cart_table" cellpadding="0" cellspacing="0" border="0">
	<thead>
		<tr>
			<th style="width:10%;"><?php echo lang('sku');?></th>
			<th style="width:20%;"><?php echo lang('name');?></th>
			<th style="width:10%;"><?php echo lang('price');?></th>
			<th><?php echo lang('description');?></th>
			<th style="text-align:center; width:10%;"><?php echo lang('quantity');?></th>
			<th style="width:8%;"><?php echo lang('totals');?></th>
		</tr>
	</thead>
	<tfoot>
		<tr class="tfoot_top"><td colspan="7"></td></tr>
		 <?php if($this->go_cart->group_discount() > 0)  : ?> 
       	<tr>
			<td colspan="5"><?php echo lang('group_discount');?></td>
			<td><?php echo format_currency(0-$this->go_cart->group_discount()); ?>                </td>
		</tr>
		<?php endif; ?>
        <tr>
			<td colspan="5"><?php echo lang('subtotal');?></td>
			<td>
            <?php echo format_currency($this->go_cart->subtotal()); ?>                </td>
		</tr>
       <?php if($this->go_cart->coupon_discount() > 0)  : ?> 
    	<tr>
			<td colspan="5"><?php echo lang('coupon_discount');?></td>
			<td><?php echo format_currency(0-$this->go_cart->coupon_discount()); ?>                </td>
		</tr>
			 <?php if($this->go_cart->order_tax() != 0) : // Only show a discount subtotal if we still have taxes to add (to show what the tax is calculated from) ?> 
		<tr>
			<td colspan="5"><?php echo lang('discounted_subtotal');?></td>
			<td><?php echo format_currency($this->go_cart->discounted_subtotal()); ?>                </td>
		</tr>

       <?php endif;

       endif; ?>
       <?php // Show shipping cost if added before taxes
		if($this->config->item('tax_shipping') && $this->go_cart->shipping_cost()>0) : ?>
		<tr>
			<td><?php echo lang('shipping');?></td>
			<td colspan="4"><?php echo $shipping['method']; ?></td>
			<td><?php echo format_currency($this->go_cart->shipping_cost()); ?>                </td>
		</tr>
		<?php endif ?>
       <?php if($this->go_cart->order_tax() != 0) : ?> 
     	<tr>
			<td colspan="5"><?php echo lang('taxes');?></td>

			<td><?php echo format_currency($this->go_cart->order_tax()); ?>                </td>
		</tr>
      <?php endif;   ?>
       <?php // Show shipping cost if added after taxes
		if(!$this->config->item('tax_shipping') && $this->go_cart->shipping_cost()>0) : ?>
		<tr>
			<td><?php echo lang('shipping');?></td>
			<td colspan="4" style="font-weight:normal;"><?php echo $shipping['method']; ?></td>
			<td><?php echo format_currency($this->go_cart->shipping_cost()); ?>                </td>
		</tr>
		<?php endif ?>
       <?php if($this->go_cart->gift_card_discount() != 0) : ?> 
     	<tr>
			<td colspan="5"><?php echo lang('gift_card');?></td>

			<td><?php echo format_currency(0-$this->go_cart->gift_card_discount()); ?>                </td>
		</tr>
      <?php endif;   ?>
        <tr class="cart_total"> 
			<td colspan="5"><?php echo lang('grand_total');?></td>
			<td><?php echo format_currency($this->go_cart->total()); ?>                </td>
		</tr>
		<tr class="tfoot_bottom"><td colspan="7"></td></tr>
	</tfoot>

	<tbody id="cart_items">
	<?php
	$subtotal = 0;
	foreach ($this->go_cart->contents() as $cartkey=>$product):?>
		<tr class="cart_spacer"><td colspan="7"></td></tr>
		<tr class="cart_item">
			<td><?php echo $product['sku'];?></td>
			<td><?php echo $product['name']; ?></td>
			<td><?php echo format_currency($product['base_price']);   ?></td>
			<td><?php echo $product['excerpt'];
				if(isset($product['options'])) {
					foreach ($product['options'] as $name=>$value)
					{
						if(is_array($value))
						{
							echo '<div><span class="gc_option_name">'.$name.':</span><br/>';
							foreach($value as $item)
								echo '- '.$item.'<br/>';
							echo '</div>';
						} 
						else 
						{
							echo '<div><span class="gc_option_name">'.$name.':</span> '.$value.'</div>';
						}
					}
				}
				?></td>
			<td style="text-align:center;"><?php echo $product['quantity'];?></td>
			<td><?php echo format_currency($product['price']*$product['quantity']); ?>				</td>
		</tr>
			
	<?php endforeach; ?>
	<tr class="cart_spacer"><td colspan="7"></td></tr>
	</tbody>
	</table> -->
	
	<div id="check_wrap" style="padding: 0 0 0 5px;">          
  			<div id="check">
								
				<div class="clear"></div>
				<div id="check_heading_left">
				<a>Items</a>
				</div>
				
				<div id="check_heading_right">
					<div class="check_heading_unit"><a href="#">Unit Price</a></div>
					<div class="check_heading_quantity" style="width:130px"><a href="#">Quantity</a></div>
					<div class="check_heading_ttl" style="padding-left:30px"><a href="#">Totals</a></div>

				</div>
				<div class="clear"></div>
				<div class="check_border"></div>
				
				<div class="clear"></div>
				
				<?php
				$subtotal = 0;
				
				foreach ($this->go_cart->contents() as $cartkey=>$product):?>
				
				<?php 
                $this_product = $this->Product_model->get_product($product['id']);
				
				$this_product->images	= (array)json_decode($this_product->images);

				unset($primary);
				
				foreach($this_product->images as $image)
				{
					if(isset($image->primary))
					{
						$primary	= $image;
					}
				}
				
				if(!isset($primary)) {
					foreach($this_product->images as $image)
					{
						$primary	= $image;
						if ($image) break;
					}
				}
                ?>
				<div class="shoping_cart_container">
					<div class="shoping_cart_container_pic">
						<!-- <img src="<?php echo base_url();?>images/shoping_cart_pic.jpg" alt="update_cart" /> -->
						<?php if (strpos($primary->filename, 'http') === 0) { ?>
							<img src="<?php echo base_url(); ?>phpthumb/phpThumb.php?src=<?php echo $primary->filename;?>&w=110&h=110&far=1" alt=""/>
						<?php } else { ?>
							<img src="<?php echo base_url(); ?>phpthumb/phpThumb.php?src=<?php echo base_url('uploads/images/thumbnails/'.$primary->filename);?>&w=110&h=110&far=1" alt=""/>
						<?php } ?>
					</div>
					<div class="shoping_cart_container_txt">
						<h2 class="color006699"><?php echo strip_tags($product['name']); ?></h2>
						<p>
						<?php
						if(isset($product['options'])) {
							foreach ($product['options'] as $name=>$value)
							{
								if(is_array($value))
								{
									echo '<div><span class="gc_option_name"><p>'.$name.'</p>:</span><br/>';
									foreach($value as $item)
										echo '- '.$item.'<br/>';
									echo '</div>';
								} 
								else 
								{
									echo '<div><span class="gc_option_name"><p>'.$name.':</span> '.$value.'</p></div>';
								}
							}
						}
						?>
						</p>
						<p>SKU: <?php echo $product['sku'];?></p>
					</div>
					
					<div class="shoping_cart_container_unit">
					<h2><?php echo format_currency($product['base_price']);   ?></h2>
					</div>
					
					<div class="shoping_cart_container_unit">
					<h2>
					<?php echo $product['quantity'];?>
					</h2>
					</div>
				
					<div class="shoping_cart_container_ttl">
					<h2><?php echo format_currency($product['price']*$product['quantity']); ?>		</h2>
					</div>
					
					<div class="shoping_cart_container_del">
					&nbsp;
					</div>					
					
					
				</div><!-- End Containter -->
				<?php endforeach; ?>
				
				
            </div> <!-- End of check-->   

				<div class="clear"></div>
				<div id="total_section">
				
				<div class="total_section_col"><span class="color006699">Shipping</span></div>
				<div class="total_section_col"><span class="color006699">Free</span></div>
<!-- 				<div class="total_section_col">Subtotal</div> -->
				<!-- <div class="total_section_col"><?php echo format_currency($this->go_cart->subtotal()); ?> </div> -->
				<div class="total_section_col">Total</div>
				<div class="total_section_col"><?php echo format_currency($this->go_cart->total()); ?> </div>
				
				</div>
				
				
				
				<div class="clear"></div>
				
        </div><!-- End of check_wrap -->
<div class="clear"></div>

	</div> <!--  container class -->
</div>	<!--  main_wrap class -->
	
<?php include('footer.php');