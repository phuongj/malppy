<?php include(APPPATH.'themes/'.$this->config->item('theme').'/views/header.php'); ?>
<script type="text/javascript" src="<?php echo base_url('js/jquery/jquery-1.7.1.min.js');?>"></script>
<script>

var $l = $.noConflict();

</script>
<script type="text/javascript" src="<?php echo base_url();?>js/custom_jquery.js"></script>

<script type='text/javascript' src='<?php echo base_url();?>js/jquery.hoverIntent.minified.js'></script>

<script type='text/javascript' src='<?php echo base_url();?>js/jquery.dcmegamenu.1.3.3.js'></script>

<script type="text/javascript" src="<?php echo base_url();?>js/custom_menu.js"></script>

<link rel="stylesheet" href="<?php echo base_url();?>css/dropkick.css" type="text/css"/>
<script src="<?php echo base_url();?>js/jquery.dropkick-1.0.0.js" type="text/javascript"></script>
<script type="text/javascript">
$l(document).ready(function() {
	//$('.continue_shopping').buttonset();

	// higlight  fields
	$l('.input').focus(function(){
		$l(this).addClass('input_hover');
	});
	
	// higlight fields
	$l('.input').blur(function(){
		$l(this).removeClass('input_hover');
	});
	
	<?php if(isset($customer['ship_address'])):?>
		$l.post('<?php echo site_url('checkout/customer_details');?>', function(data){
			//populate the form with their information
			$l('#customer_info_fields').html(data);
			//$('input:button, input:submit, button').button();
		});
	<?php else:	?>
		get_customer_form();
	<?php endif;?>

});

function get_customer_form()
{
	//the loader will only show if someone is editing their existing information
	$l('#save_customer_loader').show();
	//hide the button again
	$l('#submit_button_container').hide();
	
	//remove the shipping and payment forms
	//$('#shipping_payment_container').html('<div class="checkout_block"><img alt="loading" src="<?php echo base_url('images/ajax-loader.gif');?>"/><br style="clear:both;"/></div>').hide();
	$l.post('<?php echo site_url('checkout/customer_form'); ?>', function(data){
		//populate the form with their information
		$l('#customer_info_fields').html(data);
		//$('input:button, input:submit, button').button();
		update_summary();
		$l('#ship_country_id,#ship_zone_id,#bill_country_id,#bill_zone_id').dropkick();
	});
}

// some behavior controlling global variables
var logged_in_user = <?php if($this->Customer_model->is_logged_in(false, false)) echo "true"; else echo "false"; ?>;

var shipping_required = <?php echo ($this->go_cart->requires_shipping()) ? 'true' : 'false'; ?>;
var shipping = Array();
var shipping_choice = '<?php $shipping=$this->go_cart->shipping_method(); if($shipping) echo $shipping['method']; ?>';

var addr_context = '';
var ship_to_bill_address = <?php if(isset($customer['ship_to_bill_address'])) { echo $customer['ship_to_bill_address']; } else { echo 'false'; } ?>;
var addresses;

// cart total is also set in the summary view
cart_total = <?php echo $this->go_cart->total(); ?>;

// payment method
var chosen_method = ''; // holds the current chosen method
var payment_method = {}; // list of payment method validators



function submit_order()
{
				
	// if we need to save a payment method
	if(cart_total>0) {
	
		frm_data = $('#pmnt_form_'+chosen_method).serialize();
		
		$l.post('<?php echo site_url('checkout/save_payment_method'); ?>', frm_data, function(response)
		{
			if(typeof response != "object")
			{
				display_error('payment', '<?php echo lang('error_save_payment');  ?>');

				return;
			}
			
			if(response.status=='success')
			{
				// send them on to place the order
				$l('#order_submit_form').trigger('submit');
			}
			else if(response.status=='error')
			{
				display_error('payment', response.error);
			}
			
		}, 'json');
	} else {
		$l('#order_submit_form').trigger('submit');	
	}
}

function display_error(panel, message) 
{
	$l('#'+panel+'_error_box').html(message).show();
}

function clear_errors()
{
	$l('.error').hide();
	
	$l('.required').each(function(){ 
			$(this).removeClass('require_fail');
	});
	
	$l('.pmt_required').each(function(){ 
			$(this).removeClass('require_fail');
	});
}


// shipping cost visual calculator
function set_shipping_cost()
{
		
	clear_errors();
	
	$l.post('<?php echo site_url('checkout/save_shipping_method');?>', {shipping:$(':radio[name$="shipping_input"]:checked').val()}, function(response)
	{
		update_summary();
	});
}

function set_chosen_payment_method(value)
{
	chosen_method = value;
}

// Set payment info
function submit_payment_method()
{
	
	clear_errors();
	
	errors = false;
		
	// verify a shipping method is chosen
	if(shipping_required && $('input:radio[name=shipping_input]:checked').val()===undefined && $('input:radio[name=shipping_input]').length > 0)
	{
		display_error('shipping', '<?php echo lang('error_choose_shipping');?>');
		errors = true;
	}
		
	// validate payment method if payment is required
	if(cart_total>0)
	{
		// verify a payment option is chosen
		if($('input[name=payment_method]').length > 1)
		{
			if($('input:radio[name=payment_method]:checked').val()===undefined)
			{
				display_error('payment', '<?php echo lang('error_choose_payment');?>');
				errors = true;
			}
		}
		
		// determine if our payment method has a built-in validator
		if(typeof payment_method[chosen_method] == 'function' )
		{
			if(!payment_method[chosen_method]())
			{
				errors = true;
			}
		}
	}
	
	// stop here if we have problems
	if(errors)
	{
		return false;
	}

	// send the customer data again and then submit the order
}

function save_order()
{
	//submit additional order details
	$l.post('<?php echo site_url('checkout/save_additional_details');?>', $('#additional_details_form').serialize(), function(){

		//thus must be a callback, otherwise there is a risk of the form submitting without the additional details saved
		// if we need to save a payment method
		if(cart_total>0) {

			frm_data = $('#pmnt_form_'+chosen_method).serialize();
			
			
			$l.post('<?php echo site_url('checkout/save_payment_method');?>', frm_data, function(response)
			{
			
				if(typeof response != "object")
				{
					display_error('payment', '<?php echo lang('error_save_payment') ?>');

					return;
				}

				if(response.status=='success')
				{
					// send them on to place the order
					$l('#order_submit_form').trigger('submit');
				}
				else if(response.status=='error')
				{
					display_error('payment', response.error);
				}

			}, 'json');
		} else {
			$l('#order_submit_form').trigger('submit');	
		}
	});
}	 			

// refresh the summary so that tax rows will be incorporated into the display
// (they'll be missing before the customer enters their address and change if they change it)
function update_summary()
{
	// refresh confirmation content
	$l.post('<?php echo site_url('checkout/order_summary');?>', {}, function(response)
	{
		$l('#summary_section').html(response);	
	});
}


</script>
<style>
#dk_container_ship_zone_id .dk_toggle, #dk_container_bill_zone_id .dk_toggle {
	height:14px;
}
</style>

<div class="main_wrap">
	<div class="container">

	<div class="breadCrumbHolder module" style="margin-top:-20px;margin-bottom:30px;">
				<div class="breadCrumb module" id="breadCrumb">
					<div style="overflow: hidden; position: relative;">
						<div>
							<ul style="width: 5000px;">
                    		    <li class="first"><a href="index.php">Home</a></li>
								<li><b>Shipping</b></li>
								<li>Payment</li>
								<li class="last">Order</li>
        	          	  </ul>
						</div>
					</div>
				</div>
	</div> <!-- End of breadCrumbHolder -->

	<div><?php echo 'Status: '.$checkout_state;?></div>
	<?php if ($checkout_state == 'Shipping'){?>
	<div id="shipping_block">
						<div class="continue_shopping">
				<?php if(!$this->Customer_model->is_logged_in(false, false)) : ?>
					<!-- <input type="button" onclick="window.location='<?php echo site_url('checkout/login');?>'" value="<?php echo lang('form_login');?>" /> -->
					<a href="<?php echo site_url('checkout/login');?>"><img src="<?php echo base_url();?>images/login.png" alt="login" /></a>
					<!-- <input type="button" onclick="window.location='<?php echo site_url('checkout/register');?>'" value="<?php echo lang('register_now');?>"/> -->
					<a href="<?php echo site_url('checkout/register');?>"><img src="<?php echo base_url();?>images/bg_registration.png" alt="register_now" /></a>
					
				<?php endif;?>
				<!-- <input type="button" onclick="window.location='<?php echo base_url();?>'" value="<?php echo lang('continue_shopping');?>"/> -->
				
				<a href="<?php echo base_url();?>"><img src="<?php echo base_url();?>images/continue_shoping.png" alt="continue_shoping" /></a>
								
			</div>
			<br>
			<div class="checkout_block">
				<div id="customer_info_fields">
					<h2 class="title"><?php echo lang('customer_information');?></h2>
					<img alt="loading" src="<?php echo base_url('images/ajax-loader.gif');?>"/>
				</div>
				<br style="clear:both;"/>
			</div>
			
			<div id="shipping_payment_container" style="display:none;">
				<div class="checkout_block">
					<img alt="loading" src="<?php echo base_url('images/ajax-loader.gif');?>"/>
					<br style="clear:both;"/>
				</div>
			</div>
			
			<div id="summary_section">
			<?php  include('summary.php'); ?>
			</div>
			<div id="submit_button_container" style="text-align:center; padding-top:10px;">
			
			<a href="<?php echo base_url();?>checkout/payment">Payment</a>
			<form id="order_submit_form" action="<?php echo site_url('checkout/place_order'); ?>" method="post">
			<input type="hidden" name="process_order" value="true">
			<!-- <input style="padding:10px 15px; font-size:16px;background-image:url('<?php echo base_url('images/submit.png');?>');background-color:#0091EF;" type="submit" value="<?php echo lang('submit_order');?>" /> -->
			
			
			<input id="submit_order_button" type="submit" value="Submit Order"></input>
			<!-- <input style="padding:10px 15px; font-size:16px;" type="button" onclick="submit_payment_method()" value="<?php echo lang('submit_order');?>" />
			 --></form>
			</div>
			
			
			<br><br>
	
	</div><!-- End of shipping_block -->
	<?php } elseif  ($checkout_state == 'Payment') {?>
	<div id="payment_block">
	
		<form action="https://www.mybillpayment.com/testing/validate.php" method="post">
			
			<fieldset class="credit_payment">
			<dl class="credit_dl">
			
				<dt></dt><dd><input type="hidden" name="crefnum" maxlength="16" value="AA00000000000001"></dd>
				<dt></dt><dd><input type="hidden" name="camount" value="0.01"></dd>
				<dt></dt><dd><input type="hidden" name="cdesc" value="Testing the space"></dd>
				<dt></dt><dd><input type="hidden" name="cmid" value="000003013012374" maxlength="15"></dd>
				<dt></dt><dd><input type="hidden" name="ccurrency" value="092"></dd>
				<dt></dt><dd><input type="hidden" name="cstatusurl" value="YOUR STATUS URL"></dd>
				<dt></dt><dd><input type="hidden" name="creturnurl" value="YOUR RETURN PAGE"></dd>
				<dt></dt><dd><input type="hidden" name="cpaymode" value ="55"></dd>
				<dt><label>CardHolderName</label></dt><dd><input type="text" name="CardHolderName" value ="Arman G. de Castro"></dd>
				<dt><label>Email Address</label></dt><dd><input type="text" name="EmailAdd" value ="armandecastro@gmail.com"></dd>
		
			</dl>
		
			</fieldset>
			<input type="submit" value="Buy This">
		</form>
		

		<button type="button" id="continue_button" onclick="switch_block()" value="Payment">Continue</button>
	<button type="button" id="continue_button" onclick="switch_block()" value="Payment">Continue</button>
	</div><!-- End of payment_block -->
	<?php }?>
	</div>
</div>

<?php include(APPPATH.'themes/'.$this->config->item('theme').'/views/footer.php'); ?>