<script>
$(function() {
	// keep the cart total up to date for other JS functionality
	cart_total = <?php echo $this->go_cart->total(); ?>;
	
	if(cart_total==0)
	{
		$('#payment_section_container').hide();
		$('#no_payment_necessary').show();
	} else {
		$('#payment_section_container').show();
		$('#no_payment_necessary').hide();
	}
});
</script>

	<div id="check_wrap">          
  			<div id="check">
								
				<div class="clear"></div>
				<div id="check_heading_left">
				<a>Items</a>
				</div>
				
				<div id="check_heading_right">
					<div class="check_heading_unit"><a href="#">Unit Price</a></div>
					<div class="check_heading_quantity" style="padding-left: 20px;"><a href="#">Quantity</a></div>
					<div class="check_heading_ttl" style="padding-left: 40px;"><a href="#">Totals</a></div>

				</div>
				<div class="clear"></div>
				<div class="check_border"></div>
				
				<div class="clear"></div>
				
				<?php
				$subtotal = 0;
				
				foreach ($this->go_cart->contents() as $cartkey=>$product):?>
				
				<?php 
                $this_product = $this->Product_model->get_product($product['id']);
				
				$this_product->images	= (array)json_decode($this_product->images);

				unset($primary);

				foreach($this_product->images as $image)
				{
					if(isset($image->primary))
					{
						$primary	= $image;
					}					
				}
				
				if(!isset($primary)) {
					foreach($this_product->images as $image)
					{
						$primary	= $image;
						if ($image) break;
					}
				}
                ?>
				<div class="shoping_cart_container">
					<div class="shoping_cart_container_pic">
						<!-- <img src="<?php echo base_url();?>images/shoping_cart_pic.jpg" alt="update_cart" /> -->
						<!-- <img src="<?php echo base_url('uploads/images/thumbnails/'.$primary->filename);?>" alt=""/> -->
						<?php if (strpos($primary->filename, 'http') === 0) { ?>
							<img src="<?php echo base_url(); ?>phpthumb/phpThumb.php?src=<?php echo $primary->filename;?>&w=110&h=110&far=1" alt=""/>
						<?php } else { ?>
							<img src="<?php echo base_url(); ?>phpthumb/phpThumb.php?src=<?php echo base_url('uploads/images/thumbnails/'.$primary->filename);?>&w=110&h=110&far=1" alt=""/>
						<?php } ?>
					</div>
					<div class="shoping_cart_container_txt">
						<h2 class="color006699" style="padding-bottom:10px;"><?php echo strip_tags($product['name']); ?></h2>
						<p>SKU: <?php echo $product['sku'];?></p>
						<p>
						<?php
						if(isset($product['options'])) {
							foreach ($product['options'] as $name=>$value)
							{
								if(is_array($value))
								{
									echo '<div><span class="gc_option_name"><p>'.$name.'</p>:</span><br/>';
									foreach($value as $item)
										echo '- '.$item.'<br/>';
									echo '</div>';
								} 
								else 
								{
									echo '<div><span class="gc_option_name"><p>'.$name.':</span> '.$value.'</p></div>';
								}
							}
						}
						?>
						</p>
					</div>
					
					<div class="shoping_cart_container_unit">
					<h2><?php echo format_currency($product['base_price']);   ?></h2>
					</div>
					
					<div class="shoping_cart_container_unit">
					<h2>
					<?php echo $product['quantity'];?>
					</h2>
					</div>
				
					<div class="shoping_cart_container_ttl">
					<h2><?php echo format_currency($product['price']*$product['quantity']); ?>		</h2>
					</div>
					
					<div class="shoping_cart_container_del">
					&nbsp;
					</div>					
					
					
				</div><!-- End Containter -->
				<?php endforeach; ?>
				
				
            </div> <!-- End of check-->   

				<div class="clear"></div>
				<div id="total_section">
				
				<div class="total_section_col"><span class="color006699">Shipping</span></div>
				<div class="total_section_col"><span class="color006699">Free</span></div>
<!-- 				<div class="total_section_col">Subtotal</div> -->
				<!-- <div class="total_section_col"><?php echo format_currency($this->go_cart->subtotal()); ?> </div> -->
				<?php if($this->go_cart->coupon_discount() > 0)  : ?> 
				<div class="total_section_col">Discount</div>
				<div class="total_section_col"><?php echo format_currency(0-$this->go_cart->coupon_discount()); ?> </div>
				<?php endif; ?>
				<div class="total_section_col">Total</div>
				<div class="total_section_col"><?php echo format_currency($this->go_cart->total()); ?> </div>
				
				</div>
				
				
				
				<div class="clear"></div>
				
        </div><!-- End of check_wrap -->
<div class="clear"></div>
	<!-- <table class="cart_table" cellpadding="0" cellspacing="0" border="0">
		<thead>
			<thead>
				<tr>
					<th style="width:10%;"><?php echo lang('sku');?></th>
					<th style="width:20%;"><?php echo lang('name');?></th>
					<th style="width:10%;"><?php echo lang('price');?></th>
					<th><?php echo lang('description');?></th>
					<th style="text-align:center; width:10%;"><?php echo lang('quantity');?></th>
					<th style="width:8%;"><?php echo lang('totals');?></th>
				</tr>
			</thead>
		</thead>
		
		<tfoot>
			<tr class="tfoot_top"><td colspan="6"></td></tr>
			<?php
			/**************************************************************
			Subtotal Calculations
			**************************************************************/
			?>
			<?php if($this->go_cart->group_discount() > 0)  : ?> 
        	<tr>
				<td colspan="5"><?php echo lang('group_discount');?></td>
				<td><?php echo format_currency(0-$this->go_cart->group_discount()); ?>                </td>
			</tr>
			<?php endif; ?>
			<tr>
		    	<td colspan="5"><?php echo lang('subtotal');?></td>
				<td id="gc_subtotal_price"><?php echo format_currency($this->go_cart->subtotal()); ?></td>
			</tr>
				
				
			<?php if($this->go_cart->coupon_discount() > 0) {?>
		    <tr>
		    	<td colspan="5"><?php echo lang('coupon_discount');?></td>
				<td id="gc_coupon_discount">-<?php echo format_currency($this->go_cart->coupon_discount());?></td>
			</tr>
				<?php if($this->go_cart->order_tax() != 0) { // Only show a discount subtotal if we still have taxes to add (to show what the tax is calculated from)?> 
				<tr>
		    		<td colspan="5"><?php echo lang('discounted_subtotal');?></td>
					<td id="gc_coupon_discount"><?php echo format_currency($this->go_cart->discounted_subtotal());?></td>
				</tr>
				<?php
				}
			} 
			/**************************************************************
			 Custom charges
			**************************************************************/
			$charges = $this->go_cart->get_custom_charges();
			if(!empty($charges))
			{
				foreach($charges as $name=>$price) : ?>
					
			<tr>
				<td colspan="5"><?php echo $name?></td>
				<td><?php echo format_currency($price); ?></td>
			</tr>	
					
			<?php endforeach;
			}	
			
			/**************************************************************
			Order Taxes
			**************************************************************/
			 // Show shipping cost if added before taxes
			if($this->config->item('tax_shipping') && $this->go_cart->shipping_cost()>0) : ?>
				<tr>
				<td colspan="5"><?php echo lang('shipping');?></td>
				<td id="gc_tax_price"><?php echo format_currency($this->go_cart->shipping_cost()); ?></td>
			</tr>
			<?php endif;
			if($this->go_cart->order_tax() > 0) :  ?>
		    <tr>
		    	<td colspan="5" colspan="3"><?php echo lang('tax');?></td>
				<td id="gc_tax_price"><?php echo format_currency($this->go_cart->order_tax());?></td>
			</tr>
			<?php endif; 
			// Show shipping cost if added after taxes
			if(!$this->config->item('tax_shipping') && $this->go_cart->shipping_cost()>0) : ?>
				<tr>
				<td colspan="5"><?php echo lang('shipping');?></td>
				<td id="gc_tax_price"><?php echo format_currency($this->go_cart->shipping_cost()); ?></td>
			</tr>
			<?php endif ?>
			
			<?php
			/**************************************************************
			Gift Cards
			**************************************************************/
			if($this->go_cart->gift_card_discount() > 0) : ?>
			<tr>
				<td colspan="5"><?php echo lang('gift_card_discount');?></td>
				<td id="gc_gift_discount">-<?php echo format_currency($this->go_cart->gift_card_discount()); ?></td>
			</tr>
			<?php endif; ?>
			
			<?php
			/**************************************************************
			Grand Total
			**************************************************************/
			?>
			<tr class="cart_total">
				<td colspan="5" class="gc_view_cart_totals"><div class="cart_total_line_left"></div><?php echo lang('grand_total');?></td>
				<td id="gc_total_price" class="gc_total" nowrap><div class="cart_total_line_right"></div><?php echo format_currency($this->go_cart->total()); ?></td>
			</tr>
			
			<tr class="tfoot_bottom"><td colspan="6"></td></tr>
		</tfoot>
		
		<tbody class="cart_items">
			<?php
			$subtotal = 0;

			foreach ($this->go_cart->contents() as $cartkey=>$product):?>
				<tr class="cart_spacer"><td colspan="7"></td></tr>
				<tr class="cart_item">
					<td><?php echo $product['sku']; ?></td>
					<td><?php echo $product['name']; ?></td>
					<td><?php echo format_currency($product['price']);?></td>
					<td>
						<?php echo $product['excerpt'];
							if(isset($product['options'])) {
								foreach ($product['options'] as $name=>$value)
								{
									if(is_array($value))
									{
										echo '<div><span class="gc_option_name">'.$name.':</span><br/>';
										foreach($value as $item)
											echo '- '.$item.'<br/>';
										echo '</div>';
									} 
									else 
									{
										echo '<div><span class="gc_option_name">'.$name.':</span> '.$value.'</div>';
									}
								}
							}
							?>
					</td>
					
					<td style="text-align:center;">
						<?php echo $product['quantity'];?>
					</td>
					<td class="total" nowrap><?php echo format_currency($product['price']*$product['quantity']); ?></td>
				</tr>
			<?php endforeach;?>
			<tr class="cart_spacer"><td colspan="7"></td></tr>
		</tbody>
	</table> -->