<?php include('header.php');?>
<script type="text/javascript">
if (top.location != location) {
	top.location.href = document.location.href;
}	
</script>
<style>
.message {
    background-color: #FFFFFF;
    border: medium none;
    box-shadow: none;
    color: #AAAAAA;
    margin-bottom: 10px;
    padding: 20px 25px;
}
</style>

<div class="main_wrap">
	
	

	<div id="static_container">

		<?php
		if(validation_errors())
		{
			echo '<div id="errmsg" style="width:925px;"><div class="error">'.validation_errors().'</div></div><div class="clear"></div>';
		}
		if ($this->session->flashdata('message'))
		{
			echo '<div class="gmessage">'.$this->session->flashdata('message').'</div>';
		}
		if ($this->session->flashdata('error'))
		{
			echo '<div class="error">'.$this->session->flashdata('error').'</div>';
		}
		if (!empty($error))
		{
			echo '<div class="error">'.$error.'</div>';
		}
		?>

		<?php if ($this->go_cart->total_items()==0):?>
		<div class="container message">
				<br><br>
				<p align="center" style="color:#000">There are no products in your cart!</p>
			
				<br><br>
				<p align="center"><a href="<?php echo base_url();?>"><img src="<?php echo base_url();?>images/continue_shoping.png" alt="continue_shoping" /></a></p>
	
		</div>
		
		<?php else: ?>

		<?php echo form_open('cart/update_cart', array('id'=>'update_cart_form'));?>
		<!-- <input id="redirect_path" type="hidden" name="redirect" value="checkout"/> -->

		
		
        <div id="check_wrap">          

			<div class="breadCrumbHolder module">
				<div class="breadCrumb module" id="breadCrumb">
					<div style="overflow: hidden; position: relative;">
						<div>
							<ul style="width: 5000px;">
                    		    <li class="first"><a href="index.php">Home</a></li>
								<li class="last"><b>Shopping Cart</b></li>
<!--                 	   	     	<li>Order Details</li> -->
<!-- 								<li class="last">Order Confirmation</li> -->
        	          	  </ul>
						</div>
					</div>
				</div>
			</div>

  			<div id="check">
				
				
				
				
				<div class="clear"></div>
				<div id="check_heading_left">
				<a>Items</a>
				</div>
				
				<div id="check_heading_right">
					<div class="check_heading_unit"><a href="#">Unit Price</a></div>
					<div class="check_heading_quantity"><a href="#">Quantity</a></div>
					<div class="check_heading_ttl"><a href="#">Totals</a></div>

				</div>
				<div class="clear"></div>
				<div class="check_border"></div>
				
				<div class="clear"></div>
				
				<?php
				$subtotal = 0;
				
				foreach ($this->go_cart->contents() as $cartkey=>$product):?>
				
				<?php 
                $this_product = $this->Product_model->get_product($product['id']);
				
				$this_product->images	= (array)json_decode($this_product->images);

				unset($primary);
				
				//foreach($this_product->images as $image)
				//{
				//	$primary	= $image;
				//}

				foreach($this_product->images as $image)
				{
					if(isset($image->primary))
					{
						$primary	= $image;
					}					
				}
				
				if(!isset($primary)) {
					foreach($this_product->images as $image)
					{
						$primary	= $image;
						if ($image) break;
					}
				}
                ?>
				<div class="shoping_cart_container">
					<div class="shoping_cart_container_pic">
						<!-- <img src="<?php echo base_url();?>images/shoping_cart_pic.jpg" alt="update_cart" /> -->
						<?php if (strpos($primary->filename, 'http') === 0) { ?>
							<img src="<?php echo base_url(); ?>phpthumb/phpThumb.php?src=<?php echo $primary->filename;?>&w=110&h=110&far=1" alt=""/>
						<?php } else { ?>
							<img src="<?php echo base_url(); ?>phpthumb/phpThumb.php?src=<?php echo base_url('uploads/images/thumbnails/'.$primary->filename);?>&w=110&h=110&far=1" alt=""/>
						<?php } ?>
						
					</div>
					<div class="shoping_cart_container_txt">
						<h2 class="color006699" style="padding-bottom:10px;"><?php echo strip_tags($product['name']); ?></h2>
						<!-- <p>Size L <br/>SKU: <?php echo $product['sku'];?></p> -->
						<p>SKU: <?php echo $product['sku'];?></p>
						<?php
						if(isset($product['options'])) {
							foreach ($product['options'] as $name=>$value)
							{
								if(is_array($value))
								{
									echo '<div><span class="gc_option_name"><p>'.$name.'</p>:</span><br/>';
									foreach($value as $item)
										echo '- '.$item.'<br/>';
									echo '</div>';
								} 
								else 
								{
									echo '<div><span class="gc_option_name"><p>'.$name.':</span> '.$value.'</p></div>';
								}
							}
						}
						?>
						</p>
					</div>
					
					<div class="shoping_cart_container_unit">
					<h2><?php echo format_currency($product['base_price']);   ?></h2>
					</div>

					<!-- <div class="shoping_cart_container_unit">
					<?php if($this->go_cart->coupon_discount() > 0)  : ?> 
					<h2><?php echo format_currency(0-$this->go_cart->coupon_discount()); ?></h2>
          			<?php endif;?>
					</div> -->
					
					<div class="shoping_cart_container_qty">
					<h2>
					<?php if(!(bool)$product['fixed_quantity']):?>
						<input type="text" style="width:30px;border:none;" name="cartkey[<?php echo $cartkey;?>]" value="<?php echo $product['quantity'] ?>" size="3"/>
					<?php else:?>
						<?php echo $product['quantity'] ?>
						<input type="hidden" name="cartkey[<?php echo $cartkey;?>]" value="1"/>
					<?php endif;?>
					</h2>
					</div>
				
					<div class="shoping_cart_container_ttl">
					<h2><?php echo format_currency($product['price']*$product['quantity']); ?>		</h2>
					</div>
					
					<div class="shoping_cart_container_del">
					<a href="<?php echo site_url('cart/remove_item/'.$cartkey);?>"><img src="<?php echo base_url();?>images/delete.png" alt="Delete" /></a>
					</div>					
					
					
				</div><!-- End Containter -->
				<?php endforeach; ?>
				
				
            </div> <!-- End of check-->   

				<div class="clear"></div>
				
				<div id="total_section">
				
				<div class="total_section_col"><span class="color006699">Shipping</span></div>
				<div class="total_section_col"><span class="color006699">Free</span></div>
<!-- 				<div class="total_section_col">Subtotal</div> -->
				<!-- <div class="total_section_col"><?php echo format_currency($this->go_cart->subtotal()); ?> </div> -->
				<?php if($this->go_cart->coupon_discount() > 0)  : ?> 
				<div class="total_section_col">Discount</div>
				<div class="total_section_col"><?php echo format_currency(0-$this->go_cart->coupon_discount()); ?> </div>
				<?php endif; ?>
				<div class="total_section_col">Total</div>
				<div class="total_section_col"><?php echo format_currency($this->go_cart->total()); ?> </div>
				
				</div>

				<div class="view_cart_additions">
				<table>
					<tr>
						<td>
							<?php echo lang('coupon_label');?><br/><input type="text" name="coupon_code">
							<input type="submit" value="<?php echo lang('apply_coupon');?>"/>
						</td>
					</tr>
  				</table>
				</div>	
				
				
				<div class="clear"></div>
				<div class="check_bottom"></div>  
				<div class="check_top_links">
					<a href="<?php echo base_url();?>"><img src="<?php echo base_url();?>images/continue_shoping.png" alt="continue_shoping" /></a>
					<input type="image" src="<?php echo base_url();?>images/update_cart.png" value="<?php echo lang('form_update_cart');?>"/>
					<?php //if ($this->Customer_model->is_logged_in(false,false) || !$this->config->item('require_login')): ?>
					<!-- <input style="font-size:16px;" type="image" src="<?php echo base_url();?>images/check_out.png" onclick="$('#redirect_path').val('checkout');" value="Checkout &raquo;"/> -->
					<a href="<?php echo base_url();?>checkout/shipping"><img src="<?php echo base_url();?>images/check_out.png" alt="check_out" /></a>
					<?php //endif; ?>
					<!-- <a href="#"><img src="<?php echo base_url();?>images/check_out.png" alt="check_out" /></a> -->
				</div>			
        </div><!-- End of check_wrap -->
		
		
		
        
		</form>

    	<?php endif; ?>
    
    
		<div class="clear"></div>
     </div><!-- End of Container -->	
  <div class="clear"></div>
 <div id="banner_wrap">
<div style="position:absolute;" id="banner">
	<div id="free_shipping">
    	<div class="banner_image">
        	<a href="#"><img width="40" height="40" alt="free shipping" src="<?php echo base_url();?>images/free_ship.png"/></a>
        </div>
        <div class="banner_link">
        	<a href="#">Free Shipping </a>
        </div>
  </div><!-- End of free_shipping -->
	<div id="free_return">
    	<div class="banner_image">
        	<a href="#"><img width="40" height="40" alt="free shipping" src="<?php echo base_url();?>images/free_return.png"/></a>
        </div>
        <div class="banner_link">
        	<a href="#">Free Returns </a>
        </div>
  </div><!-- End of free_shipping -->  
  <div id="customer_suport">
    	<div class="banner_image">
        	<a href="#"><img width="40" height="40" alt="free shipping" src="<?php echo base_url();?>images/customer_suport.png"/></a>
        </div>
        <div class="banner_link">
        	<a href="#">24/7 Customer Service   1-800-333-001 </a>
        </div>
  </div><!-- End of free_shipping --> 
</div><!-- End of banner -->
</div> 
</div><!-- End of Main Wrapper -->

<div class="clear"></div>

<script type="text/javascript">
	jQuery('.buttonset').buttonset();
</script>
<?php include('footer.php'); ?>