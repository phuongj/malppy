<?php



?>
<?php $this_customer = $this->go_cart->customer(); ?>
<h2 class="title"><?php echo $this_customer['lastname']; ?>'s Wish List</h2>

							<div class="addthis">
							<div id="social_sharing">
							<!-- AddThis Button BEGIN -->
							<div class="addthis_toolbox addthis_default_style ">
							<a class="addthis_button_preferred_1"></a>
							<a class="addthis_button_preferred_2"></a>
							<a class="addthis_button_preferred_3"></a>
							<a class="addthis_button_preferred_4"></a>
							<a class="addthis_button_compact"></a>
							<a class="addthis_counter addthis_bubble_style"></a>
							
							</div>
							<script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=xa-4e4ed7263599fdd0"></script>
							<!-- AddThis Button END -->
						    </div> <!-- End of social_sharing --> 
 						   </div> <!-- End of addthis -->   

<br><br>

<table class="cart_table" cellpadding="0" cellspacing="0" border="0">
	<thead>
		<tr>
			<th class="product_info">Product Name</th>
			<th>Product Code</th>
			<th></th>
		</tr>
	</thead>

	<tbody class="cart_items" style="text-align:left;">
	<?php
	foreach($wishlists as $wishlist): ?>
		<tr class="cart_spacer"><td colspan="7"></td></tr>
		<tr class="cart_item">
			<td>
				<?php $p = $this->Product_model->get_product($wishlist->product_id); 
				
				echo strip_tags($p->name);
				
				?>
			</td>
			<td><?php echo $p->sku; ?></td>
			<td><a href="<?php echo site_url($p->slug); ?>">View Detail</a></td>
		</tr>
		
	<?php endforeach;?>
	</tbody>
</table>