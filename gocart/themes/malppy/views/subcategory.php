<?php include('header.php'); ?>
<style>
.top_border {
    border-bottom: 1px solid #D5D5D5;
    margin: 10px 20px;
}
#breadcrumb {
    margin: 0 20px;
    padding-bottom: 5px;
    padding-top: 10px;
}
#cat_container {
    margin-top: -31px;
	width: 999px;
}

#slider_wrap {
	height:auto;
}
.product_wrap {
	background-color: #FFFFFF;
    width: 100%;
}
#sort_products {
	font-size:11px;
}
</style>
<style type="text/css">
.ui-widget-content { border: 1px solid #ccc; background: #797979; color: #222222; }
.ui-widget-header { border: 1px solid #aaaaaa; background: #CC0000; color: #222222; font-weight: bold; }

.ui-slider { position: relative; text-align: left; }
.ui-slider .ui-slider-handle { position: absolute; z-index: 2; width: 5px; height: 5px	; cursor: default; }
.ui-slider .ui-slider-range { position: absolute; z-index: 1; font-size: .7em; display: block; border: 0; background-position: 0 0; }

.ui-slider-horizontal { height: 3px; }
.ui-slider-horizontal .ui-slider-handle { top: -.1em; margin-left: -.2em; }
.ui-slider-horizontal .ui-slider-range { top: 0; height: 100%;  }
.ui-slider-horizontal .ui-slider-range-min { left: 0;  }
.ui-slider-horizontal .ui-slider-range-max { right: 0;}
</style>
	<script type="text/javascript">
	var startprice = '<?php echo number_format($startprice,0,'.',''); ?>';
	var endprice = '<?php if ($endprice) { echo number_format($endprice,0,'.',''); } else { echo '1000'; } ?>';

	jQuery(function() {
		jQuery("#slider-range").slider({
			range: true,
			min: 0,
			max: 1000,
			values: [startprice, endprice],
			slide: function(event, ui) {
				jQuery("#amount").val('BND' + ui.values[0] + ' - BND' + ui.values[1]);
				//console.log(ui);
				setTimeout(function() { sliderMethod(event, ui); }, 2000); // delay trigger for 2s
			}
		});
		jQuery("#amount").val('BND' + jQuery("#slider-range").slider("values", 0) + ' - BND' + jQuery("#slider-range").slider("values", 1));
	});

	function sliderMethod(event, ui) {
		//console.log(ui);
		window.location='<?php echo site_url(uri_string());?>/?'+'pricerange='+ui.values[0]+'/'+ui.values[1];
	}
	</script>
	<div class="main_wrap">
	<div id="cat_container">
		<div id="breadcrumb">
            <ul style="float:left;">           
				<li><a href="<?php echo base_url();?>">Home</a></li>				<?php 									$leaf_node = $this->Category_model->get_breadcrumb($category->id);										$child_counts = count((array)$leaf_node)/2;					$isLast = NO;										for ($i = 1; $i < $child_counts; $i++) {											$namevar = 'level'.$i;						$cat_name = $leaf_node->$namevar;												$slugvar = 'slug'.$i;						$slug = $leaf_node->$slugvar;						$li_property = "";						$a_style = "";												if ($cat_name == $category->name) {							$isLast = YES;							$prop= 'class="last"';							$a_style = 'style="color:#006699"';						}												echo '<li '.$prop.'><a '.$a_style.' href="'.$slug.'">'.$cat_name.'</a></li>';												if ($isLast == YES) {							break;						}											}								?>								
            </ul>
			<span style="float:right;"><img src="<?php echo base_url();?>images/header_fb.png" width="100" height="25" alt="fb" /></span>
        	<div class="clear"></div>
        	<div class="category_name"><?php echo $category->name; ?></div>
        </div><!-- End of breadcrumb --> 

		<div class="top_border"></div>

    	<div id="cat_first_portion">
		<div id="shop_by">
        	<?php		function display_sidecategories($cats, $layer, $first='',$include='')		{			if($first)			{				echo '<ul '.$first.'>';			}			foreach ($cats as $cat)			{								echo '<li><a href="'.site_url($cat['category']->slug).'">'.$cat['category']->name.'</a></li>';																if ($cat['category']->id == $include) {															display_sidecategories($cat['children'], 1,'class="nav"');									}							}			if($first)			{				echo '</ul>';			}			}
		
		$parentsub = $this->Category_model->get_categories_tierd_filtered(0);
		//print_r($parentsub);
		display_sidecategories($parentsub[$category->parent_id]['children'], 1,'class="side"',$category->id); ?>
            
          
      </div><!-- End of shop_by -->
        
        <div id="slider_wrap" style="width:805px;background:none;">
        	<div id="slconlinks_wrap">
         		<div id="slconlinks_controler">
        			<div id="slconlinks"></div>
        			
	            </div><!-- End of slconlinks_controler -->
    	    </div><!-- End of slconlinks_wrap -->

			<p class="result_text">Total <?php echo count($products); ?> products found.
				<?php if (isset($startprice) && isset($endprice)) { ?>
				Showing product price from BND<?php echo $startprice; ?> to BND<?php echo $endprice; ?>.
				<?php } ?>
			</p>
	
			<div class="pull-right" style="margin-top:20px;margin-right:35px;float:right">
					<p style="font-size: 10px; float:left;">Sort by:</p>
					<select id="sort_products" onchange="window.location='<?php echo site_url(uri_string());?>/?'+jQuery(this).val();">
						<option value=''></option>
						<option<?php echo(!empty($_GET['by']) && $_GET['by']=='name/asc')?' selected="selected"':'';?> value="by=name/asc">Product Name (A > Z)</option>
						<option<?php echo(!empty($_GET['by']) && $_GET['by']=='name/desc')?' selected="selected"':'';?>  value="by=name/desc">Product Name (Z > A)</option>
						<option<?php echo(!empty($_GET['by']) && $_GET['by']=='price/asc')?' selected="selected"':'';?>  value="by=price/asc">Price (Low > High)</option>
						<option<?php echo(!empty($_GET['by']) && $_GET['by']=='price/desc')?' selected="selected"':'';?>  value="by=price/desc">Price (High > Low)</option>
					</select>
				</div>
        	<div class="clear"></div>

        	<div id="product_wrap">
				
				

				<div id="products_list_portion"  style="padding:20px 20px 10px;margin:0 auto;text-align:center;width:100%;"> <!-- Products -->
	
		<?php if(count($products) > 0):?>
		<div class="clear"></div>
		<div class="pagination">
		<?php echo $this->pagination->create_links();?>
		</div>
		<?php		
		$cat_counter = 1;
		foreach($products as $product):
			if($cat_counter == 1):
			?>
			
			<div class="category_container">
			
			<?php endif;?>
			
			<div class="product_box">
				<div class="thumbnail">
					<?php
					$photo	= '<img src="'.base_url('images/nopicture.png').'" alt="'.lang('no_image_available').'"/>';
					$product->images	= array_values($product->images);
						
					if(!empty($product->images[0]))
					{
						$primary	= $product->images[0];
						foreach($product->images as $photo)
						{
							if(isset($photo->primary))
							{
								$primary	= $photo;
							}
						}
						if (strpos($primary->filename, 'http') === 0) {
						$photo	= '<img src="'.base_url().'phpthumb/phpThumb.php?src='.$primary->filename.'&w=170&h=220&zc=1&f=png" width="170" height="220" alt="'.$product->seo_title.'"/>';
						} else {
						$photo	= '<img src="'.base_url().'phpthumb/phpThumb.php?src='.base_url('uploads/images/small/'.$primary->filename).'&w=170&h=220&zc=1&f=png" width="170" height="220" alt="'.$product->seo_title.'"/>';
						}

						//$photo	= '<img src="'.base_url('uploads/images/thumbnails/'.$primary->filename).'" alt="'.$product->seo_title.'"/>';
					}
					?>
					<a href="<?php echo site_url($product->slug); ?>">
						<?php echo $photo; ?>
					</a>
				</div>
				<div class="product_name">
					<a href="<?php echo site_url($product->slug); ?>"><?php echo $product->name;?></a>
				</div>
				
				<div>
				<div class="price_container">
					<?php if($product->promoprice > 0):?>
						<span class="price_slash"><?php echo format_currency($product->saleprice); ?></span>
						<span class="price_sale">Now <?php echo format_currency($product->promoprice); ?></span>
					<?php else: ?>
						<span class="price_reg"><?php echo format_currency($product->saleprice); ?></span>
					<?php endif; ?>
				</div>
                    
				</div>
			</div>
			
			<?php 
			$cat_counter++;
			if($cat_counter == 5):?>

			</div>

			<?php 
			$cat_counter = 1;
			endif;
		endforeach;
			
		if($cat_counter != 1):?>
				<br class="clear"/>
			</div>
		<?php endif;?>
		
		<div class="gc_pagination">
		<?php echo $this->pagination->create_links();?>
		</div>
		<div class="clear"></div>
	<?php endif; ?>

		</div> <!-- End of Products -->

        	</div><!-- End of slider -->  
	
        </div><!-- End of slider_wrap -->

        
        
        <div class="clear"></div>
	</div><!-- End of First Portion -->

<div class="clear"></div>

	
	<div style="height:80px;"></div>
  <div class="clear"></div>
  <div id="sale_wrap" style="margin-bottom:10px;">
  	<div id="sale" style="position:absolute;">
   	  	<a href="<?php echo base_url();?>sales"><img src="<?php echo base_url();?>images/sale.png" width="1030" height="120" alt="sale" /></a>
      </div><!-- End of Sales -->	
  </div>  <!-- End of sale_wrap -->	
  
     </div><!-- End of Portion -->	
  <div class="clear"></div>
</div><!-- End of Main Wrapper -->

<?php include('footer.php'); ?>