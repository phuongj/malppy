<?php include('header.php'); ?>

<style>
#slider_wrap {
	background:none;
    background-position: 0 0;
    background-repeat: no-repeat;
    height: 360px;
    padding-left: 5px;
    width: 998px;
}
.right_main_pics {
	border: medium none;
    float: right;
    margin-right: 5px;
    margin-top: -2px;
    width: 300px;
    height: 340px;
}
.small_pic {
    float: right;
}
</style>

<div class="main_wrap">
	<div id="index_container">
    	<div id="first_portion" style="padding-top: 6px;">
        
        
        <div id="slider_wrap" style="width:690px;float:left;">
        
        		<div id="slconlinks_wrap">
         	<div id="slconlinks_controler" style="margin: 270px 0 0 170px;">
        		<div id="slconlinks"></div>
            </div><!-- End of slconlinks_controler -->
        </div><!-- End of slconlinks_wrap -->
        	<div id="slider" style="width:690px; height:355px;">
				<?php 
				$banner_count	= 1;
				foreach ($banners as $banner)
				{
					echo '<div class="banner_container">';
		
					if($banner->link != '')
					{
						$target	= false;
						if($banner->new_window)
						{
							$target = 'target="_blank"';
						}
						echo '<a href="'.$banner->link.'" '.$target.' >';
					}
					//echo '<img class="banners_img'.$banner_count.'" src="'.base_url('uploads/'.$banner->image).'" />';
					echo '<img class="banners_img'.$banner_count.'" src="'.base_url().'phpthumb/phpThumb.php?src='.base_url('uploads/'.$banner->image).'&w=690&h=355&zc=1" />';
	
					if($banner->link != '')
					{
						echo '</a>';
					}
	
					echo '</div>';
	
					//$banner_count++;
				}
				?>                            	
        	</div><!-- End of slider -->  
			
        </div><!-- End of slider_wrap -->
		<div class="right_main_pics">
				<?php 
				$smallbanner_count	= 1;
				foreach ($smallbanners as $banner)
				{
					echo '<div class="small_pic">';
		
					if($banner->link != '')
					{
						$target	= false;
						if($banner->new_window)
						{
							$target = 'target="_blank"';
						}
						echo '<a href="'.$banner->link.'" '.$target.' >';
					}
					echo '<img src="'.base_url().'phpthumb/phpThumb.php?src='.base_url('uploads/'.$banner->image).'&w=300&h=180&zc=1" width="300" height="175" alt="" border="0" />';
	
					if($banner->link != '')
					{
						echo '</a>';
					}
	
					echo '</div>';
	
					$banner_count++;
				}
				?> 
                  <!-- <div class="small_pic"><a href="#"><img src="images/item_small_protion1.jpg" width="300" height="210" alt="small_protion" /></a></div>
                  <div class="small_pic"><a href="#"><img src="images/item_small_protion2.jpg" width="300" height="210" alt="small_protion" /></a></div> -->
        </div>
        
       <div class="clear"></div> 
	</div><!-- End of First Portion -->

<div class="clear"></div>
<div id="banner_wrap">
<div id="banner" style="position:absolute;">
	<div id="free_shipping">
    	<div class="banner_image">
        	<a href="<?php echo base_url();?>free-shipping"><img src="<?php echo base_url();?>images/free_ship.png" width="40" height="40" alt="free shipping" /></a>
        </div>
        <div class="banner_link">
        	<a href="<?php echo base_url();?>free-shipping">Free Shipping </a>
        </div>
  </div><!-- End of free_shipping -->
	<div id="free_return">
    	<div class="banner_image">
        	<a href="<?php echo base_url();?>free-returns"><img src="<?php echo base_url();?>images/free_return.png" width="40" height="40" alt="free shipping" /></a>
        </div>
        <div class="banner_link">
        	<a href="<?php echo base_url();?>free-returns">Free Returns </a>
        </div>
  </div><!-- End of free_shipping -->  
  <div id="customer_suport">
    	<div class="banner_image">
        	<a href="<?php echo base_url();?>customer-service-center"><img src="<?php echo base_url();?>images/customer_suport.png" width="40" height="40" alt="free shipping" /></a>
        </div>
        <div class="banner_link">
        	<a href="<?php echo base_url();?>customer-service-center">24/7 Customer Service   1-800-333-001 </a>
        </div>
  </div><!-- End of free_shipping --> 
</div><!-- End of banner -->
</div><!-- End of banner wrap -->

<div class="clear"></div>


<div id="new_arrival_wap" >
    
             <div id="new_arival_top">
				<p>NEW ARRIVALS</p>
				
           	</div><!-- End of new_arival_top -->
        
       <ul class="switchable-triggers"><li>Women</li><li>Men</li><li>Baby</li></ul><div id="scrollable-1">
    <ul style="position: absolute; left: 0px; width: 100%; transition-property: none; transition-duration: 500ms; transition-timing-function: ease-in-out;">
      
      <?php 
      
      foreach($categories_new_arrivals as $product) {

		if (strpos($product->product_image, 'http') === 0) {
						$photo	= '<img src="'.base_url().'phpthumb/phpThumb.php?src='.$product->product_image.'&w=182&h=237&zc=1&f=png" width="182" height="237" alt="'.$product->seo_title.'"/>';
						} else {
						$photo	= '<img src="'.base_url().'phpthumb/phpThumb.php?src='.base_url('uploads/images/small/'.$product->product_image).'&w=182&h=237&zc=1&f=png" width="182" height="237" alt="'.$product->seo_title.'"/>';
						}
						
		?>
		
		<li><div class="categories_new_arrival_product">
		
				<a href="<?php echo site_url($product->slug); ?>">
						<?php echo $photo; ?>
				</a>
				
				<div id="bottom_block"><p id="new_arrv_product_name"><?php echo $product->name;?></p><p id="new_arrv_product_price">$ <?php echo $product->saleprice; ?></p></div>
			</div>
				
		</li> 
					
					
					
			<?php 
	  }
      ?>
      
      
    </ul>
  </div>
        
    
</div><!-- End of new_arrival_wap -->

<?php 
	foreach ($boxes as $box)
	{
		$section[$box->catid][$box->id]['id'] = $box->id;
		$section[$box->catid][$box->id]['image'] = $box->image;
		$section[$box->catid][$box->id]['primary'] = $box->primary;
		$section[$box->catid][$box->id]['link'] = $box->link;
		$this_cat = $this->Category_model->get_category($box->catid);
		$section_cat[$box->catid]['name'] = $this_cat->name;
		$section_cat[$box->catid]['id'] = $box->catid;
		//$section[$box->catid]['category'] = 
		/*echo '<div class="box_container">';
		
		if($box->link != '')
		{
			$target	= false;
			if($box->new_window)
			{
				$target = 'target="_blank"';
			}
			echo '<a href="'.$box->link.'" '.$target.' >';
		}
		echo '<img src="'.base_url('uploads/'.$box->image).'" />';
		
		if($box->link != '')
		{
			echo '</a>';
		}

		echo '</div>';*/
	}
	?>
	
	
<?php  
$i=0;
foreach ($section_cat as $scat):
if (empty($scat['name'])) { continue; }
$primary = "";
foreach ($section[$scat['id']] as $sec) {
	if($sec['primary']==1) {
		$primary	= $sec['image'];
		$primary_link = $sec['link'];
		$target = "";
		break;
	}	
}
?>
 <div class="clear"></div>
	
<?php if ($i==2) { ?>
<div class="clear"></div>
  <div id="sale_wrap">
  	<div id="sale" style="position:absolute;">
   	  	<a href="<?php echo base_url();?>sales"><img src="images/sale.png" width="1030" height="120" alt="sale" /></a>
      </div><!-- End of Sales -->	
  </div>  <!-- End of sale_wrap -->	
<?php } ?>
	
<?php $i++; endforeach; ?>

  <div class="clear"></div>   
        <!-- <div class="promotion_wrapr_spcl">
        <div class="promotion_heading_no_bg">
        	<h1>Promotions Items!</h1>
         </div>
            <div class="promotion_pics">
                  <div class="main_promotion"><a href="#"><img src="images/item_promotion.jpg" width="580" height="340"  alt="promotion"/></a></div>
                  <div class="small_promotion"><a href="#"><img src="images/item_small_protion1.jpg" width="210" height="170" alt="small_protion" /></a></div>
                  <div class="small_promotion"><a href="#"><img src="images/item_small_protion2.jpg" width="210" height="170" alt="small_protion" /></a></div>
                  <div class="small_promotion"><a href="#"><img src="images/item_small_protion3.jpg" width="210" height="170" alt="small_protion" /></a></div>
                  <div class="small_promotion"><a href="#"><img src="images/item_small_protion4.jpg" width="210" height="170" alt="small_protion" /></a></div>   
            </div><1!-- End of item --1>
	</div><1!-- End of promotion_pics --1>    -->
  <div class="clear"></div>
     </div><!-- End of Portion -->	
  <div class="clear"></div>
</div><!-- End of Main Wrapper -->
<div class="clear"></div>
	  

<?php include('footer.php'); ?>
<?php exit; ?>
<?php ob_start();?>
<script type="text/javascript">
var rotate;
$(document).ready(function(){
	$('.banner_container').each(function(item)
	{
		if(item != 0)
		{
			$(this).hide();
		}
	});
	if($('.banner_container').size() > 1)
	{
		rotate_banner();
	}
	
});

var cnt	= 0;

function rotate_banner()
{
	//stop the animations from going nuts when returning from minimize
	$('.banner_container:eq('+cnt+')').fadeOut();
	cnt++;
	if(cnt == $('.banner_container').size())
	{
		cnt = 0;
	}
	$('.banner_container:eq('+cnt+')').fadeIn(function(){
		setTimeout("rotate_banner()", 3000);
	});
}
</script>

<?php
$ads_javascript	= ob_get_contents();
ob_end_clean();


$additional_header_info = $ads_javascript;

include('header.php'); ?>

<div id="banners">
	<?php 
	$banner_count	= 1;
	foreach ($banners as $banner)
	{
		echo '<div class="banner_container">';
		
		if($banner->link != '')
		{
			$target	= false;
			if($banner->new_window)
			{
				$target = 'target="_blank"';
			}
			echo '<a href="'.$banner->link.'" '.$target.' >';
		}
		echo '<img class="banners_img'.$banner_count.'" src="'.base_url('uploads/'.$banner->image).'" />';
		
		if($banner->link != '')
		{
			echo '</a>';
		}

		echo '</div>';

		$banner_count++;
	}
	?>
</div><!--ads end-->

<div id="homepage_boxes">
	<?php 
	foreach ($boxes as $box)
	{
		echo '<div class="box_container">';
		
		if($box->link != '')
		{
			$target	= false;
			if($box->new_window)
			{
				$target = 'target="_blank"';
			}
			echo '<a href="'.$box->link.'" '.$target.' >';
		}
		echo '<img src="'.base_url('uploads/'.$box->image).'" />';
		
		if($box->link != '')
		{
			echo '</a>';
		}

		echo '</div>';
	}
	?>
</div>

<?php include('footer.php'); ?>