// JavaScript Document

var s = jQuery.noConflict();
s(document).ready(function() {
    s('#slider').cycle({
	fx:     'scrollHorz', 
    speed:  'slow',
	delay: 5000,
    timeout: 5000,
	next:   '#ar_left', 
    prev:   '#ar_right',
	pager:   '#slconlinks',
	cleartype:0
        //pagerAnchorBuilder: pagerFactory
		
		});
	});