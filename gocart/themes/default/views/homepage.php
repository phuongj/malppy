<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="css/style.css" type="text/css" media="screen, projection" />
<!--[if IE]>
<link rel="stylesheet" href="css/style_ie.css" type="text/css" media="screen, projection" />
<![endif]-->
<link href="css/skins/grey.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="js/custom_jquery.js"></script>
<script type='text/javascript' src='js/jquery.hoverIntent.minified.js'></script>
<script type='text/javascript' src='js/jquery.dcmegamenu.1.3.3.js'></script>
<script type="text/javascript" src="js/custom_menu.js"></script>
<script type="text/JavaScript" src="js/cycle.js"></script>
<script type="text/JavaScript" src="js/custom_cycle.js"></script>

<link href="css/flexipage.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.flexipage.js"></script>

<script type="text/javascript">
	var carous = jQuery.noConflict();
	carous(document).ready(function() {
      carous('.arival').flexipage({
           perpage:5,
           carousel: true,
  		   speed:1000,
  		   animation: "swing"
       });
	});
</script>

<title>malppy</title>
</head>
<body>
<div id="header">
    <div class="main_wrap">
    	
        <div id="logo">
    	  	<a href="<?php echo base_url();?>">
			<img src="<?php echo base_url('images/logo.png');?>" alt="<?php echo $this->config->item('company_name'); ?>">
			</a>
        </div><!-- End of logo -->
        <div id="fb_top">
   	    	<img src="images/header_fb.png" width="100" height="25" alt="fb" />
        </div><!-- End of fb_top -->
        <div id="header_right">
        	
            <div id="top_items_n_check_out">
            	<div id="mytrolly"><a href="#"><img src="images/trolly.png" width="20" height="20" alt="troly" /></a></div>
                <ul>
                	
                    <li><a href="#"><span class="c_db6363 space">2 items </span>  in your cart</a></li>
                    <li><a href="#">Checkout!</a></li>
                </ul>
            </div><!-- End of top_items_n_check_out -->
            
            <div id="reg_now">
            	<span>Not yet register? <a href="#"> Register now. </a></span>       
            </div><!-- End of reg_now -->
            
            <div class="clear"></div>
            <div id="search">
				<?php echo form_open('cart/search');?>
            		<input name="term" id="key" type="text" onblur="if (this.value == '') {this.value = 'Shoes, Clothing, Bags, etc.';}" 
                	onfocus="if(this.value == 'Shoes, Clothing, Bags, etc.') {this.value = '';}" value="Shoes, Clothing, Bags, etc."/>
                	<input id="submit" type="submit"  value="&nbsp;"/>
               </form>
            </div>
        </div><!-- End of header_right -->
  </div><!-- End of Main Wrapper -->
<div class="clear"></div>  

  <div id="nav_wrap">
      <div id="nav">
      		<div class="grey">  
                <ul id="mega-menu-3" class="mega-menu">
					<?php
		function display_categories($cats, $layer, $first='')
		{
			if($first)
			{
				echo '<ul'.$first.'>';
			}
			
			foreach ($cats as $cat)
			{

				echo '<li><a href="'.site_url($cat['category']->slug).'">'.$cat['category']->name.'</a>'."\n";
				if (sizeof($cat['children']) > 0)
				{
					if($layer == 1)
					{
						$next = $layer+1;
						display_categories($cat['children'], $next, ' class="first"');
					}
					else
					{
						$next = $layer+1;
						display_categories($cat['children'], $next, ' class="nav"');
					}
				}
				echo '</li>';
			}
			if($first)
			{
				echo '</ul>';
			}	
		}
			
		display_categories($this->categories, 1);
		
		
		
		if($gift_cards_enabled):?>
			<li><a href="<?php echo site_url('cart/giftcard');?>"><?php echo lang('giftcard');?></a></li>
		<?php endif;?>
                    <!-- <li><a href="#">WOMEN'S</a></li>
                    <li><a href="#">MEN'S</a>
                        <ul>
                            <li><h6> Handbags</h6>
                                <ul>
                                    <li><a href="#">Shoulder Bags</a></li>
                                    <li><a href="#">Totes</a></li>
                                    <li><a href="#">Clutches</a></li>
                                    <li><a href="#">Hobos</a></li>
                                    <li><a href="#">New Arrivals</a></li>
                                    <li><a href="#">Sale &amp; Clearance</a></li>
                                    <li><a href="#">View all...</a></li>
                                    
								</ul>
                                <h6> Luggage</h6>
                                <ul>
                                    <li><a href="#">Carry Ons </a></li>
                                    <li><a href="#">Travel Accessories</a></li>
                                    <li><a href="#">Travel Totes</a></li>
                                    <li><a href="#">Upright Suitcases</a></li>
                                    <li><a href="#">New Arrivals</a></li>
                                    <li><a href="#">Sale &amp; Clearance</a></li>
                                    <li><a href="#">View all...</a></li>
                                    
                                </ul>                                
                                
                            </li>
                            <li><h6>Briefcase</h6>
                                <ul>
                                    <li><a href="#">New Arrivals Sale &amp; Clearance</a></li>
                                </ul>
                                <h6>Travel Bags</h6>
                                <ul>
                                    <li><a href="#">Travel Accessories</a></li>
                                    <li><a href="#">Travel Totes</a></li>
                                    <li><a href="#">Carry Ons </a></li>
                                    <li><a href="#">Upright Suitcases</a></li>
                                    <li><a href="#">New Arrivals</a></li>
                                    <li><a href="#">View all...</a></li>
                                </ul>
                                <h6>Wallets</h6>
                                <ul>
                                    <li><a href="#">Checkbook Wallets </a></li>
                                    <li><a href="#">Bi-Folds </a></li>
                                    <li><a href="#">Coin &amp; Card Cases </a></li>
                                    <li><a href="#">View all...</a></li>
                                </ul>
                                
                            </li>
                            <li class="promo"><img src="images/menu_image.png" width="234" height="310" alt="pic" /></li>
                            
                        </ul>
                        </li>
                        
                        <li><a href="#">BABY'S</a></li>
                        <li><a href="#">FOOTWEAR</a></li>
                        <li><a href="#">FASHION ACCESSORIES</a></li>
                        <li><a href="#">NOVELTIES</a></li>
                        <li><a href="#">SALE!</a>
                            <ul>
                            <li><h6> Handbags</h6>
                                <ul>
                                    <li><a href="#">Shoulder Bags</a></li>
                                    <li><a href="#">Totes</a></li>
                                    <li><a href="#">Clutches</a></li>
                                    <li><a href="#">Hobos</a></li>
                                    <li><a href="#">New Arrivals</a></li>
                                    <li><a href="#">Sale &amp; Clearance</a></li>
                                    <li><a href="#">View all...</a></li>
                                    
								</ul>
                                <h6> Luggage</h6>
                                <ul>
                                    <li><a href="#">Carry Ons </a></li>
                                    <li><a href="#">Travel Accessories</a></li>
                                    <li><a href="#">Travel Totes</a></li>
                                    <li><a href="#">Upright Suitcases</a></li>
                                    <li><a href="#">New Arrivals</a></li>
                                    <li><a href="#">Sale &amp; Clearance</a></li>
                                    <li><a href="#">View all...</a></li>
                                    
                                </ul>                                
                                
                            </li>
                            <li><h6>Briefcase</h6>
                                <ul>
                                    <li><a href="#">New Arrivals Sale &amp; Clearance</a></li>
                                </ul>
                                <h6>Travel Bags</h6>
                                <ul>
                                    <li><a href="#">Travel Accessories</a></li>
                                    <li><a href="#">Travel Totes</a></li>
                                    <li><a href="#">Carry Ons </a></li>
                                    <li><a href="#">Upright Suitcases</a></li>
                                    <li><a href="#">New Arrivals</a></li>
                                    <li><a href="#">View all...</a></li>
                                </ul>
                                <h6>Wallets</h6>
                                <ul>
                                    <li><a href="#">Checkbook Wallets </a></li>
                                    <li><a href="#">Bi-Folds </a></li>
                                    <li><a href="#">Coin &amp; Card Cases </a></li>
                                    <li><a href="#">View all...</a></li>
                                </ul>
                                
                            </li>
                            <li class="promo"><img src="images/menu_image.png" width="234" height="310" alt="pic" /></li>
                             
                        </ul>
                        
                        </li>-->
                        <li><a href="#">NEW ARRIVALS</a></li>
                   	 </ul>
			</div><!-- End of grey -->
      </div><!-- End of nav -->
  </div><!-- End of nav_wrap -->
</div><!-- End of header -->
<div class="clear"></div>

<div class="main_wrap">
	<div id="index_container">
    	<div id="first_portion">
        <div id="shop_by">
        	<h1>Shop by...</h1>
            <h2>Mens</h2>
            <ul class="first">                      
               <li><a href="#">Hot Tees</a></li>
               <li><a href="#">Polo</a></li>
               <li><a href="#">Shirts</a></li>
               <li><a href="#">Executive</a></li>
               <li><a href="#">Super Streets</a></li>
               <li><a href="#">Golfer</a></li>
               <li><a href="#">Coats, Vest &amp; Blazers</a></li>
               <li><a href="#">Shorts</a></li>
               <li><a href="category.html">View all...</a></li>
			</ul>
            <div class="leftsidebar_border"></div>
            <h2>Women's</h2>
            <ul class="second">                     
               <li><a href="#">Hot &amp; Sexy</a></li>
               <li><a href="#">Young Women</a></li>
               <li><a href="#">Corporate</a></li>
               <li><a href="#">Mature</a></li>
               <li><a href="#">Slighly Bigger</a></li>
               <li><a href="#">Formal</a></li>
               <li><a href="#">Wedding</a></li>
               <li><a href="#">Pants</a></li>
               <li><a href="category.html">View all...</a></li>
			</ul>
      </div><!-- End of shop_by -->
        
        <div id="slider_wrap">
        
        		<div id="slconlinks_wrap">
         	<div id="slconlinks_controler">
        		<div id="slconlinks"></div>
            </div><!-- End of slconlinks_controler -->
        </div><!-- End of slconlinks_wrap -->
        	<div id="slider">
            	<!-- <img src="images/slider.jpg" width="800" height="419" alt="slider" />
                <img src="images/cat_slider.jpg" width="800" height="419" alt="slider" />
                <img src="images/slider.jpg" width="800" height="419" alt="slider" />
                <img src="images/cat_slider.jpg" width="800" height="419" alt="slider" />
                <img src="images/slider.jpg" width="800" height="419" alt="slider" /> -->
				<?php 
	$banner_count	= 1;
	foreach ($banners as $banner)
	{
		echo '<div class="banner_container">';
		
		if($banner->link != '')
		{
			$target	= false;
			if($banner->new_window)
			{
				$target = 'target="_blank"';
			}
			echo '<a href="'.$banner->link.'" '.$target.' >';
		}
		echo '<img class="banners_img'.$banner_count.'" src="'.base_url('uploads/'.$banner->image).'" />';
		
		if($banner->link != '')
		{
			echo '</a>';
		}

		echo '</div>';

		$banner_count++;
	}
	?>

                            	
        	</div><!-- End of slider -->  
        </div><!-- End of slider_wrap -->

        
        
	</div><!-- End of First Portion -->

<div class="clear"></div>
<div id="banner_wrap">
<div id="banner" style="position:absolute;">
	<div id="free_shipping">
    	<div class="banner_image">
        	<a href="#"><img src="images/free_ship.png" width="40" height="40" alt="free shipping" /></a>
        </div>
        <div class="banner_link">
        	<a href="#">Free Shipping </a>
        </div>
  </div><!-- End of free_shipping -->
	<div id="free_return">
    	<div class="banner_image">
        	<a href="#"><img src="images/free_return.png" width="40" height="40" alt="free shipping" /></a>
        </div>
        <div class="banner_link">
        	<a href="#">Free Returns </a>
        </div>
  </div><!-- End of free_shipping -->  
  <div id="customer_suport">
    	<div class="banner_image">
        	<a href="#"><img src="images/customer_suport.png" width="40" height="40" alt="free shipping" /></a>
        </div>
        <div class="banner_link">
        	<a href="#">24/7 Customer Service   1-800-333-001 </a>
        </div>
  </div><!-- End of free_shipping --> 
</div><!-- End of banner -->
</div><!-- End of banner wrap -->

<div class="clear"></div>


<div id="new_arrival_wap" >
    <div id="new_arrival" style="position:absolute;">
             <div id="new_arival_top">
				<h1>NEW ARRIVALS</h1>
           	</div><!-- End of new_arival_top -->
            
        <div id="new_product_wrap">
            <ul class="arival">
                <li><img src="images/new_arival1.jpg" width="180" height="250" alt="new arival" /><div class="price_tag">BND 29.90</div></li>
                <li><img src="images/new_arival2.jpg" width="180" height="250" alt="new arival" /><div class="price_tag">BND 29.90</div></li>
                <li><img src="images/new_arival3.jpg" width="180" height="250" alt="new arival" /><div class="price_tag">BND 19.90</div></li>
                <li><img src="images/new_arival4.jpg" width="180" height="250" alt="new arival" /><div class="price_tag">BND 29.90</div></li>
                <li><img src="images/new_arival5.jpg" width="180" height="250" alt="new arival" /><div class="price_tag">BND 39.90</div></li>
                <li><img src="images/new_arival4.jpg" width="180" height="250" alt="new arival" /><div class="price_tag">BND 49.90</div></li>
                <li><img src="images/new_arival3.jpg" width="180" height="250" alt="new arival" /><div class="price_tag">BND 59.90</div></li>
                <li><img src="images/new_arival2.jpg" width="180" height="250" alt="new arival" /><div class="price_tag">BND 69.90</div></li>
                <li><img src="images/new_arival1.jpg" width="180" height="250" alt="new arival" /><div class="price_tag">BND 79.90</div></li>
                <li><img src="images/new_arival5.jpg" width="180" height="250" alt="new arival" /><div class="price_tag">BND 89.90</div></li>
                <li><img src="images/new_arival1.jpg" width="180" height="250" alt="new arival" /><div class="price_tag">BND 99.90</div></li>
                <li><img src="images/new_arival2.jpg" width="180" height="250" alt="new arival" /><div class="price_tag">BND 09.90</div></li>
                <li><img src="images/new_arival3.jpg" width="180" height="250" alt="new arival" /><div class="price_tag">BND 19.90</div></li>
 
            </ul>
        </div>  <!-- End of new_product_wrap -->  	
    </div><!-- End of new_arrival -->
</div><!-- End of new_arrival_wap -->
<div class="clear"></div>
    <div class="promotion_wrapr">
        <div class="promotion_heading"><h1>Women</h1></div>
            <div class="promotion_pics">
                  <div class="main_promotion"><a href="#"><img src="images/women_promotion.jpg" width="580" height="340"  alt="promotion"/></a></div>
                  <div class="small_promotion"><a href="#"><img src="images/women_small_protion.jpg" width="210" height="170" alt="small_protion" /></a></div>
                  <div class="small_promotion"><a href="#"><img src="images/women_small_protion2.jpg" width="210" height="170" alt="small_protion" /></a></div>
                  <div class="small_promotion"><a href="#"><img src="images/women_small_protion3.jpg" width="210" height="170" alt="small_protion" /></a></div>
                  <div class="small_promotion"><a href="#"><img src="images/women_small_protion4.jpg" width="210" height="170" alt="small_protion" /></a></div>   
            </div><!-- End of promotion_pics -->
	</div><!-- End of promotion_wrapr -->
 <div class="clear"></div>
    <div class="promotion_wrapr">
        <div class="promotion_heading"><h1>Men</h1></div>
            <div class="promotion_pics">
                  <div class="main_promotion"><a href="#"><img src="images/men_promotion.jpg" width="580" height="340"  alt="promotion"/></a></div>
                  <div class="small_promotion"><a href="#"><img src="images/men_small_protion1.jpg" width="210" height="170" alt="small_protion" /></a></div>
                  <div class="small_promotion"><a href="#"><img src="images/men_small_protion2.jpg" width="210" height="170" alt="small_protion" /></a></div>
                  <div class="small_promotion"><a href="#"><img src="images/men_small_protion3.jpg" width="210" height="170" alt="small_protion" /></a></div>
                  <div class="small_promotion"><a href="#"><img src="images/men_small_protion4.jpg" width="210" height="170" alt="small_protion" /></a></div>   
            </div><!-- End of promotion_pics -->
	</div><!-- End of promotion_wrapr -->
 <div class="clear"></div>
    <div class="promotion_wrapr">
        <div class="promotion_heading"><h1>Baby</h1></div>
            <div class="promotion_pics">
                  <div class="main_promotion"><a href="#"><img src="images/baby_promotion.jpg" width="580" height="340"  alt="promotion"/></a></div>
                  <div class="small_promotion"><a href="#"><img src="images/baby_small_protion1.jpg" width="210" height="170" alt="small_protion" /></a></div>
                  <div class="small_promotion"><a href="#"><img src="images/baby_small_protion2.jpg" width="210" height="170" alt="small_protion" /></a></div>
                  <div class="small_promotion"><a href="#"><img src="images/baby_small_protion3.jpg" width="210" height="170" alt="small_protion" /></a></div>
                  <div class="small_promotion"><a href="#"><img src="images/baby_small_protion4.jpg" width="210" height="170" alt="small_protion" /></a></div>   
            </div><!-- End of promotion_pics -->
	</div><!-- End of promotion_wrapr -->   
  <div class="clear"></div>
  <div id="sale_wrap">
  	<div id="sale" style="position:absolute;">
   	  	<a href="#"><img src="images/sale.png" width="1030" height="120" alt="sale" /></a>
      </div><!-- End of Sales -->	
  </div>  <!-- End of sale_wrap -->	
  <div class="clear"></div>
    <div class="promotion_wrapr">
        <div class="promotion_heading"><h1>Footwear</h1></div>
            <div class="promotion_pics">
                  <div class="main_promotion"><a href="#"><img src="images/footwear_promotion.jpg" width="580" height="340"  alt="promotion"/></a></div>
                  <div class="small_promotion"><a href="#"><img src="images/footwear_small_protion1.jpg" width="210" height="170" alt="small_protion" /></a></div>
                  <div class="small_promotion"><a href="#"><img src="images/footwear_small_protion2.jpg" width="210" height="170" alt="small_protion" /></a></div>
                  <div class="small_promotion"><a href="#"><img src="images/footwear_small_protion3.jpg" width="210" height="170" alt="small_protion" /></a></div>
                  <div class="small_promotion"><a href="#"><img src="images/footwear_small_protion4.jpg" width="210" height="170" alt="small_protion" /></a></div>
            </div><!-- End of promotion_pics -->
	</div><!-- End of promotion_wrapr -->
 <div class="clear"></div> 
    <div class="promotion_wrapr">
        <div class="promotion_heading"><h1>Fashion</h1></div>
            <div class="promotion_pics">
                  <div class="main_promotion"><a href="#"><img src="images/fashion_promotion.jpg" width="580" height="340"  alt="promotion"/></a></div>
                  <div class="small_promotion"><a href="#"><img src="images/fashion_small_protion.jpg" width="210" height="170" alt="small_protion" /></a></div>
                  <div class="small_promotion"><a href="#"><img src="images/fashion_small_protion2.jpg" width="210" height="170" alt="small_protion" /></a></div>
                  <div class="small_promotion"><a href="#"><img src="images/fashion_small_protion3.jpg" width="210" height="170" alt="small_protion" /></a></div>
                  <div class="small_promotion"><a href="#"><img src="images/fashion_small_protion4.jpg" width="210" height="170" alt="small_protion" /></a></div>   
            </div><!-- End of promotion_pics -->
	</div><!-- End of promotion_wrapr -->
 <div class="clear"></div> 
    <div class="promotion_wrapr">
        <div class="promotion_heading"><h1>Novelties</h1></div>
            <div class="promotion_pics">
                <div class="main_promotion"><a href="#"><img src="images/novelties_promotion.jpg" width="580" height="340"  alt="promotion"/></a></div>
                <div class="small_promotion"><a href="#"><img src="images/novelties_small_protion1.jpg" width="210" height="170" alt="small_protion" /></a></div>
                <div class="small_promotion"><a href="#"><img src="images/novelties_small_protion2.jpg" width="210" height="170" alt="small_protion" /></a></div>
                <div class="small_promotion"><a href="#"><img src="images/novelties_small_protion3.jpg" width="210" height="170" alt="small_protion" /></a></div>
                <div class="small_promotion"><a href="#"><img src="images/novelties_small_protion4.jpg" width="210" height="170" alt="small_protion" /></a></div>
            </div><!-- End of promotion_pics -->
	</div><!-- End of promo-->
  <div class="clear"></div>   
        <div class="promotion_wrapr_spcl">
        <div class="promotion_heading_no_bg">
        	<h1>Promotions Items!</h1>
         </div>
            <div class="promotion_pics">
                  <div class="main_promotion"><a href="#"><img src="images/item_promotion.jpg" width="580" height="340"  alt="promotion"/></a></div>
                  <div class="small_promotion"><a href="#"><img src="images/item_small_protion1.jpg" width="210" height="170" alt="small_protion" /></a></div>
                  <div class="small_promotion"><a href="#"><img src="images/item_small_protion2.jpg" width="210" height="170" alt="small_protion" /></a></div>
                  <div class="small_promotion"><a href="#"><img src="images/item_small_protion3.jpg" width="210" height="170" alt="small_protion" /></a></div>
                  <div class="small_promotion"><a href="#"><img src="images/item_small_protion4.jpg" width="210" height="170" alt="small_protion" /></a></div>   
            </div><!-- End of item -->
	</div><!-- End of promotion_pics -->   
  <div class="clear"></div>
     </div><!-- End of Portion -->	
  <div class="clear"></div>
</div><!-- End of Main Wrapper -->
<div class="clear"></div>
	  <div  id="clearance_social">
        			<div id="clearance"><img src="images/clearance.png" width="200" height="300" alt="clearance" /></div>
                    <div id="freeshipping"><img src="images/freeshiping.png" width="200" height="300" alt="freeshipping" /></div>
                    <div id="fblike_box"><img src="images/fblike_box.png" width="260" height="300" alt="fb" /></div>
				<div id="subscribe">
                    	<h1>Subscribe to our fun and informative weekly newsletter</h1>
                        <div id="subscrb"><input type="text" /></div>
                        <div id="newsltr_submit"><button type="submit">&nbsp;</button></div>
                        <div class="clear"></div>
        				<p>Personal data will not be disclosed to any third parties. You can unsubscribe from our newsletter at any time. Thanks a lot !</p>
        				<img src="images/maplpy.png" width="270" height="37" />   
        		</div><!-- End of subscribe -->
        </div><!-- End of clearance_social -->
		<div class="clear"></div>
        <div id="index_txt_conent">
        	<p><strong>Malppy.com - Online Shopping Simplified for You.</strong></p>

<p>We are trend-setters in shopping online in Brunei. There are three main reasons: renowned brands, multiple options and value-added services that cater to our customers. Our collection of major brands, international and local, caters to both high-end and affordable looks. There are so many options for you to choose from when browsing our site. The fashionable range caters to all age groups and backgrounds, and it's categorized for easier browsing by price, size, color and design to can ensure a shopping experience that doesn't waste your time! Just hover your mouse over any image to see the product from 360 degree angles.</p>

<p>Our site not only provides gorgeous products for our customers, but also FREE shipping nationwide, a 30 days return policy and a customer service hotline to answer any inquiries and assist your online shopping experience. If you are not satisfied with your purchase, you may return your order within 30 days subject to our Terms &amp; Conditions. Feel free to contact Customer Service at 03 2382 2208, and they will be delighted to help with any inquiries. We are here to make your shopping experience a relaxed, simple and fun process.</p>

<p>Got inquiries? Send us an e-mail at <span style="text-decoration:underline">customer@malppy.com</span></p>
        </div><!-- End of index_txt_conent -->
   
<div class="clear"></div>

<div id="footer_wrap">

 
 

 


	
    <div id="footer_cus_serv">
    	<h1>Customer Services</h1>
        <ul>
        	<li><a href="#">Contact Info</a></li>
            <li><a href="#">Returns Shipping</a></li>
            <li><a href="#">FAQ's </a></li>
            <li><a href="#">Customer Service Center</a></li>
            <li><a href="#">Safe Shopping Guarantee </a></li>
            <li><a href="#">Secure Shopping</a></li>            
        </ul>
	
    </div><!-- End of footer_cus_serv -->
    
    <div id="footer_about">
    	<h1>About</h1>
        <ul>
        	<li><a href="#">MALPPY</a></li>
            <li><a href="#">Careers</a></li>
			<li><a href="#">Press/Media</a></li> 
            <li><a href="#">Terms &amp; Conditions</a></li>
            <li><a href="#">CarePrivacy Policy ers</a></li>       
        </ul>	
    </div><!-- End of footer_about -->    




   
  <div id="footer_join_us">
   	<h1>Join Us On</h1>
      <ul>
       	  <li><a href="#"><img src="images/footer_fb.png" width="105" height="25" alt="fb" /></a></li>
      </ul>	
    </div><!-- End of footer_join_us -->
    
     <div id="footer_shop_us">
      <h1>Shop With Us</h1>
      <ul>
       	  <li><a href="#">Women</a></li>
          <li><a href="#">Men</a></li>
          <li><a href="#">Baby</a></li>
          <li><a href="#">Footwear</a></li>
          <li><a href="#">Fashion Accessories</a></li>
          <li><a href="#">Novelties</a></li>
          <li><a href="#">SALE!</a></li>
          <li><a href="#">New Arrivals</a></li>
          
      </ul>	
	
    </div><!-- End of footer_shop_us -->
    
     <div id="footer_payment_method">
     <h1>Our Payment Methods</h1>
     <div class="pay_method"><a href="#"><img src="images/visa.png" width="32" height="23" alt="visa" /></a></div>
     <div class="pay_method"><a href="#"><img src="images/master.png" width="32" height="23" alt="visa" /></a></div>
     <div class="pay_method"><a href="#"><img src="images/paypal.png" width="32" height="23" alt="visa" /></a></div>
     <div class="pay_method"><a href="#"><img src="images/cash.png" width="32" height="23" alt="visa" /></a></div>
     <div class="pay_method"><a href="#"><img src="images/master2.png" width="32" height="23" alt="visa" /></a></div>
     <div class="footer_space"></div>
     <div class="clear"></div>
     	<h1>We deliver by</h1>
    	<img src="images/ups.png" width="75" height="75" alt="ups" />
    </div><!-- End of footer_payment_method -->    
	
    <div class="clear"></div>   
    <div class="footer_copyright">     
    	COPYRIGHT (C) MALPPY.COM. LTD. ALL RIGHTS RESERVED
    </div>
    
</div><!-- End of footer_wrap -->

</body>
</html>
<?php exit; ?>
<?php ob_start();?>
<script type="text/javascript">
var rotate;
$(document).ready(function(){
	$('.banner_container').each(function(item)
	{
		if(item != 0)
		{
			$(this).hide();
		}
	});
	if($('.banner_container').size() > 1)
	{
		rotate_banner();
	}
	
});

var cnt	= 0;

function rotate_banner()
{
	//stop the animations from going nuts when returning from minimize
	$('.banner_container:eq('+cnt+')').fadeOut();
	cnt++;
	if(cnt == $('.banner_container').size())
	{
		cnt = 0;
	}
	$('.banner_container:eq('+cnt+')').fadeIn(function(){
		setTimeout("rotate_banner()", 3000);
	});
}
</script>

<?php
$ads_javascript	= ob_get_contents();
ob_end_clean();


$additional_header_info = $ads_javascript;

include('header.php'); ?>

<div id="banners">
	<?php 
	$banner_count	= 1;
	foreach ($banners as $banner)
	{
		echo '<div class="banner_container">';
		
		if($banner->link != '')
		{
			$target	= false;
			if($banner->new_window)
			{
				$target = 'target="_blank"';
			}
			echo '<a href="'.$banner->link.'" '.$target.' >';
		}
		echo '<img class="banners_img'.$banner_count.'" src="'.base_url('uploads/'.$banner->image).'" />';
		
		if($banner->link != '')
		{
			echo '</a>';
		}

		echo '</div>';

		$banner_count++;
	}
	?>
</div><!--ads end-->

<div id="homepage_boxes">
	<?php 
	foreach ($boxes as $box)
	{
		echo '<div class="box_container">';
		
		if($box->link != '')
		{
			$target	= false;
			if($box->new_window)
			{
				$target = 'target="_blank"';
			}
			echo '<a href="'.$box->link.'" '.$target.' >';
		}
		echo '<img src="'.base_url('uploads/'.$box->image).'" />';
		
		if($box->link != '')
		{
			echo '</a>';
		}

		echo '</div>';
	}
	?>
</div>

<?php include('footer.php'); ?>