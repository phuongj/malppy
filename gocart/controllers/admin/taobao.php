<?php

class Taobao extends Admin_Controller {	
	
	private $use_inventory = false;
	
	function __construct()
	{		
		parent::__construct();
		remove_ssl();
		
		//$this->auth->check_access('Admin', true);
		$this->auth->check_access(array('Admin', 'Content'), true);
		$this->load->model('Taobao_model');
		$this->load->model('Product_model');
		$this->load->model('Settings_model');
		$this->load->model('Audit_model');
		
		//$this->load->helper('form');
		$this->lang->load('product');
		$this->load->helper( array('form', 'image') );
	}

	function index()
	{
		$data['page_title']	= 'Taobao';
		$data['TaobaokeItem'] = "";
		
		$currency = $this->Settings_model->get_settings('currency');
		$data['bnd_to_rmb']	= $currency['bnd_to_rmb'];
		
		if (isset($_REQUEST['submit'])) {
			$result = $this->search();
			
			$data['keyword']= $_REQUEST['keyword'];
			
			//Gets the error message
			//$sub_msg = @$result['sub_msg'];
			$data['sub_msg']= @$result['sub_msg'];
			
			//Return result
			//$TaobaokeItem = @$result['taobaoke_items']['taobaoke_item'];
			//$TaobaokeCount = @$result['total_results'];
			$data['TaobaokeItem']= @$result['taobaoke_items']['taobaoke_item'];
			$data['TaobaokeCount']= @$result['total_results'];
		}
		
		$this->load->view($this->config->item('admin_folder').'/taobao_search', $data);
	}

	function category($id = false)
	{
		$data['page_title']	= lang('products');
		
		$data['products']	= $this->Category_model->get_category_products_taobao($id);

		if ($id=='-1') {
			$data['products']	= $this->Category_model->get_category_products_empty();
		}

		$currency = $this->Settings_model->get_settings('currency');
		$data['bnd_to_rmb']	= $currency['bnd_to_rmb'];
		
		$data['catid'] = $id;

		$this->load->view($this->config->item('admin_folder').'/taobao_inventory', $data);
	}

	function auto_stock_update()
	{
		$data['products']	= $this->Taobao_model->get_products();

		foreach($data['products'] AS $product) {
			$this->check_stock($product->num_iid);
		}		
	}
	
	function check_stock($num_iid = false)
	{
		if ($num_iid) {	
			$paramArr = array(
					'method' => 'taobao.taobaoke.items.detail.get',   //API name
			     'timestamp' => date('Y-m-d H:i:s'),			
				    'format' => 'xml',  //Return format, this demo supports only XML
		    	   'app_key' => $this->config->item('taobao_appKey'),  //Appkey			
			    		 'v' => '2.0',   //API version number		   
				'sign_method'=> 'md5', //Signature method
					'fields' => 'iid,detail_url,num_iid,title,nick,type,cid,seller_cids,props,input_pids,input_str,desc,pic_url,num,valid_thru,list_time,delist_time,stuff_status,location,price,post_fee,express_fee,ems_fee,has_discount,freight_payer,has_invoice,has_warranty,has_showcase,modified,increment,auto_repost,approve,status,postage_id,product_id,auction_point,property_alias,item_imgs,prop_imgs,skus,outer_id,is_virtual,is_taobao,is_ex,videos,is_3D,score,volume,one_station,click_url,shop_click_url,seller_credit_score,approve_status', //Returns the field
			      'num_iids' => $num_iid, //num_iid
				      'nick' => $this->config->item('taobao_userNick'), //Promoter Nick
			);
			
			//Generating signatures
			$sign = $this->createSign($paramArr,$this->config->item('taobao_appSecret'));
			
			//Organizational parameters
			$strParam = $this->createStrParam($paramArr);
			$strParam .= 'sign='.$sign;
			
			//Construct Url
			$urls = $this->config->item('taobao_url').$strParam;
			
			//Connection timeout auto retry
			$cnt=0;	
			while($cnt < 3 && ($result=$this->vita_get_url_content($urls))===FALSE) $cnt++;
			
			//Parsing Xml data
			$result = $this->getXmlData($result);
			
			//Gets the error message
			//$sub_msg = $result['sub_msg'];
			
			//Return result
			$taobaokeItemdetail = $result['taobaoke_item_details']['taobaoke_item_detail']['item'];
			$taobaokeItem = $result['taobaoke_item_details']['taobaoke_item_detail'];
			
			$data['num_iid'] = $num_iid;
			$data['original_name'] = mysql_real_escape_string($taobaokeItemdetail['title']);
			$data['quantity'] = mysql_real_escape_string($taobaokeItemdetail['num']);
			if (!$data['quantity']) $data['quantity'] = 0;
			$data['stock_updated'] = date("Y-m-d H:i:s");

			$this->Taobao_model->save($data);
			
			//$this->session->set_flashdata('message', 'Product ['.$num_iid.'] stock updated.');
			
			//redirect($this->config->item('admin_folder').'/taobao/inventory');
			
			//return $data;
			//$this->load->view($this->config->item('admin_folder').'/taobao_detail', $data);
		}
	}

	function cron_stock() {
		$data['page_title']		= "";
		$this->load->model('Product_model');
		
		$data['products']	= $this->Product_model->get_products();
		
		$this->load->view($this->config->item('admin_folder').'/taobao_stock', $data);
	}
	
	
	function search($id = false)
	{
		$this->product_id	= $id;
		
		$data['page_title']		= lang('product_form');
		
		$currency = $this->Settings_model->get_settings('currency');
		$data['bnd_to_rmb']	= $currency['bnd_to_rmb'];
		
		if ($id) {	
			$data['num_iid']		= $id;
			$this->load->view($this->config->item('admin_folder').'/taobao', $data);
		}
	}
	
	function search2()
	{
		$num_iid=!(isset($_GET['num_iid']))?'0':intval($_GET['num_iid']);
		$cid = !($_GET['cid'])?'0':intval($_GET['cid']);
		$parent_cid = !(isset($_GET['parent_cid']))?'0':intval($_GET['parent_cid']);	   
		$keyword = $_GET['keyword'];
		$start_price= !($_GET['start_price'])?'0.01':intval($_GET['start_price']);
		$end_price= !($_GET['end_price'])?'99999999':intval($_GET['start_price']);
		$auto_send = !(isset($_GET['auto_send']))?'':$_GET['auto_send'];	   //$_GET['auto_send'];
		$area = $_GET['area'];
		$start_credit= !(isset($_GET['start_credit']))?'1heart':$_GET['start_credit'];
		$end_credit= !(isset($_GET['end_credit']))?'5goldencrown':$_GET['end_credit'];	  
		$sort = $_GET['sort'];
		$guarantee =!(isset($_GET['guarantee']))?'':$_GET['guarantee'];//$_GET['guarantee'];
		$start_commissionRate= !(isset($_GET['start_commissionRate']))?'150':intval($_GET['start_commissionRate']);
		$end_commissionRate= !(isset($_GET['end_commissionRate']))?'5000':intval($_GET['end_commissionRate']);
		$start_commissionNum= !(isset($_GET['start_commissionNum']))?'0':intval($_GET['start_commissionNum']);
		$end_commissionNum= !(isset($_GET['end_commissionNum']))?'99999999':intval($_GET['end_commissionNum']);
		$page_no = !(isset($_GET['page']))?'1':intval($_GET['page']);
		$page_size = !($_GET['page_size'])?'10':intval($_GET['page_size']);
		
		/* Build By weikiat */
		
	/* Specified contain Taobao commodity category or list Start*/
	
		//Parameter arrays
		$paramArr = array(
	
			/* API systems-level input parameter Start */
	
		    	'method' => 'taobao.taobaoke.items.get',  //API name
		     //'timestamp' => date('Y-m-d H:i:s'),			
			 	'timestamp' => '2012-09-14 09:03:15',
			    'format' => 'xml',  //Return format, this demo supports only XML
	    	   'app_key' => $this->config->item('taobao_appKey'),  //Appkey			
		    		 'v' => '2.0',   //API version number		   
			'sign_method'=> 'md5', //Signature method			
	
			/* API system-level parameters End */				 
	
			/* Application API-level input parameter Start*/
	
		    	'fields' =>  'iid,num_iid,title,nick,pic_url,price,click_url,commission,commission_rate,commission_num,commission_volume,shop_click_url,seller_credit_score,volume,item_location',  //Returns the field
		    	  'nick' => $this->config->item('taobao_userNick'),  //Promote its nickname
	   	       'keyword' => $keyword,  //Query keywords	
		    	   'cid' => $cid,  //Item class ID 		   
		   'start_price' => $start_price, //Starting price
		     'end_price' => $end_price, //Highest price
			 'auto_send' => $auto_send, //Auto shipping
			 	  'area' => $area, //Location of goods such as: Hangzhou
		  'start_credit' => $start_credit, //Sellers start credit
		    'end_credit' => $end_credit,  //Sellers the highest credit
		   		  'sort' => $sort, //Sort by
			 'guarantee' => $guarantee, //Query whether eliminating sellers
	'start_commissionRate'=>$start_commissionRate, //Starting Commission rate options
	 'end_commissionRate'=> $end_commissionRate, //High Commission rates
	'start_commissionNum'=> $start_commissionNum, //Start Trojan promotion options
	 'end_commissionNum' => $end_commissionNum, //Highest cumulative amount of promotion
			   'page_no' => $page_no, //Results pages. 1~99
	  	     'page_size' => $page_size , //Number of results to return per page. maximum per-page 40 
					
			/* API-level input parameters End*/
		);
		
		//Generating signatures
		$sign = $this->createSign($paramArr,$this->config->item('taobao_appSecret'));
		
		//Organizational parameters
		$strParam = $this->createStrParam($paramArr);
		$strParam .= 'sign='.$sign;
		
		//Construct Url
		$url = $this->config->item('taobao_url').$strParam;
		
		//Connection timeout auto retry
		$cnt=0;	
		while($cnt < 3 && ($result=@$this->vita_get_url_content($url))===FALSE) $cnt++;
		
		//Parsing Xml data
		$result = $this->getXmlData($result);
		
		return $result;
		
		//redirect($this->config->item('admin_folder').'/taobao');
	}
	
	function inventory($sort_by='createdate',$sortorder='desc', $code=0, $page=0, $rows=20)
	{
		$data['page_title']		= "Inventory";

		$data['code']		= $code;
		$term				= false;
		
		$post	= $this->input->post(null, false);
		if($post)
		{
			//if the term is in post, save it to the db and give me a reference
			$term	= json_encode($post);
			$data['code']	= $this->Search_model->record_term($term);
			
			//reset the term to an object for use
			$term	= (object)$post;
		}
		elseif ($code)
		{
			$term	= $this->Search_model->get_term($code);
			$term	= json_decode($term);
		} 
 		
 		$data['term']	= $term;
 		$data['products']	= $this->Taobao_model->get_products($term, $sort_by, $sortorder, $rows, $page);
		$data['total']	= $this->Taobao_model->get_products_count($term);

		$this->load->library('pagination');
		
		$config['base_url']	= site_url($this->config->item('admin_folder').'/taobao/inventory/'.$sort_by.'/'.$sortorder.'/'.$code.'/');
		$config['total_rows']		= $data['total'];
		$config['per_page']			= $rows;
		$config['uri_segment']		= 7;

		$config['full_tag_open']	= '<div class="pagination"><ul>';
		$config['full_tag_close']	= '</ul></div>';
		$config['cur_tag_open']		= '<li class="active"><a href="#">';
		$config['cur_tag_close']	= '</a></li>';
		
		$config['num_tag_open']		= '<li>';
		$config['num_tag_close']	= '</li>';
		
		$config['prev_link']		= '&laquo;';
		$config['prev_tag_open']	= '<li>';
		$config['prev_tag_close']	= '</li>';

		$config['next_link']		= '&raquo;';
		$config['next_tag_open']	= '<li>';
		$config['next_tag_close']	= '</li>';
		
		$this->pagination->initialize($config);
	
		$data['sort_by']	= $sort_by;
		$data['sortorder']	= $sortorder;
		$data['pages']		= $this->pagination->create_links();
		$data['page'] 	= $page;
		
		//$data['products']	= $this->Taobao_model->get_products();
		
		$currency = $this->Settings_model->get_settings('currency');
		$data['bnd_to_rmb']	= $currency['bnd_to_rmb'];

		$this->load->view($this->config->item('admin_folder').'/taobao_inventory', $data);
	}

	function saved_list($sort_by='createdate',$sortorder='desc', $code=0, $page=0, $rows=20)
	{
		$data['page_title']		= "Saved List";

		$data['code']		= $code;
		$term				= false;
		
		$post	= $this->input->post(null, false);
		if($post)
		{
			//if the term is in post, save it to the db and give me a reference
			$term	= json_encode($post);
			$data['code']	= $this->Search_model->record_term($term);
			
			//reset the term to an object for use
			$term	= (object)$post;
		}
		elseif ($code)
		{
			$term	= $this->Search_model->get_term($code);
			$term	= json_decode($term);
		} 
 		
 		$data['term']	= $term;
 		$data['products']	= $this->Taobao_model->get_saved_list($term, $sort_by, $sortorder, $rows, $page);
		$data['total']	= $this->Taobao_model->get_saved_list_count($term);

		$this->load->library('pagination');
		
		$config['base_url']	= site_url($this->config->item('admin_folder').'/taobao/saved_list/'.$sort_by.'/'.$sortorder.'/'.$code.'/');
		$config['total_rows']		= $data['total'];
		$config['per_page']			= $rows;
		$config['uri_segment']		= 7;

		$config['full_tag_open']	= '<div class="pagination"><ul>';
		$config['full_tag_close']	= '</ul></div>';
		$config['cur_tag_open']		= '<li class="active"><a href="#">';
		$config['cur_tag_close']	= '</a></li>';
		
		$config['num_tag_open']		= '<li>';
		$config['num_tag_close']	= '</li>';
		
		$config['prev_link']		= '&laquo;';
		$config['prev_tag_open']	= '<li>';
		$config['prev_tag_close']	= '</li>';

		$config['next_link']		= '&raquo;';
		$config['next_tag_open']	= '<li>';
		$config['next_tag_close']	= '</li>';
		
		$this->pagination->initialize($config);
	
		$data['sort_by']	= $sort_by;
		$data['sortorder']	= $sortorder;
		$data['pages']		= $this->pagination->create_links();
		$data['page'] 	= $page;
		
		//$data['products']	= $this->Taobao_model->get_products();
		
		$currency = $this->Settings_model->get_settings('currency');
		$data['bnd_to_rmb']	= $currency['bnd_to_rmb'];

		$this->load->view($this->config->item('admin_folder').'/saved_list', $data);
	}
	
	function detail($id = false, $duplicate = false)
	{
		$this->product_id	= $id;
		
		$data['page_title']		= lang('product_form');
		
		if ($id) {	
			$data['num_iid']		= $id;
			$this->load->view($this->config->item('admin_folder').'/taobao_detail', $data);
		}
	}
	
	function bookmark($num_iid = false, $bulk = false)
	{
		$save['id'] = '';
		
		if ($num_iid) {	
			$save['num_iid'] = $num_iid;
			
			$paramArr = array(
					'method' => 'taobao.taobaoke.items.detail.get',   //API name
			     'timestamp' => date('Y-m-d H:i:s'),			
				    'format' => 'xml',  //Return format, this demo supports only XML
		    	   'app_key' => $this->config->item('taobao_appKey'),  //Appkey			
			    		 'v' => '2.0',   //API version number		   
				'sign_method'=> 'md5', //Signature method
					'fields' => 'iid,detail_url,num_iid,title,nick,type,cid,seller_cids,props,input_pids,input_str,desc,pic_url,num,valid_thru,list_time,delist_time,stuff_status,location,price,post_fee,express_fee,ems_fee,has_discount,freight_payer,has_invoice,has_warranty,has_showcase,modified,increment,auto_repost,approve,status,postage_id,product_id,auction_point,property_alias,item_imgs,prop_imgs,skus,outer_id,is_virtual,is_taobao,is_ex,videos,is_3D,score,volume,one_station,click_url,shop_click_url,seller_credit_score,approve_status', //Returns the field
			      'num_iids' => $num_iid, //num_iid
				      'nick' => $this->config->item('taobao_userNick'), //Promoter Nick
			);
			
			//Generating signatures
			$sign = $this->createSign($paramArr,$this->config->item('taobao_appSecret'));
			
			//Organizational parameters
			$strParam = $this->createStrParam($paramArr);
			$strParam .= 'sign='.$sign;
			
			//Construct Url
			$urls = $this->config->item('taobao_url').$strParam;
			
			//Connection timeout auto retry
			$cnt=0;	
			while($cnt < 3 && ($result=$this->vita_get_url_content($urls))===FALSE) $cnt++;
			
			//Parsing Xml data
			$result = $this->getXmlData($result);
			
			//Gets the error message
			//$sub_msg = $result['sub_msg'];
			
			//Return result
			$taobaokeItemdetail = $result['taobaoke_item_details']['taobaoke_item_detail']['item'];
			$taobaokeItem = $result['taobaoke_item_details']['taobaoke_item_detail'];
			
			$pat = "/<(\/?)(script|i?frame|style|html|body|title|link|a|meta|\?|\%)([^>]*?)>/isU";
			
			$save['original_name'] = mysql_real_escape_string($taobaokeItemdetail['title']);
			$save['product_name'] = mysql_real_escape_string($taobaokeItemdetail['title']);
			//$save['product_name'] = $this->Newiconv("UTF-8", "GBK",$taobaokeItemdetail['title']);
			
			$save['price'] = $taobaokeItemdetail['price'];
			
			$pat = "/<(\/?)(script|i?frame|style|html|body|title|link|a|meta|\?|\%)([^>]*?)>/isU";
			$descclear = preg_replace($pat,"",$taobaokeItemdetail['desc']);
			
			$save['description'] = mysql_real_escape_string(stripslashes($descclear));
			$save['original_description'] = mysql_real_escape_string(stripslashes($descclear));
			$save['quantity'] = mysql_real_escape_string($taobaokeItemdetail['num']);
			$save['location'] = mysql_real_escape_string(trim($taobaokeItemdetail['location']['city']." ".$taobaokeItemdetail['location']['state']));
			
			$save['seller'] = mysql_real_escape_string(trim($taobaokeItemdetail['nick']));
			$save['shop_url'] = mysql_real_escape_string(trim($taobaokeItem['shop_click_url']));
			$save['pic_url'] = mysql_real_escape_string(trim($taobaokeItemdetail['pic_url']));

			$save['new'] = 1;
			$save['createdate'] = date("Y-m-d H:i:s");
			$save['updated']  	= date("Y-m-d H:i:s");
			$save['adminid'] 	= $this->auth->get_adminid();
			
			//$post_images = array();
			
			$dom = new domDocument;
			@$dom->loadHTML($save['description']);
			$dom->preserveWhiteSpace = false;
			$images = $dom->getElementsByTagName('img');
			$i=0;
			foreach ($images as $image) {
			  if (!$image->getAttribute('src')) continue;
			  $i++;
			  $img_file = stripslashes(str_replace('"',"",$image->getAttribute('src')));
			  $img_arr[] = $img_file;
			  
			  $image_id = time()+$i;
			  $post_images[$image_id]['filename'] = $img_file;
			  /*$file_loc = "uploads/images/full/".basename($img_file);
			  
			  $img_file=file_get_contents($img_file);
			  $file_handler=fopen($file_loc,'w');
			  fclose($file_handler);*/
			  
			}
			$save['images']	= json_encode($post_images);
			
			$this->Taobao_model->save($save);

			/* Audit Trail Log */
			$admin_id = $this->session->sess_match_useragent; 	// Action by which user?
			$action_remark = "imported a product"; 			// Action description
			$table_name = "taobao"; 						// Affected table name
			$row_id = $num_iid; 							// Affected row id
			$this->Audit_model->log_action($admin_id, $action_remark, $table_name, $row_id);
			/* End of Audit Trail */
			
			if ($bulk == false) {
				redirect($this->config->item('admin_folder').'/taobao/inventory');
			}
			//$this->load->view($this->config->item('admin_folder').'/taobao_detail', $data);
		}
	}

	function bulk_import()
	{
		$products	= $this->input->post('product');
		
		if(!$products)
		{
			$this->session->set_flashdata('error',  lang('error_bulk_no_products'));
			redirect($this->config->item('admin_folder').'/taobao');
		}
				
		foreach($products as $id=>$product)
		{
			$this->bookmark($product,true);
		}
		
		$this->session->set_flashdata('message', lang('message_bulk_update'));
		redirect($this->config->item('admin_folder').'/taobao/inventory');
	}

	function bulk_delete()
	{
		$products	= $this->input->post('product');
		
		if(!$products)
		{
			$this->session->set_flashdata('error',  'There are no products selected to bulk delete');
			redirect($this->config->item('admin_folder').'/taobao/inventory');
		}
				
		foreach($products as $num_iid=>$product)
		{
			$product['num_iid']	= $num_iid;

			$this->delete($num_iid, false);
		}
		
		$this->session->set_flashdata('message', lang('message_bulk_update'));
		redirect($this->config->item('admin_folder').'/taobao/inventory');
	}

	function bulk_add()
	{
		$list	= $this->input->post('approve');

		if(!$list)
		{
			$this->session->set_flashdata('error',  lang('error_bulk_no_products'));
			redirect($this->config->item('admin_folder').'/taobao/saved_list');
		}
				
		foreach($list as $num_iid=>$l)
		{
			$save['num_iid']	= $num_iid;	
			$save['is_saved'] = 0;	
			$save['updated']	= date("Y-m-d H:i:s");		
			$this->Taobao_model->save($save);
		}
		
		$this->session->set_flashdata('message', lang('message_bulk_update'));
		redirect($this->config->item('admin_folder').'/taobao/saved_list');
	}

	function bulk_save()
	{
		$products	= $this->input->post('product2');
		$approve	= $this->input->post('approve');

		if(!$products)
		{
			$this->session->set_flashdata('error',  lang('error_bulk_no_products'));
			redirect($this->config->item('admin_folder').'/taobao/inventory');
		}
				
		foreach($products as $num_iid=>$product)
		{
			$product['num_iid']	= $num_iid;	
			$product['updated']	= date("Y-m-d H:i:s");		
			$this->Taobao_model->save($product);

			if ($approve[$num_iid]['enabled']	== 1) {
				$this->insert($num_iid);
			}
		}
		
		$this->session->set_flashdata('message', lang('message_bulk_update'));
		redirect($this->config->item('admin_folder').'/taobao/inventory');
	}
	
	function add()
	{
		$num_iid = "mpy".rand(0,999999);
		$this->lang->load('digital_product');
		$data['num_iid'] = '';
		$data['page_title']		= "Add Product";
		$data['product_categories']	= array();
		$this->load->model('Option_model');
		
		$data['categories']		= $this->Category_model->get_categories_tierd();
		
		if ($num_iid) {	
			$data['num_iid'] = $num_iid;
			
			$product = $this->Taobao_model->get_product($data['num_iid']);

			// get product & options data
			$data['product_options']	= $this->Option_model->get_product_options($product->id);
						
			$data['id'] 					= $product->id;
			$data['original_name'] 			= $product->original_name;
			$data['name'] 					= $product->product_name;
			$data['description'] 			= $product->description;
			$data['excerpt'] 				= $product->excerpt;
			$data['info'] 					= $product->info;
			$data['specification'] 			= $product->specification;
			$data['original_description'] 	= $product->original_description;
			$data['images']					= (array)json_decode($product->images);

			$data['price']					= $product->price;
			$data['saleprice'] 				= $product->saleprice;
			$data['promoprice']				= $product->promoprice;
			$data['weight'] 				= $product->weight;
			$data['quantity'] 				= $product->quantity;
			$data['stock_updated']			= $product->stock_updated;

			$data['sale']					= $product->sale;
			$data['new']					= $product->new;

			$data['slug']					= $product->slug;
			$data['seo_title']				= $product->seo_title;
			$data['meta']					= $product->meta;

			
			$data['product_categories']	= $product->categories;			
		} 

		$currency = $this->Settings_model->get_settings('currency');
		$data['bnd_to_rmb']	= $currency['bnd_to_rmb'];
			
		$this->load->view($this->config->item('admin_folder').'/taobao_form', $data);
		
		if(!is_array($data['product_categories']))
		{
			$data['product_categories']	= $product->categories;
		}
	}

	function edit($num_iid = false,$editing_mode)
	{
		$this->lang->load('digital_product');
		$data['num_iid'] = '';
		$data['page_title']		= "Edit Product";
		$data['product_categories']	= array();
		$this->load->model('Option_model');
		$data['editing_mode'] = $editing_mode;
		$data['categories']		= $this->Category_model->get_categories_tierd();
		
		if ($num_iid) {	
			$data['num_iid'] = $num_iid;
			
			$product = $this->Taobao_model->get_product($data['num_iid']);

			// get product & options data
			$data['product_options']	= $this->Option_model->get_product_options($product->id);
						
			$data['id'] = $product->id;
			$data['original_name'] = $product->original_name;
			$data['name'] = $product->product_name;
			$data['description'] = $product->description;
			$data['excerpt'] = $product->excerpt;
			$data['info'] = $product->info;
			$data['specification'] = $product->specification;
			$data['original_description'] = $product->original_description;
			$data['images']	= (array)json_decode($product->images);

			$data['price']				= $product->price;
			$data['saleprice'] = $product->saleprice;
			$data['promoprice']			= $product->promoprice;
			$data['weight'] = $product->weight;
			$data['quantity'] = $product->quantity;
			$data['stock_updated']		= $product->stock_updated;

			$data['sale']				= $product->sale;
			$data['new']				= $product->new;

			$data['slug']				= $product->slug;
			$data['seo_title']			= $product->seo_title;
			$data['meta']				= $product->meta;
			
			$data['product_categories']	= $product->categories;

			$currency = $this->Settings_model->get_settings('currency');
			$data['bnd_to_rmb']	= $currency['bnd_to_rmb'];
			
			$this->load->view($this->config->item('admin_folder').'/taobao_form', $data);
		} 
		
		if(!is_array($data['product_categories']))
		{
			$data['product_categories']	= $product->categories;
		}
	}

	function description($num_iid = false)
	{
		$this->lang->load('digital_product');
		$data['num_iid'] = '';
		$data['page_title']		= "Edit Taobao Product";
		$data['product_categories']	= array();
		
		$data['categories']		= $this->Category_model->get_categories_tierd();
		
		if ($num_iid) {	
			$data['num_iid'] = $num_iid;
			
			$product = $this->Taobao_model->get_product($data['num_iid']);
			
			$data['id'] = $product->id;
			$data['name'] = $product->product_name;
			$data['description'] = $product->description;
			$data['excerpt'] = $product->excerpt;
			$data['info'] = $product->info;
			$data['specification'] = $product->specification;
			$data['original_description'] = $product->original_description;
			$data['images']	= (array)json_decode($product->images);
			
			$data['product_categories']	= $product->categories;
			
			$this->load->view($this->config->item('admin_folder').'/iframe/taobao_description', $data);
		} 
		
		if(!is_array($data['product_categories']))
		{
			$data['product_categories']	= $product->categories;
		}
	}
	
	function insert($num_iid = false)
	{
		$data['num_iid'] = '';
		$data['page_title']		= "Add Taobao Product";
		$data['product_categories']	= array();
		$wanted_post_images = array();
		$this->load->model('Option_model');
		
		if ($num_iid) {	
			$data['num_iid'] = $num_iid;
			
			$product = $this->Taobao_model->get_product($data['num_iid']);
			
			$save['id'] = '';
			$save['name'] = stripslashes($product->product_name);
			$save['description'] = stripslashes($product->description);
			$save['excerpt'] = stripslashes($product->excerpt);
			$save['info'] = stripslashes($product->info);
			$save['specification'] = stripslashes($product->specification);
			$save['price']		= $product->price;
			$save['saleprice']	= $product->saleprice;
			$save['promoprice']	= $product->promoprice;
			$save['quantity'] = $product->quantity;
			$save['enabled'] 	= 1;
			$save['track_stock'] 	= 1;
			$save['images']		= $product->images;
			$save['num_iid'] 	= $product->num_iid;

			$save['new']		= $product->new;
			$save['sale']		= $product->sale;

			$save['weight']		= $product->weight;

			$save['seo_title']	= $product->seo_title;
			$save['meta']		= $product->meta;
			$save['slug']		= $product->slug;

			$save['sku']		= $product->sku;

			$save['created']	= $product->createdate;
			$save['updated']	= date("Y-m-d H:i:s");
			$save['adminid']	= $product->adminid;
						
			$this->load->model(array('Product_model'));
			
			//save categories
			/*$categories			= $this->input->post('categories');
			$catid = $categories[0];
				
			if($this->Taobao_model->get_product($num_iid)->sku == "" && $catid) {
				$sku_no = $this->Category_model->get_sku($catid);
				$save['sku'] = $sku_no;
			} else {
				$save['sku'] = $this->Taobao_model->get_product($num_iid)->sku;
			}*/
			
			$save['seller']	= $this->Taobao_model->get_product($num_iid)->seller;
			
			
			if ($product->images) {
				$post_images = (array)json_decode($product->images);
				
				foreach ($post_images AS $postid => $post_image) {
					if (!isset($post_image->wanted) || $post_image->wanted!=1) continue;
					$wanted_post_images[$postid]['wanted'] = 1;
					$wanted_post_images[$postid]['filename'] = $post_image->filename;
					$wanted_post_images[$postid]['alt'] = "";
					$wanted_post_images[$postid]['caption'] = "";
				}
			}
			
			if ($wanted_post_images) $save['images'] = json_encode($wanted_post_images); // filter unwanted images
			
			//$this->save($num_iid, false);

			$product_options	= $this->Option_model->get_product_options($product->id);

			// format options
			$count	= 0;
			if(!empty($product_options)):
				foreach($product_options as $option):
					$option	= (object)$option;
						
					if(empty($option->required))
					{
						$option->required = false;
					}
					$options[$count]['name'] = $option->name;
					$options[$count]['type'] = $option->type;
							
					$options[$count]['required'] = $option->required;
								
					if(!empty($option->values))
						$valcount = 0;
						foreach($option->values as $value) : 
							$value = (object)$value;
								$options[$count][values][$valcount]['name'] = $value->name;
						
								$options[$count][values][$valcount]['value'] = $value->value;
								$options[$count][values][$valcount]['price'] = $value->price;
		
										
								$options[$count][values][$valcount]['limit'] = $value->limit;
							$valcount++;
						endforeach; 
													
					$count++; 
				endforeach;
			endif;

			$this->load->helper('text');

			//first check the slug field
			$slug = $save['slug'];
			
			//if it's empty assign the name field
			if(empty($slug) || $slug=='')
			{
				$slug = $save['name'];
			}
			
			$slug	= url_title(convert_accented_characters($slug), 'dash', TRUE);

			//validate the slug
			$this->load->model('Routes_model');

			$slug	= $this->Routes_model->validate_slug($slug);
				
			$route['slug']	= $slug;	
			$route_id	= $this->Routes_model->save($route);
			
			$save['slug']				= $slug;
			$save['route_id']			= $route_id;
		

			// save product 
			$product_id	= $this->Product_model->save($save, $options, $product->categories);

			//save the route
			$route['id']	= $route_id;
			$route['slug']	= $slug;
			$route['route']	= 'cart/product/'.$product_id;
			
			$this->Routes_model->save($route);
			
			$this->delete($num_iid, false);
			
			//$this->load->view($this->config->item('admin_folder').'/products', $data);
			//redirect($this->config->item('admin_folder').'/products/form/'.$product_id);
		}
	}
	
	function save($num_iid = false, $redirect = true)
	{
		$save['num_iid'] = '';
		$data['page_title']		= "Edit Taobao Product";
		$data['product_categories']	= array();
		
		if ($num_iid) {	
			$data['num_iid'] = $num_iid;
			$product = $this->Taobao_model->get_product($data['num_iid']);
			
			$save['num_iid']			= $num_iid;
			$save['product_name']		= $this->input->post('name');
			$save['description']		= $this->input->post('description');
			$save['excerpt']			= $this->input->post('excerpt');
			$save['info'] 				= $this->input->post('info');
			$save['specification'] 		= $this->input->post('specification');
			$post_images				= $this->input->post('images');

			$save['saleprice']			= $this->input->post('saleprice');
			$save['promoprice']			= $this->input->post('promoprice');
			$save['new']				= $this->input->post('new');
			$save['sale']				= $this->input->post('sale');
			$save['weight']				= $this->input->post('weight');

			$save['slug']				= $this->input->post('slug');
			$save['seo_title']			= $this->input->post('seo_title');
			$save['meta']				= $this->input->post('meta');

			$save['updated']			= date("Y-m-d H:i:s");

			
			$save['images']				= json_encode($post_images);

			//save categories
			$categories			= $this->input->post('categories');

			if (!$categories) {
				$this->session->set_flashdata('message', 'Category cannot be empty!');
				redirect($this->config->item('admin_folder').'/taobao/edit/'.$num_iid);
				
				//$this->load->view($this->config->item('admin_folder').'/taobao_form', $data);
			} else {
					
			$catid = $categories[0];	
			for($i=0; $i<count($categories); $i++) {
				if ($this->Category_model->get_category($categories[$i])->parent_id == 0) {
					$catid = $categories[$i];
					break;
				}
			}
			
			if($this->Taobao_model->get_product($num_iid)->sku == "" && $catid) {
				$sku_no = $this->Category_model->get_sku($catid);
				$save['sku'] 	= $sku_no;
				if (count($categories) > 1) {
					$suffix = "";
					for($i=1; $i<count($categories); $i++) {
						if ($this->Category_model->get_category($categories[$i])->parent_id == $catid) {
							$suffix .= $this->Category_model->get_category($categories[$i])->code;
						}
					}
					$save['sku'] .= $suffix;
				}
			}

			// format options
			$options	= array();
			if($this->input->post('option'))
			{
				foreach ($this->input->post('option') as $option)
				{
					$options[]	= $option;
				}

			}	

			if (!$product->original_name) {
				$save['is_saved'] 	= 0;
				$save['seller'] 	= "malppy";
				$save['updated']  	= date("Y-m-d H:i:s");
				$save['adminid'] 	= $this->auth->get_adminid();
			}			
			
			$product_id = $this->Taobao_model->save($save,$options,$categories);

			/* Audit Trail Log */
			$admin_id = $this->session->sess_match_useragent; 	// Action by which user?
			$action_remark = "edited a product"; 			// Action description
			$table_name = "taobao"; 						// Affected table name
			$row_id = $product_id; 							// Affected row id
			$this->Audit_model->log_action($admin_id, $action_remark, $table_name, $row_id);
			/* End of Audit Trail */
			
			$data['id'] = $product->id;
			$data['name'] = $product->product_name;
			$data['description'] = $product->description;
			$data['excerpt'] = $product->excerpt;
			$data['info'] = $product->info;
			$data['specification'] = $product->specification;
			$data['original_description'] = $product->original_description;
			$data['images']	= (array)json_decode($product->images);
			
			$data['categories']		= $this->Category_model->get_categories_tierd();
			
			if($this->input->post('submit'))
			{
				$data['product_categories']	= array();	
			}
			
			if ($redirect) {
				//$this->load->view($this->config->item('admin_folder').'/taobao_form', $data);
				redirect($this->config->item('admin_folder').'/taobao/edit/'.$num_iid);
			}
			}
		}
	}
	
	function delete($num_iid = false, $redirect = true)
	{
		if ($num_iid)
		{	
			$product	= $this->Taobao_model->get_product($num_iid);
			//if the product does not exist, redirect them to the customer list with an error
			if (!$product)
			{
				$this->session->set_flashdata('error', lang('error_not_found'));
				redirect($this->config->item('admin_folder').'/taobao/inventory');
			}
			else
			{
				
				//if the product is legit, delete them
				$this->Taobao_model->delete_product($num_iid);

				/* Audit Trail Log */
				$admin_id = $this->session->sess_match_useragent; 	// Action by which user?
				$action_remark = "deleted a product"; 			// Action description
				$table_name = "taobao"; 						// Affected table name
				$row_id = $num_iid; 							// Affected row id
				$this->Audit_model->log_action($admin_id, $action_remark, $table_name, $row_id);
				/* End of Audit Trail */
				
				if ($redirect) {
					$this->session->set_flashdata('message', lang('message_deleted_product'));
					redirect($this->config->item('admin_folder').'/taobao/inventory');
				}
			}
		}
		else
		{
			//if they do not provide an id send them to the product list page with an error
			$this->session->set_flashdata('error', lang('error_not_found'));
			redirect($this->config->item('admin_folder').'/taobao/inventory');
		}
	}
	
	function product_image_form()
	{
		$data['file_name'] = false;
		$data['error']	= false;
		$this->load->view($this->config->item('admin_folder').'/iframe/taobao_image_uploader', $data);
	}
	
	function product_image_upload()
	{
		$data['file_name'] = false;
		$data['error']	= false;
		
		$config['allowed_types'] = 'gif|jpg|png';
		//$config['max_size']	= $this->config->item('size_limit');
		$config['upload_path'] = 'uploads/images/full';
		$config['encrypt_name'] = true;
		$config['remove_spaces'] = true;

		$this->load->library('upload', $config);
		
		if ( $this->upload->do_upload())
		{
			$upload_data	= $this->upload->data();
			
			$this->load->library('image_lib');
			/*
			
			I find that ImageMagick is more efficient that GD2 but not everyone has it
			if your server has ImageMagick then you can change out the line
			
			$config['image_library'] = 'gd2';
			
			with
			
			$config['library_path']		= '/usr/bin/convert'; //make sure you use the correct path to ImageMagic
			$config['image_library']	= 'ImageMagick';
			*/			
			
			//this is the larger image
			$config['image_library'] = 'gd2';
			$config['source_image'] = 'uploads/images/full/'.$upload_data['file_name'];
			$config['new_image']	= 'uploads/images/medium/'.$upload_data['file_name'];
			$config['maintain_ratio'] = TRUE;
			$config['width'] = 600;
			$config['height'] = 500;
			$this->image_lib->initialize($config);
			$this->image_lib->resize();
			$this->image_lib->clear();

			//small image
			$config['image_library'] = 'gd2';
			$config['source_image'] = 'uploads/images/medium/'.$upload_data['file_name'];
			$config['new_image']	= 'uploads/images/small/'.$upload_data['file_name'];
			$config['maintain_ratio'] = TRUE;
			$config['width'] = 180;
			$config['height'] = 250;
			$this->image_lib->initialize($config); 
			$this->image_lib->resize();
			$this->image_lib->clear();

			//cropped thumbnail
			$config['image_library'] = 'gd2';
			$config['source_image'] = 'uploads/images/small/'.$upload_data['file_name'];
			$config['new_image']	= 'uploads/images/thumbnails/'.$upload_data['file_name'];
			$config['maintain_ratio'] = TRUE;
			$config['width'] = 150;
			$config['height'] = 150;
			$this->image_lib->initialize($config); 	
			$this->image_lib->resize();	
			$this->image_lib->clear();

			//$data['file_name']	= $upload_data['file_name'];
			$data['file_name']	= base_url().'uploads/images/full/'.$upload_data['file_name'];
		}
		
		if($this->upload->display_errors() != '')
		{
			$data['error'] = $this->upload->display_errors();
		}
		$this->load->view($this->config->item('admin_folder').'/iframe/taobao_image_uploader', $data);
	}
	
	function Newiconv($_input_charset, $_output_charset, $input) {
	    $output = "";
	    if (!isset($_output_charset))
	        $_output_charset = $this->parameter['_input_charset '];
	    if ($_input_charset == $_output_charset || $input == null) {
	        $output = $input;
	    } elseif (function_exists("m\x62_\x63\x6fn\x76\145\x72\164_\145\x6e\x63\x6f\x64\x69\x6e\147")) {
	        $output = mb_convert_encoding($input, $_output_charset, $_input_charset);
	    } elseif (function_exists("\x69\x63o\156\x76")) {
	        $output = iconv($_input_charset, $_output_charset, $input);
	    } else
	        //error
	    return $output;
	}
	
	//Gets the data compatible with filegetcontents and curl
	function vita_get_url_content($url) {
		if(!function_exists('file_get_contents')) {
			$file_contents = file_get_contents($url);
		} else {
		$ch = curl_init();
		$timeout = 5; 
		curl_setopt ($ch, CURLOPT_URL, $url);
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
		$file_contents = curl_exec($ch);
		curl_close($ch);
		}
		
	return $file_contents;
	}
	
	//Sign function 
	function createSign ($paramArr,$appSecret) { 
	    $sign = $appSecret; 
	    ksort($paramArr); 
	    foreach ($paramArr as $key => $val) { 
	       if ($key !='' && $val !='') { 
	           $sign .= $key.$val; 
	       } 
	    } 
	    $sign = strtoupper(md5($sign.$appSecret));
	    return $sign; 
	}

	//Group functions 
	function createStrParam ($paramArr) { 
	    $strParam = ''; 
	    foreach ($paramArr as $key => $val) { 
	       if ($key != '' && $val !='') { 
	           $strParam .= $key.'='.urlencode($val).'&'; 
	       } 
	    } 
	    return $strParam; 
	} 

	//Parse XML functions
	function getXmlData ($strXml) {
		$pos = strpos($strXml, 'xml');
		if ($pos) {
			$xmlCode=simplexml_load_string($strXml,'SimpleXMLElement', LIBXML_NOCDATA);
			$arrayCode=$this->get_object_vars_final($xmlCode);
			return $arrayCode ;
		} else {
			return '';
		}
	}
	
	function get_object_vars_final($obj){
		if(is_object($obj)){
			$obj=get_object_vars($obj);
		}
		if(is_array($obj)){
			foreach ($obj as $key=>$value){
				$obj[$key]=$this->get_object_vars_final($value);
			}
		}
		return $obj;
	}
}
