<?php

class Delivery extends Admin_Controller {	

	function __construct()
	{		
		parent::__construct();

		$this->load->model('Order_model');
		$this->load->model('Product_model');
		$this->load->model('Taobao_model');
		$this->load->model('Search_model');
		$this->lang->load('order');
	}
	
	function index()
	{
		$this->load->helper('form');
		$this->load->helper('date');
		$data['message']	= $this->session->flashdata('message');
		$data['page_title']	= "Delivery";
				
		$this->load->view($this->config->item('admin_folder').'/delivery', $data);
	}

	function search()
	{
		$this->load->helper('form');
		$this->load->helper('date');
		$data['message']	= $this->session->flashdata('message');
		$data['page_title']	= "Pending Delivery";

		$data['code']		= $code;
		$term				= false;
		
		$post	= $this->input->post(null, false);
		if($post)
		{
			//if the term is in post, save it to the db and give me a reference
			$term	= json_encode($post);
			$data['code']	= $this->Search_model->record_term($term);
			
			//reset the term to an object for use
			$term	= (object)$post;
		}
		elseif ($code)
		{
			$term	= $this->Search_model->get_term($code);
			$term	= json_decode($term);
		} 

 		$data['term']	= $term;

		if ($term) {
			$data['orders'] = $this->Order_model->get_orders($term,'','DESC',0,0,'Pending Delivery');
			$data['total']	= count($data['orders']);
		}

		$this->load->view($this->config->item('admin_folder').'/delivery_search', $data);
	}

	function items_arrival()
	{
		$this->load->helper('form');
		$this->load->helper('date');
		$data['message']	= $this->session->flashdata('message');
		$data['page_title']	= "Pending Delivery";

		$data['code']		= $code;
		$term				= false;
		
		$post	= $this->input->post(null, false);
		if($post)
		{
			//if the term is in post, save it to the db and give me a reference
			$term	= json_encode($post);
			$data['code']	= $this->Search_model->record_term($term);
			
			//reset the term to an object for use
			$term	= (object)$post;
		}
		elseif ($code)
		{
			$term	= $this->Search_model->get_term($code);
			$term	= json_decode($term);
		} 

 		$data['term']	= $term;

		if ($term) {
			$data['orders'] = $this->Order_model->get_orders($term);
			$data['total']	= count($data['orders']);
		}

		$this->load->view($this->config->item('admin_folder').'/delivery_arrival', $data);
	}

	function delivery_status()
	{
		$this->load->helper('form');
		$this->load->helper('date');
		$data['message']	= $this->session->flashdata('message');
		$data['page_title']	= "Delivery Status";

		$data['term']	= $term;
 		$data['orders']	= $this->Order_model->get_orders($term, $sort_by, $sortorder, $rows, $page, 'Pending Delivery');
		$data['total']	= $this->Order_model->get_orders_count($term);
				
		$this->load->view($this->config->item('admin_folder').'/delivery_status', $data);
	}
}