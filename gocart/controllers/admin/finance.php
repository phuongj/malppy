<?php

class Finance extends Admin_Controller {

	//this is used when editing or adding a customer
	var $customer_id	= false;	

	function __construct()
	{		
		parent::__construct();
		remove_ssl();

		$this->auth->check_access('Admin', true);
		
		//$this->load->model('Order_model');
		$this->load->model('Search_model');
		$this->load->model('Finance_model');
		$this->load->helper(array('formatting', 'utility'));
		
		$this->lang->load('report');
	}
	
	function index()
	{
		
		$orders		= $this->Finance_model->get_orders();
		foreach($orders as &$o)
		{
			$data['orders'][] = $this->Finance_model->get_items($o->id);
		}
		$data['page_title']	= lang('reports');
		$this->load->view($this->config->item('admin_folder').'/finance', $data);
	}
	
	function account()
	{
		$start	= $this->input->post('start');
		$end	= $this->input->post('end');
		$data['best_sellers']	= $this->Finance_model->get_orders($start, $end);
		
		$this->load->view($this->config->item('admin_folder').'/reports/account', $data);	
	}
	
	function sales()
	{
		$bulk_orders	= $this->Order_model->get_orders();
		
		$orders			= array();
		
		foreach($bulk_orders as $o)
		{
			// omit orders with a blank date
			if($o->ordered_on=='0000-00-00 00:00:00')
			{
				continue;
			}
			
			
			$date	= explode('-', $o->ordered_on);
			$y		= $date[0];
			$m		= $date[1];
			if(!isset($orders[$y]))
			{
				$orders[$y]	= array();
			}
			
			if(!isset($orders[$y][$m]))
			{
				$orders[$y][$m]	= array();
			}
			
			//coupon discounts
			if(!isset($orders[$y][$m]['coupon_discounts']))
			{
				$orders[$y][$m]['coupon_discounts'] = 0;
			}
			$orders[$y][$m]['coupon_discounts'] += $o->coupon_discount;
			
			//gift card discounts
			if(!isset($orders[$y][$m]['gift_card_discounts']))
			{
				$orders[$y][$m]['gift_card_discounts'] = 0;
			}
			$orders[$y][$m]['gift_card_discounts'] += $o->gift_card_discount;
			
			//total of product sales
			if(!isset($orders[$y][$m]['product_totals']))
			{
				$orders[$y][$m]['product_totals'] = 0;
			}
			$orders[$y][$m]['product_totals'] += $o->subtotal;
			
			//total of Shipping
			if(!isset($orders[$y][$m]['shipping']))
			{
				$orders[$y][$m]['shipping'] = 0;
			}
			$orders[$y][$m]['shipping'] += $o->shipping;
			
			//total taxes
			if(!isset($orders[$y][$m]['tax']))
			{
				$orders[$y][$m]['tax'] = 0;
			}
			$orders[$y][$m]['tax'] += $o->tax;
			
			//Grand Total less discounts
			if(!isset($orders[$y][$m]['total']))
			{
				$orders[$y][$m]['total'] = 0;
			}
			$orders[$y][$m]['total'] += $o->total;	
		}
		
		krsort($orders);
		foreach($orders as &$order)
		{
			krsort($order);
		}
		
		$data['orders'] = $orders;
		$this->load->view($this->config->item('admin_folder').'/reports/sales', $data);	
	}
	
	function form($id = false)
	{
		$this->load->helper(array('form', 'date'));
		$this->load->library('form_validation');
		
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		
		$this->id	= $id;
		
		$data['page_title']		= "Account Manager";
		
		//default values are empty if the product is new
		$data['id']						= '';
		$data['code']					= '';
		$data['create_date']			= '';
		$data['credit']					= 0;
		$data['debit'] 					= 0;
		$data['remarks']				= '';		
		
		$this->form_validation->set_rules('credit', '', 'trim');
		$this->form_validation->set_rules('debit', 'l', 'trim');
		$this->form_validation->set_rules('remark', '', 'trim');
		$this->form_validation->set_rules('create_date', '');
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view($this->config->item('admin_folder').'/finance_form', $data);
		}
		else
		{
			$save['id']						= $id;
			$save['create_date']			= $this->input->post('create_date');
			$save['credit']				= $this->input->post('credit');
			$save['debit']			 	= $this->input->post('debit');
			$save['remarks'] 			= $this->input->post('remarks');
			
			// save coupon
			$finance_id = $this->Finance_model->save($save);
			
			// We're done
			$this->session->set_flashdata('message', lang('message_saved_coupon'));
			
			//go back to the product list
			redirect($this->config->item('admin_folder').'/finance');
		}
	}

}