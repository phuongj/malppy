<?php

class Logistic extends Admin_Controller {	

	function __construct()
	{		
		parent::__construct();

		$this->load->model('Order_model');
		$this->load->model('Product_model');
		$this->load->model('Customer_model');
	}
	
	function index()
	{
		$this->load->helper('form');
		$this->load->helper('date');
		$data['message']	= $this->session->flashdata('message');
		$data['page_title']	= "Delivery";
				
		$this->load->view($this->config->item('admin_folder').'/logistic', $data);
	}

	function grouping()
	{
		$this->load->helper('form');
		$data['message']	= $this->session->flashdata('message');
		$data['page_title']	= "Grouping";

		$data['shipments'] = $this->Order_model->get_shipments();
				
		$this->load->view($this->config->item('admin_folder').'/logistic_grouping', $data);
	}

	function shipping()
	{
		$this->load->helper('form');
		$data['message']	= $this->session->flashdata('message');
		$data['page_title']	= "Shipping";

		$search = $this->input->post('term');

		if ($this->input->post('confirm')) {
			// Add New Ship Shipment if button is submit
			$save['box_id'] = $this->input->post('box_id');
			$save['shipment_number'] = $this->input->post('shipping_number');
			$save['remark'] = $this->input->post('shipping_remark');

			$save['total_order'] = $this->input->post('total_order');
			$save['total_item'] = $this->input->post('total_item');

			$save[shipped_on]		= date("Y-m-d");

			$save[orders]		= $this->Order_model->get_orders_by_box($save['box_id']);

			$this->Order_model->add_shipping($save);

			//$this->session->set_flashdata('message', 'New Shipment created!');
		}
		
		$data['shipments'] = $this->Order_model->get_shipments($search);
		
		$this->load->view($this->config->item('admin_folder').'/logistic_shipping', $data);
	}

	function update_status($shipment_id='')
    {
    	// get the order details
		$data['shipment_id'] = $shipment_id;
    	
		$data['shipment'] = $this->Order_model->get_shipment($shipment_id);

		if($this->input->post('update'))
		{

			$save[id] = $shipment_id;
			$save[shipped_on] = $this->input->post('ship_arrived_on');
			$save[sent_on] = $this->input->post('ship_sent_on');

			$this->Order_model->save_shipment($save);

			$data[shipped_on] = $save[shipped_on];
			$data[sent_on] = $save[sent_on];

			$data[finished] = true;
		}
	
		

		$this->load->view($this->config->item('admin_folder').'/iframe/shipment_status.php', $data);
 
    }

	function local_delivery()
	{
		$this->load->helper('form');
		$data['message']	= $this->session->flashdata('message');
		$data['page_title']	= "Packing & Delivery";

		$shipments = $this->Order_model->get_shipments();

		foreach ($shipments AS $shipment) {
			if ($shipment->is_close) continue;
			$orders = $this->Order_model->get_shipment_orders($shipment->id);
			
			$ship[$shipment->id]['shipment_number'] = $shipment->shipment_number;
			$ship[$shipment->id]['shipid'] = $shipment->id;

			foreach ($orders AS $order) {

				$order_detail = $this->Order_model->get_order($order->order_id);
				$ship[$shipment->id]['orders'][$order->order_id] = $order_detail;
			}				

		}

		$data['shipment'] = $ship;
		
		$this->load->view($this->config->item('admin_folder').'/logistic_local_delivery', $data);	
		
	}

	function do_slip($order_id)
	{
		$this->load->helper('date');
		$this->load->helper('form');
		$this->load->helper(array('formatting', 'utility'));

		$data['order'] = $this->Order_model->get_order($order_id);
			
		$this->load->view($this->config->item('admin_folder').'/do_slip.php', $data);
	}

	function close_order($order_id)
	{		
		$save['id'] = $order_id;
		$save['status'] = "Closed";
		$save['closed_on'] = date("Y-m-d H:i:s");		
		$save['is_delivered'] = 1;
		
		$this->Order_model->close_order($save);
		$this->local_delivery();
	}

	function new_shipment()
	{
		$this->load->helper('date');
		$data	= $this->input->post('ship');
		
		//echo "<pre>";
		//print_r($data);
		//echo "</pre>";
		$save				= array();
		$save[shipment_number] = $data[shipno];
		$save[orders] = $data[orders];

		$eo		 	= explode('-', $data[shipped_on]);
		$save[shipped_on]		= $eo[2].'-'.$eo[0].'-'.$eo[1];

		$this->Order_model->add_shipping($save);

		$this->session->set_flashdata('message', 'New Shipment created!');
		redirect($this->config->item('admin_folder').'/logistic/grouping');
	}

	function delete($id)
	{
		if($id) {
			$this->Order_model->delete_shipment($id);
		}
		$this->session->set_flashdata('message', 'Shipment deleted!');
		redirect($this->config->item('admin_folder').'/logistic/grouping');
	}

	function bulk_save()
	{
		$logistics	= $this->input->post('logistic');
		
		if(!$logistics)
		{
			$this->session->set_flashdata('error',  lang('error_bulk_no_products'));
			redirect($this->config->item('admin_folder').'/logistic/local_delivery');
		}
				
		foreach($logistics as $id=>$logistic)
		{
			$logistic['id']	= $id;

			$save				= array();
			$save['id']			= $id;
			$save['is_delivered']	= $logistic['is_delivered'];
			$save['status']	=	'Delivered';
			$this->Order_model->save_order($save);
		}
		
		$this->session->set_flashdata('message', lang('message_bulk_update'));
		redirect($this->config->item('admin_folder').'/logistic/local_delivery');
	}

	function bulk_date()
	{
		$order	= $this->input->post('order');

		if(!$order)
		{
			$this->session->set_flashdata('error',  lang('error_bulk_no_products'));
			redirect($this->config->item('admin_folder').'/logistic/local_delivery');
		}
				
		foreach($order as $id=>$order)
		{
			$save['id']			= $id;

			if ($order[is_packed]) {
				$save['is_packed']	= $order[is_packed];
			} else {
				$save['is_packed'] = 0;
			}

			if ($order[bt_date]) {
				$save['bt_on']	= $order[bt_date];
			}

			if ($order[do_date]) {
				$save['do_on']	= $order[do_date];
			}

			$this->Order_model->save_order($save);
		}
		
		$this->session->set_flashdata('message', "Updated!");
		redirect($this->config->item('admin_folder').'/logistic/local_delivery');
	}

	function bulk_close()
	{
		$shipment_id	= $this->input->post('shipment');
		$orders = $this->Order_model->get_shipment_orders($shipment_id);

		foreach ($orders AS $order) {
			$save['id'] = $order->order_id;
			$save['status'] = "Closed";
			$save['closed_on'] = date("Y-m-d H:i:s");		
			$save['is_delivered'] = 1;
		
			$this->Order_model->close_order($save);
		}	

		$save2['id'] = $shipment_id;
		$save2['status'] = "closed";
		$save2['is_close'] = 1;
		$this->Order_model->save_shipment($save2);
		
		$this->session->set_flashdata('message', "Updated!");
		redirect($this->config->item('admin_folder').'/logistic/local_delivery');
	}
}