<?php require('header.php'); ?>

<div id="breadcrumb">
	<ul>
		<li><a href="<?php echo site_url($this->config->item('admin_folder').'/logistic');?>">Logistic</a></li>
       	<li class="last"><a href="<?php echo site_url($this->config->item('admin_folder').'/logistic/shipping');?>">Shipping Tracking</a></li>
    </ul>
</div><!-- End of breadcrumb --> 
<br>

<?php echo form_open($this->config->item('admin_folder').'/logistic/shipping', array('id'=>'search_form')); ?>
	<input id="top" type="text" class="gc_tf1" name="term" value="" /> 
	<input type="hidden" name="term" id="search_term" value=""/>
	<input type="hidden" name="start_date" id="start_date" value=""/>
	<input type="hidden" name="end_date" id="end_date" value=""/>
	<span class="button_set"><a href="#" onclick="do_search('top'); return false;">Search</a>
	</span>
</form>
<br>

<?php echo form_open($this->config->item('admin_folder').'/logistic/bulk_save', array('id'=>'bulk_form'));?>

<table class="gc_table" cellspacing="0" cellpadding="0">
    <thead>
		<tr>
			<th class="gc_cell_left">Date Shipped</th>
			<th>Shipping ID</th>
			<th style="text-align:center;">Order</th>
			<th style="text-align:center;">Item</th>
			<th style="text-align:center;">Remark</th>
			<th style="text-align:center;">Sent</th>
			<th style="text-align:center;">Arrived</th>
			<th class="gc_cell_right"></th>
	    </tr>
	</thead>
 	<!-- <tfoot>
    <?php echo $pagination?>
	</tfoot> -->
    <tbody>
		
	<?php echo (count($shipments) < 1)?'<tr><td style="text-align:center;" colspan="10">No shipment found.</td></tr>':''?>
    <?php foreach($shipments as $shipment): ?>
		<tr>
			<td><?php echo $shipment->shipped_on; ?></td>
			<td style="white-space:nowrap"><?php echo $shipment->shipment_number; ?></td>
			<td style="text-align:center;"><?php echo $shipment->total_order; ?></td>
			<td style="text-align:center;">
			<?php echo $shipment->total_item; ?>
			</td>
			<td style="text-align:center;"><?php echo $shipment->remark; ?></td>
			<td style="text-align:center;"><?php echo $shipment->shipped_on; ?>
			<!-- <?php
				$data	= array('name'=>'logistic['.$shipment->id.'][is_delivered]', 'value'=>1, 'checked'=>set_checkbox('logistic['.$shipment->id.'][is_delivered]', 1, (bool)$shipment->is_delivered));
				echo form_checkbox($data);
			?> -->
			</td>
			<td style="text-align:center;"><?php echo $shipment->sent_on; ?>
			<!-- <?php
				$data	= array('name'=>'logistic['.$shipment->id.'][is_delivered]', 'value'=>1, 'checked'=>set_checkbox('logistic['.$shipment->id.'][is_delivered]', 1, (bool)$shipment->is_delivered));
				echo form_checkbox($data);
			?> -->
			</td>			
			<!-- <td style="text-align:center;">
				<?php
					$data	= array('name'=>'logistic['.$shipment->id.'][is_delivered]', 'value'=>1, 'checked'=>set_checkbox('logistic['.$shipment->id.'][is_delivered]', 1, (bool)$shipment->is_delivered));
					echo form_checkbox($data);
				?>
			</td> -->
			<td class="gc_cell_right list_buttons">
				<a title="Update Status" id="status_button_<?php echo $shipment->id; ?>" class="update_status" href="<?php echo site_url($this->config->item('admin_folder').'/logistic/update_status/'.$shipment->id); ?>">Update Status</a>&nbsp;
				<!-- <a href="<?php echo site_url($this->config->item('admin_folder').'/orders/view/'.$shipment->id);?>">View Status</a>&nbsp; -->
				<!-- <a href="<?php echo site_url($this->config->item('admin_folder').'/orders/view/'.$shipment->id);?>">Print All</a>&nbsp; -->
				
			</td>
		</tr>
    <?php endforeach; ?>
		
    </tbody>
</table>
</form>

<!-- <div class="button_set">
	<a href="#" onclick="$('#bulk_form').submit(); return false;">Update</a>
</div> -->

<script type="text/javascript">

$(document).ready(function(){
	get_best_sellers();
	get_sales();
	$('input:button').button();
	$('#best_sellers_start').datepicker({ dateFormat: 'mm-dd-yy', altField: '#best_sellers_start_alt', altFormat: 'yy-mm-dd' });
	$('#best_sellers_end').datepicker({ dateFormat: 'mm-dd-yy', altField: '#best_sellers_end_alt', altFormat: 'yy-mm-dd' });
});

function get_best_sellers()
{
	$.post('<?php echo site_url($this->config->item('admin_folder').'/reports/best_sellers');?>',{start:$('#best_sellers_start').val(), end:$('#best_sellers_end').val()}, function(data){
		$('#best_sellers').html(data);
	});
}

function get_sales()
{
	$.post('<?php echo site_url($this->config->item('admin_folder').'/reports/sales');?>',{bah:Math.floor(Math.random( )*9999999999)}, function(data){
		$('#sales_container').html(data);
	});
}

function do_search(val)
{
	$('#search_term').val($('#'+val).val());
	$('#start_date').val($('#start_'+val+'_alt').val());
	$('#end_date').val($('#end_'+val+'_alt').val());
	$('#search_form').submit();
}

$('.update_status').colorbox({
					width: '400px',
					height: '300px',
					iframe: true
				});

</script>

<?php include('footer.php'); ?>