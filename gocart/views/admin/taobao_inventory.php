<?php require('header.php'); 
	
//set "code" for searches
if(!$code) {
	$code = '';
} else {
	$code = '/'.$code;
}

function sort_url($by, $sort, $sorder, $code, $admin_folder) {
	if ($sort == $by) {
		if ($sorder == 'asc') {
			$sort	= 'desc';
		} else {
			$sort	= 'asc';
		}
	} else {
		$sort	= 'asc';
	}
	$return = site_url($admin_folder.'/taobao/inventory/'.$by.'/'.$sort.'/'.$code);
	return $return;
}
	 	
$pagination .= $pages;
	
if ($term) {
	echo '<p id="searched_for"><div style="width:70%;float:left;"><strong>'.sprintf(lang('search_returned'), intval($total)).'</strong></div><div style="width:29% float:right; text-align:right;"><a href="'.base_url().$this->config->item('admin_folder').'/orders" class="button">'.lang('all_orders').'</a></div></p>';
}
?>
<?php
$atts = array(
			'width'      => '1280',
            'height'     => '768',
            'scrollbars' => 'yes',
            'status'     => 'yes',
            'resizable'  => 'yes',
            'screenx'    => '0',
            'screeny'    => '0'
          	);
?>
<script type="text/javascript">
function areyousure()
{
	return confirm('<?php echo lang('confirm_delete_product');?>');
}
function areyousure2()
{
	return confirm('Are you sure you want to publish this product?');
}
function areyousure3()
{
	return confirm('Are you sure you want to suspend this product?');
}
function view_category_product(id)
{	
	if (id) {
		location.href='<?php echo  site_url($this->config->item('admin_folder').'/taobao/category/');?>/'+id;
	} else {
		location.href='<?php echo  site_url($this->config->item('admin_folder').'/taobao/inventory');?>';
	}	
}
</script>
<style>
.pager {color:#333; position:relative;text-align:left;color:#555;margin-top:13px;}
.pager ul {display:inline;padding-left:0px;margin-left:0px}
.pager li {display:inline;list-style:none;text-align:center; margin:2px;}
.pager li a {border: 1px solid #CCCCCC;color: #555555;font-size: 10px !important;padding: 5px 9px !important;text-decoration: none !important;}
.pager li a.last {border:2px #ccc solid;}
.pager li a:hover {background-color: #333333; color: #fff!important; border: 1px solid #333;}
.pager li a.last:hover {border:2px #000 solid;}
.pager li.active a {background-color: #333;color: #FFF;border: 1px solid #333;}
.pager .next a, .pager .previous a {border: 1px #fff solid; padding: 5px 9px !important;  }
.pager .next a:hover, .pager .previous a:hover {font-weight:normal;}
.pager .total {font-size:80%;}
</style>

<div id="breadcrumb">
	<ul>
    	<li><a href="<?php echo site_url($this->config->item('admin_folder').'/taobao/inventory');?>">Inventory</a></li>
        <?php if ($catid) { ?>
		<li><a href="<?php echo site_url($this->config->item('admin_folder').'/taobao/inventory');?>">Pending Approval</a></li>
		<li class="last"><a href="#"><?php echo $this->Category_model->get_category($catid)->name; ?></a></li>
		<?php } else { ?>
		<li class="last"><a href="#">Pending Approval</a></li>
		<?php } ?>
    </ul>
</div><!-- End of breadcrumb --> 

<div class="button_set">
	<a href="<?php echo site_url($this->config->item('admin_folder').'/taobao/add');?>"><?php echo lang('add_new_product');?></a>
</div>



	<div class="button_set" style="text-align:left;float:left">
	<select name="catid" id="catid" class="gc_tf1" onchange="view_category_product(this.value)">
		<option value="">Show All Products</option>
		<option value="-1" <?php if ($catid=='-1')  { ?>selected<?php } ?>>Show Products Without Category</option>
		<?php
		define('ADMIN_FOLDER', $this->config->item('admin_folder'));
		define('SELECTED_CATID', $catid);
		$this->load->model('Product_model');
		$Product_model = new Product_model;
		function list_categories($cats, $sub='', $Product_model) {	
			foreach ($cats as $cat):?>
			<option id="cat_item_<?php echo $cat['category']->id;?>" value="<?php echo $cat['category']->id;?>" <?php if ($cat['category']->id==SELECTED_CATID)  { ?>selected<?php } ?>><?php echo  $sub.$cat['category']->name; ?> (<?php echo $Product_model->count_taobao_products($cat['category']->id); ?>)</option>
			<?php
			if (sizeof($cat['children']) > 0)
			{
				$sub2 = str_replace('-&nbsp;', '&nbsp;', $sub);
					$sub2 .=  '&nbsp;-&nbsp;';
				list_categories($cat['children'], $sub2, $Product_model);
			}
			endforeach;
		}
		$cats_tierd = $this->Category_model->get_categories_tierd();
		
		list_categories($cats_tierd,'',$Product_model);
		?>
	</select>
	</div>
	<div class="pager" style="float:right">
		<?php echo $pagination; ?>
	</div>
	<div style="clear:both"></div>

<?php echo form_open($this->config->item('admin_folder').'/taobao/bulk_save', array('id'=>'bulk_form'));?>

<table border="0" width="100%" class="table gc_table">
	<thead>
	<tr>
		<th class="gc_cell_left"><input type="checkbox" id="gc_check_all" /></th>
		<th>No</th>
		<th></th>
		<th>Product Name</th>
		<th>Original SKU</th>
		<th>Product Code</th>
		<th style="width:100px;">Cost Price</th>
		<!-- <th>Cost Price (BND)</th> -->
		<th style="width:80px;">Sales Price <br>(BND)</th>
		<th style="width:50px;text-align:center;">Available Quantity</th>
		<!-- <th>Added Date</th> -->
		<th style="width:90px;text-align:center;">Date added</th>
		<th style="width:90px;text-align:center;">Last edited</th>
		<th style="width:90px;text-align:center;">User</th>
		<?php
		// Restrict access to Admins only
		if($this->auth->check_access('Admin')) : 
		?>
		<th nowrap><input type="checkbox" id="pb_check_all" />&nbsp;Published?</th>   
		<?php endif; ?>
		<th></th>   
	</tr>
	</thead>
	<tbody>
	<?php
		$index+=$page;
		foreach ($products as $prod) { 
		$user = $this->Product_model->get_admin($prod->adminid);
	?>
	<tr>
		<td><input name="product[<?php echo $prod->num_iid; ?>]" type="checkbox" value="<?php echo $prod->num_iid; ?>" class="gc_check"/></td>
		<td>
			<?php echo ++$index;?>
		</td>		
		<td style="width:50px;">
		<?php 
        	$this_product = $this->Taobao_model->get_product($prod->num_iid);

			$this_product->images	= (array)json_decode($this_product->images);
			$this_product->images	= array_values($this_product->images);
			$primary	= $this_product->images[0];
				
			foreach($this_product->images as $image) { if(isset($image->primary)) $primary	= $image; }
            
			if (strpos($primary->filename, 'http') === 0) { ?>
				<img src="<?php echo base_url(); ?>phpthumb/phpThumb.php?src=<?php echo $primary->filename;?>&w=50&h=50&zc=1" alt="" width="50" />
				<!-- <img src="<?php echo base_url(); ?>phpthumb/phpThumb.php?src=<?php echo $prod->pic_url; ?>&w=50&h=50&far=1" width="50" height="50" /> -->
			<?php } else { ?>
				<img src="<?php echo base_url(); ?>phpthumb/phpThumb.php?src=<?php echo base_url('uploads/images/thumbnails/'.$primary->filename); ?>&w=50&h=50&far=1" alt=""/>
			<?php } ?>
		</td>
		<td>
            <a href="<?php echo $this_product->shop_url; ?>" target="_blank" style="padding:0px;"><?php echo strip_tags(urldecode($prod->product_name));?></a>
		</td>
		<td>
			<?php echo $prod->num_iid;?>
		</td>
		<td>
			<?php echo $prod->sku;?>
		</td>
		<td>
			(RMB) <?php echo $prod->price;?> <br>
			(<?php echo $this->config->item('currency'); ?>) <?php echo @number_format($prod->price/$bnd_to_rmb,2);?>
		</td>
		<!-- <td>
			<?php echo @number_format($prod->price/$bnd_to_rmb,2);?>
		</td> -->
		<td><?php echo form_input(array('name'=>'product2['.$prod->num_iid.'][saleprice]', 'value'=>set_value('saleprice', $prod->saleprice), 'class'=>'gc_tf3'));?></td>
		<td style="text-align:center;">
            <?php echo $prod->quantity;?>
		</td>
		<!-- <td>
            <?php echo $prod->createdate;?>
		</td> -->
		<td style="text-align:center;"><?php echo date("d-m-Y",strtotime($prod->createdate));?></td>
		<td style="text-align:center;"><?php echo date("d-m-Y",strtotime($prod->updated));?></td>
		<td style="text-align:center;"><?php echo $user;?></td>
		<?php
		// Restrict access to Admins only
		if($this->auth->check_access('Admin')) : 
		?>
		<td align="center">
			<?php
				$data	= array('name'=>'approve['.$prod->num_iid.'][enabled]', 'value'=>1, 'class'=>'pb_check', 'checked'=>set_checkbox('approve['.$product->id.'][enabled]', 1, (bool)$product->enabled));
				echo form_checkbox($data);
			?>
		</td>
		<?php endif; ?>
		<td class="gc_cell_right list_buttons" style="white-space:nowrap">
			<?php echo anchor_popup('http://www.malppy.com/cart/preview/'.$prod->id, 'Preview', $atts); ?>
			<a href="<?php echo $admin_url;?>taobao/edit/<?php echo $prod->num_iid;?>/pending_approval">Edit</a>

			<!-- <a href="<?php echo $admin_url;?>taobao/check_stock/<?php echo $prod->num_iid;?>">update stock</a> &nbsp; -->
			<!-- <a href="<?php echo $admin_url;?>taobao/delete/<?php echo $prod->num_iid;?>">delete</a> -->
		</td>
	</tr>
	<?php } ?>
	<?php
		if (!$products) {
		echo '<tr><td colspan="10" class="submsg" align="center">No record found.</td></tr>';
		}
	?>
	</tbody>
</table>
</form>
<div class="button_set" style="text-align:left;">
	<!-- <a href="#" onclick="$('#bulk_form').submit(); return false;">Publish All</a> -->
	<a href="#" onclick="$('#bulk_form').attr('action','<?php echo site_url($this->config->item('admin_folder').'/taobao/bulk_delete'); ?>');$('#bulk_form').submit(); return false;">Delete</a>

	<?php
		// Restrict access to Admins only
		if($this->auth->check_access('Admin')) : 
	?>
	<span style="float: right; margin-right: 120px;"><a href="#" onclick="$('#bulk_form').submit(); return false;">Update</a><span>
	<?php endif; ?>
	<div class="clear"></div>
</div>

<script type="text/javascript">
$(document).ready(function(){
	$('#gc_check_all').click(function(){
		if(this.checked)
		{
			$('.gc_check').attr('checked', 'checked');
		}
		else
		{
			 $(".gc_check").removeAttr("checked"); 
		}
	});

	$('#pb_check_all').click(function(){
		if(this.checked)
		{
			$('.pb_check').attr('checked', 'checked');
		}
		else
		{
			 $(".pb_check").removeAttr("checked"); 
		}
	});

	//$.post('<?php echo site_url($this->config->item('admin_folder').'/taobao/auto_stock_update');?>/',{},function(data){

	//});
});

function submit_form()
{
	if($(".gc_check:checked").length > 0)
	{
		if(confirm('Confirm import?'))
		{
			$('#taobao_form').submit();
		}
	}
	else
	{
		alert('No item selected.');
	}
}
</script>

<?php include('footer.php'); ?>