<?php include('header.php'); ?>
<?php include('page.class.php'); 
$TaobaokeCount = !(isset($TaobaokeCount))?'0':$TaobaokeCount;
$page = !(isset($_GET['page']))?'1':intval($_GET['page']);
$page_size = !(isset($_GET['page_size']))?'10':intval($_GET['page_size']); ?>

<div id="breadcrumb">
	<ul>
    	<li><a href="<?php echo site_url($this->config->item('admin_folder').'/taobao');?>">Taobao</a></li>
        <li class="last"><a href="#">Search Products</a></li>
    </ul>
</div><!-- End of breadcrumb --> 

<form action="taobao" method="get" name="form1" id="form1">
<table class="gc_table" cellspacing="0" cellpadding="0">
	<thead>
	<tr>
      <th class="gc_cell_left">Category ID</td>
      <th>Keyword</td>      
      <th>Start Price</td>
      <th>End Price</td>
	  <th>Area</td>
	  <th>Sort by</td>
      <th>Page Size</td>      
     
      <!-- <th>Auto Delivery</td>                        
      <th class="gc_cell_right">Customer Guarantee</td>       -->
    </tr>
	</thead>
	<tbody>
	<tr class="gc_row">
      <td>
<select name="cid" style="width:200px; ">
<option value="0" selected="selected">All Categories </option>
<?php include('taobao_category_list.php'); ?>
</select>      
      </td>
      <td><input type="text" name="keyword" id="keyword" value=""></td>      
      <td><input type="text" name="start_price" id="start_price"></td>
      <td><input type="text" name="end_price" id="end_price"></td>      
	  <td>
      <select name="area">
      <option value="" selected> All area </option>
      <?php include('taobao_area_list.php'); ?>
      </select>      
      </td>
	  <td>
      <select name="sort">
	  <option value=""></option>
	  <option value="price_asc">(Price Low to High) </option>
      <option value="price_desc">(Price High to Low) </option>      
      
     <!--  <option value="credit_desc">(æ·‡ï¼„æ•¤ç»›å¤Œéª‡æµ åº¨ç�®é�’é¢�ç¶†) </option> 
      <option value="commissionRate_desc">(æµ£ï½‰å™¾å§£æ—‚å·¼æµ åº¨ç�®é�’æ�¿ç°³ </option>
      <option value="commissionRate_asc">(æµ£ï½‰å™¾å§£æ—‚å·¼æµ åº�ç¶†é�’ä¼´ç�®) </option>
      <option value="commissionNum_desc">(éŽ´æ„ªæ°¦é–²å¿”åžšæ¥‚æ¨ºåŸŒæµ£?  </option>
      <option value="commissionNum_asc">(éŽ´æ„ªæ°¦é–²å¿Žç² æµ£åº¡åŸŒæ¥‚ </option>
      <option value="commissionVolume_desc">(éŽ¬ç»˜æ•®é�‘è½°å‰‘é–²æˆœç² æ¥‚æ¨ºåŸŒæ�? </option>
      <option value="commissionVolume_asc">(éŽ¬ç»˜æ•®é�‘è½°å‰‘é–²æˆœç² æµ£åº¡åŸŒæ¥? </option>
      <option value="delistTime_desc()">(é�Ÿå——æ�§æ¶“å¬«ç�¦é�ƒå •æ£¿æµ åº¨ç�®é�’æ�¿ç°? </option>
      <option value="delistTime_asc">(é�Ÿå——æ�§æ¶“å¬«ç�¦é�ƒå •æ£¿æµ åº�ç¶†é�’ä¼´ç�? </option>-->
      </select>
      </td>
	   <td>
      <select name="page_size">
      <?php 
	  for($i=10;$i<41;$i++){
	  ?>
      <option value="<?php echo $i?>"><?php echo $i?> </option>
      <? }?>
      </select>      
      </td>    
      <!-- <td>
      <select name="auto_send">
      <option value="" selected>Select </option>      
        <option value="true">Yes </option>
        <option value="true">No </option>        
      </select>
     </td>
      <td>
      <select name="guarantee">
      <option value="" selected>Select </option>      
        <option value="true">Yes </option>
        <option value="true">No </option>        
      </select>      
      </td>           -->                    
    </tr> 
    </tbody>
	<thead>
	<tr>
      
      <!-- <th>Start Credit</td>
      <th>End Credit</td> -->
       <!-- <th colspan="6"></td>    -->
    </tr>
	</thead>
	<tbody>
 	<tr>
      
      
      <!-- <td>
      <select name="start_credit">
      <option value="1heart">(æ¶“â‚¬è¹? </option>      
      <option value="2heart">(æ¶“ã‚…ç¸? </option>      
      <option value="3heart">(é�¥æ¶˜ç¸? </option>      
      <option value="4heart">(æ¶“â‚¬è¹? </option>      
      <option value="5heart">(æµœæ–¿ç¸? </option>                              
      <option value="1diamond">(æ¶“â‚¬é–? </option>      
      <option value="2diamond">(æ¶“ã‚‰æ�? </option>      
      <option value="3diamond">(æ¶“å¤�æ�? </option>      
      <option value="4diamond">(é�¥æ¶¢æ�? </option>      
      <option value="5diamond">(æµœæ—ˆæ�? </option>                              
      <option value="1crown">(æ¶“â‚¬é�? </option>      
      <option value="2crown">(æ¶“ã‚…å•? </option>      
      <option value="3crown">(æ¶“å¤Šå•? </option>   
      <option value="4crown">(é�¥æ¶˜å•? </option>            
      <option value="5crown">(æµœæ–¿å•? </option>      
      <option value="1goldencrown">(æ¶“â‚¬æ¦›å‹«å•? </option>                              
      <option value="2goldencrown">(æµœå²„ç²�é��? </option>      
      <option value="3goldencrown">(æ¶“å¤�ç²�é��? </option>      
      <option value="4goldencrown">(é�¥æ¶¢ç²�é��? </option>      
      <option value="5goldencrown">(æµœæ—ˆç²�é��? </option>      
      </select>
      </td>
      <td>
      <select name="end_credit">
      <option value="1heart">(æ¶“â‚¬è¹? </option>      
      <option value="2heart">(æ¶“ã‚…ç¸? </option>      
      <option value="3heart">(é�¥æ¶˜ç¸? </option>      
      <option value="4heart">(æ¶“â‚¬è¹? </option>      
      <option value="5heart">(æµœæ–¿ç¸? </option>                              
      <option value="1diamond">(æ¶“â‚¬é–? </option>      
      <option value="2diamond">(æ¶“ã‚‰æ�? </option>      
      <option value="3diamond">(æ¶“å¤�æ�? </option>      
      <option value="4diamond">(é�¥æ¶¢æ�? </option>      
      <option value="5diamond">(æµœæ—ˆæ�? </option>                              
      <option value="1crown">(æ¶“â‚¬é�? </option>      
      <option value="2crown">(æ¶“ã‚…å•? </option>      
      <option value="3crown">(æ¶“å¤Šå•? </option>   
      <option value="4crown">(é�¥æ¶˜å•? </option>            
      <option value="5crown">(æµœæ–¿å•? </option>      
      <option value="1goldencrown">(æ¶“â‚¬æ¦›å‹«å•? </option>                              
      <option value="2goldencrown">(æµœå²„ç²�é��? </option>      
      <option value="3goldencrown">(æ¶“å¤�ç²�é��? </option>      
      <option value="4goldencrown">(é�¥æ¶¢ç²�é��? </option>      
      <option value="5goldencrown" selected>(æµœæ—ˆç²�é��? </option>      
      </select>      
      </td>     -->  
                 
     
      <td colspan="7"><div class="button_set" style="text-align:center;"><input type="submit" name="submit" value="Search"></div>
      </td>                                
    </tr>    
	</tbody>
</table>
</form>

<p>&nbsp;</p>

<?php echo form_open($this->config->item('admin_folder').'/taobao/bulk_import', array('id'=>'taobao_form')); ?>
<table border="0" width="100%" class="table gc_table">
	<thead>
	<tr>
		<th class="gc_cell_left"><input type="checkbox" id="gc_check_all" /></th>
		<th>Detail</th>
		<th>Product Name</th>        
		<!-- <th>Num_iid</th> -->
		<!-- <th>Image</th> -->        
		<th>Seller</th>
		<th>Price</th>
		<th>Link</th>            
        <!-- <th>Volume</th> --> 		
		<th>Location</th>
		<th>Shop URL</th>   
		<th class="gc_cell_right">Action</th>		     
	</tr>
	</thead>
	<tbody>
	<?php
	if (empty($sub_msg)){
		if (is_array($TaobaokeItem)){
		foreach ($TaobaokeItem as $key => $val) { 
	?>
	<tr>
		<td><input name="product[]" type="checkbox" value="<?php echo $val['num_iid']; ?>" class="gc_check"/></td>
		<td>
			<!-- <a href=item.php?num_iid=<?php echo $val['num_iid'];?> target="_blank">view</a> -->
			<a href="taobao/detail/<?php echo $val['num_iid'];?>">view</a>
		</td>		
		<td>
            <?php echo $val['title'];?>
		</td>
		<!-- <td>
			<a href="javascript:;" title="<?php echo $val['num_iid'];?>">view</a>
		</td>     -->    
		<td>
			<?php echo $val['nick'];?>
		</td>
		<!-- <td>
			<a href="<?php echo $val['pic_url'];?>" target="_blank">é–¸ãƒ¥å¢½æ¾§?/a>
		</td> -->
		<td>
			<?php echo $val['price'];?>
		</td>
		<td>
			<a href="<?php echo $val['click_url'];?>" target="_blank">view</a>
		</td>
 		<!-- <td>
			<?php echo $val['num'];?>
		</td> -->
 		<td>
			<?php echo $val['item_location'];?>
		</td>                        
		<td>
			<a href="<?php echo $val['shop_click_url'];?>" target="_blank">website</a>
		</td>        
		<td>
			<a href="taobao/bookmark/<?php echo $val['num_iid'];?>">add item</a>
		</td>	
	</tr>
	<?php
		}
		}else{
		echo '<tr><td colspan="10" class="submsg" align="center">No record found.</td></tr>';
		}
	?>
	<?php if (is_array($TaobaokeItem)){ ?>
	<tr>
      <td colspan="15"> <?php
	// Paging new PageClass (total data per page number, page number, URL component);
	$pages = new PageClass($TaobaokeCount,$page_size,$page,'taobao?'.$_SERVER['QUERY_STRING'].'&page={page}');
	echo $pages->myde_write();
	?>
      </td>
    </tr>    
	<tr>
			<td colspan="15" class="gc_table_tools">
				<div class="gc_product_import">
					<a onclick="submit_form();" class="button">Batch Import</a>
				</div>
			</td>
		</tr>
	<?php } ?>
    <?
	}else{

	echo '<tr><td colspan=15 class="sub_msg">'.$sub_msg.'</td></tr>';		
	}
?>
	

	</tbody>
</table>
<script type="text/javascript">
$(document).ready(function(){
	$('#gc_check_all').click(function(){
		if(this.checked)
		{
			$('.gc_check').attr('checked', 'checked');
		}
		else
		{
			 $(".gc_check").removeAttr("checked"); 
		}
	});
});

function submit_form()
{
	if($(".gc_check:checked").length > 0)
	{
		if(confirm('Confirm import?'))
		{
			$('#taobao_form').submit();
		}
	}
	else
	{
		alert('No item selected.');
	}
}
</script>

<?php include('footer.php'); ?>