<table class="gc_table" cellspacing="0" cellpadding="0" style="margin-bottom:10px;">
	<thead>
		<tr>
			<?php /*<th>ID</th> uncomment this if you want it*/ ?>
			<th class="gc_cell_left">Customer</th>
			<th style="text-align:center;">Products Sold</th>
			<th class="gc_cell_right">Sales Total</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach($orders as $customer_id=>$order):?>
		<?php $cust = $this->Customer_model->get_customer($customer_id); ?>
		<tr class="gc_row">
			<td class="gc_cell_left"><?php echo $order['name']; ?></td>
			<td style="text-align:center;"><?php echo $order['product_totals'];?></td>
			<td class="gc_cell_right"><?php echo format_currency($order['total']);?></td>
		</tr>
		<?php endforeach;?>
	</tbody>
</table>

