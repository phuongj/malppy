<table class="gc_table" cellspacing="0" cellpadding="0">
	<thead>
		<tr>
			<?php /*<th>ID</th> uncomment this if you want it*/ ?>
			<th class="gc_cell_left">No.</th>
			<th>Datetime</th>
			<th>Debit</th>
			<th>Credit</th>
			<th>Balance</th>
			<th class="gc_cell_right">Remarks</th>			
		</tr>
	</thead>
	<tbody>
		<?php $index=0; $balance=0; foreach($best_sellers as $b):?>
		<?php if ($b->credit != "0.00") $balance = $balance - $b->credit; else $balance = $balance+ $b->debit ?>
		<tr class="gc_row">
			<?php /*<td style="width:16px;"><?php echo  $customer->id; ?></td>*/?>
			<td class="gc_cell_left"><?php echo  ++$index; ?></td>
			<td><?php echo $b->create_date; ?></td>
			<td><?php echo $b->debit; ?></td>
			<td><?php echo $b->credit; ?></td>
			<td><?php echo $balance; ?></td>
			<td class="gc_cell_right"><?php echo $b->remarks; ?></td>
		</tr>
		<?php endforeach;?>
	</tbody>
</table>