<?php require('header.php'); 
	
	//set "code" for searches
	if(!$code)
	{
		$code = '';
	}
	else
	{
		$code = '/'.$code;
	}
	function sort_url($by, $sort, $sorder, $code, $admin_folder)
	{
		if ($sort == $by)
		{
			if ($sorder == 'asc')
			{
				$sort	= 'desc';
			}
			else
			{
				$sort	= 'asc';
			}
		}
		else
		{
			$sort	= 'asc';
		}
		$return = site_url($admin_folder.'/products/index/'.$by.'/'.$sort.'/'.$code);
		return $return;
	}
			

	$pagination .= $pages;	
	
if ($term)
{
	$search_result = '<p id="searched_for"><div style="width:70%;float:left;"><strong>'.sprintf(lang('search_returned'), intval($total)).'</strong></div></p>';
	
}
?>
<script type="text/javascript">
function areyousure()
{
	return confirm('<?php echo lang('confirm_delete_product');?>');
}
function areyousure2()
{
	return confirm('Are you sure you want to publish this product?');
}
function areyousure3()
{
	return confirm('Are you sure you want to suspend this product?');
}
function view_category_product(id)
{	
	if (id) {
		location.href='<?php echo  site_url($this->config->item('admin_folder').'/products/category/');?>/'+id;
	} else {
		location.href='<?php echo  site_url($this->config->item('admin_folder').'/products');?>';
	}	
}
</script>
<style>
.pager {color:#333; position:relative;text-align:left;color:#555;margin-top:13px;}
.pager ul {display:inline;padding-left:0px;margin-left:0px}
.pager li {display:inline;list-style:none;text-align:center; margin:2px;}
.pager li a {border: 1px solid #CCCCCC;color: #555555;font-size: 10px !important;padding: 5px 9px !important;text-decoration: none !important;}
.pager li a.last {border:2px #ccc solid;}
.pager li a:hover {background-color: #333333; color: #fff!important; border: 1px solid #333;}
.pager li a.last:hover {border:2px #000 solid;}
.pager li.active a {background-color: #333;color: #FFF;border: 1px solid #333;}
.pager .next a, .pager .previous a {border: 1px #fff solid; padding: 5px 9px !important;  }
.pager .next a:hover, .pager .previous a:hover {font-weight:normal;}
.pager .total {font-size:80%;}
</style>

<div id="breadcrumb">
	<ul>
    	<li><a href="<?php echo site_url($this->config->item('admin_folder').'/products');?>">Inventory</a></li>
        <?php if ($catid) { ?>
		<li><a href="<?php echo site_url($this->config->item('admin_folder').'/products');?>">Live Products</a></li>
		<li class="last"><a href="#"><?php echo $this->Category_model->get_category($catid)->name; ?></a></li>
		<?php } else { ?>
		<li class="last"><a href="#">Live Products</a></li>
		<?php } ?>
    </ul>
</div><!-- End of breadcrumb --> 

<div class="button_set">
	<a href="<?php echo site_url($this->config->item('admin_folder').'/products/form');?>">Add New Product</a>
</div>	

	<div class="button_set" style="text-align:left;float:left">
	<!-- <?php $cats = $this->Category_model->get_categories(); ?>
	<select name="catid" id="catid" class="gc_tf1" onchange="view_category_product(this.value)" style="float:left">
	<option value="">All Products</option>
	<?php foreach ($cats as $cat):?>		
		<option id="cat_item_<?php echo $cat->id;?>" value="<?php echo $cat->id;?>" <?php if ($cat->id==$catid)  { ?>selected<?php } ?>><?php echo $cat->name;?></option>
	<?php endforeach; ?>
	</select> -->

	<select name="catid" id="catid" class="gc_tf1" onchange="view_category_product(this.value)">
		<option value="">Show All Products</option>
		<option value="-50" <?php if ($catid=='-50')  { ?>selected<?php } ?>>Show Sales Promo Products</option>
		<?php
		define('ADMIN_FOLDER', $this->config->item('admin_folder'));
		define('SELECTED_CATID', $catid);
		$this->load->model('Product_model');
		$Product_model = new Product_model;
		function list_categories_dropdown($cats, $sub='', $Product_model) {	
			foreach ($cats as $cat):?>
			<option id="cat_item_<?php echo $cat['category']->id;?>" value="<?php echo $cat['category']->id;?>" <?php if ($cat['category']->id==SELECTED_CATID)  { ?>selected<?php } ?>><?php echo  $sub.$cat['category']->name; ?> (<?php echo $Product_model->count_products($cat['category']->id); ?>)</option>
			<?php
			if (sizeof($cat['children']) > 0)
			{
				$sub2 = str_replace('-&nbsp;', '&nbsp;', $sub);
					$sub2 .=  '&nbsp;-&nbsp;';
				list_categories_dropdown($cat['children'], $sub2, $Product_model);
			}
			endforeach;
		}
		$cats_tierd = $this->Category_model->get_categories_tierd();
		
		list_categories_dropdown($cats_tierd,'',$Product_model);
		?>
	</select>
	
	<!-- <a href="#" onclick="$('#bulk_form').submit(); return false;"><?php echo lang('bulk_save');?></a> -->
	</div>
	<div class="pager" style="float:right">
		<?php echo $pagination; ?>
	</div>
	<div style="clear:both"></div>

<!-- <div style="clear:both;height:20px;"></div> -->
		
<?php echo form_open($this->config->item('admin_folder').'/products/bulk_save', array('id'=>'bulk_form'));?>

	<table class="gc_table" cellspacing="0" cellpadding="0">
		<thead>
			<tr>
				<th class="gc_cell_left">No</th>
				<th style="width:50px;"></th>
				<th><a href="<?php echo sort_url('name', $sort_by, $sortorder, $code, $this->config->item('admin_folder')); ?>">Product Name</a>&nbsp;<?php if ($sort_by=="name" && $sortorder=="desc") { ?><img src="<?php echo base_url('images/sortdown.png');?>" border="0"><?php } ?><?php if ($sort_by=="name" && $sortorder=="asc") { ?><img src="<?php echo base_url('images/sortup.png');?>" border="0"><?php } ?></th>
				<th style="width:100px;"><a href="<?php echo sort_url('num_iid', $sort_by, $sortorder, $code, $this->config->item('admin_folder')); ?>">Original SKU</a>&nbsp;<?php if ($sort_by=="num_iid" && $sortorder=="desc") { ?><img src="<?php echo base_url('images/sortdown.png');?>" border="0"><?php } ?><?php if ($sort_by=="num_iid" && $sortorder=="asc") { ?><img src="<?php echo base_url('images/sortup.png');?>" border="0"><?php } ?></th>
				<th style="width:100px;"><a href="<?php echo sort_url('sku', $sort_by, $sortorder, $code, $this->config->item('admin_folder')); ?>">Product Code</a>&nbsp;<?php if ($sort_by=="sku" && $sortorder=="desc") { ?><img src="<?php echo base_url('images/sortdown.png');?>" border="0"><?php } ?><?php if ($sort_by=="sku" && $sortorder=="asc") { ?><img src="<?php echo base_url('images/sortup.png');?>" border="0"><?php } ?></th>				
				<th style="width:50px;"><a href="<?php echo sort_url('price', $sort_by, $sortorder, $code, $this->config->item('admin_folder')); ?>">Cost <?php echo lang('price');?></a>&nbsp;<?php if ($sort_by=="price" && $sortorder=="desc") { ?><img src="<?php echo base_url('images/sortdown.png');?>" border="0"><?php } ?><?php if ($sort_by=="price" && $sortorder=="asc") { ?><img src="<?php echo base_url('images/sortup.png');?>" border="0"><?php } ?></th>
				<th style="width:50px;"><a href="<?php echo sort_url('saleprice', $sort_by, $sortorder, $code, $this->config->item('admin_folder')); ?>"><?php echo lang('saleprice');?></a>&nbsp;<?php if ($sort_by=="saleprice" && $sortorder=="desc") { ?><img src="<?php echo base_url('images/sortdown.png');?>" border="0"><?php } ?><?php if ($sort_by=="saleprice" && $sortorder=="asc") { ?><img src="<?php echo base_url('images/sortup.png');?>" border="0"><?php } ?></th>
				<th style="width:50px;">Promo Price</th>
				<th style="width:50px;text-align:center;"><a href="<?php echo sort_url('quantity', $sort_by, $sortorder, $code, $this->config->item('admin_folder')); ?>"><?php echo lang('quantity');?></a>&nbsp;<?php if ($sort_by=="quantity" && $sortorder=="desc") { ?><img src="<?php echo base_url('images/sortdown.png');?>" border="0"><?php } ?><?php if ($sort_by=="quantity" && $sortorder=="asc") { ?><img src="<?php echo base_url('images/sortup.png');?>" border="0"><?php } ?></th>
				<th style="width:60px;text-align:center;">Published?</th>
				<th style="width:60px;text-align:center;">Suspense?</th>
				<th style="width:90px;text-align:center;">Date added</th>
				<th style="width:90px;text-align:center;"><a href="<?php echo sort_url('updated', $sort_by, $sortorder, $code, $this->config->item('admin_folder')); ?>">Last edited</a>&nbsp;<?php if ($sort_by=="updated" && $sortorder=="desc") { ?><img src="<?php echo base_url('images/sortdown.png');?>" border="0"><?php } ?><?php if ($sort_by=="updated" && $sortorder=="asc") { ?><img src="<?php echo base_url('images/sortup.png');?>" border="0"><?php } ?></th>
				<th style="width:90px;text-align:center;">User</th>
				<th class="gc_cell_right"></th>
			</tr>
		</thead>
		<tbody>
		<?php echo (count($products) < 1)?'<tr><td style="text-align:center;" colspan="12">'.lang('no_products').'</td></tr>':''?>
	<?php $index+=$page; foreach ($products as $product):?>
			<tr class="gc_row">
				<td><?php echo ++$index;?></td>
				<td>
				<?php 
                $this_product = $this->Product_model->get_product($product->id);
				
				//$this_product->images	= (array)json_decode($this_product->images);
				//$this_product->images	= array_values($this_product->images);
				//$primary	= $this_product->images[0];
				$product_image = $this_product->product_image;
				
				$user = $this->Product_model->get_admin($product->adminid);
				
				//foreach($this_product->images as $image) { if(isset($image->primary)) $primary	= $image; }
                ?>
				<?php if (strpos($product_image, 'http') === 0) { ?>
			
					<img src="<?php echo base_url(); ?>phpthumb/phpThumb.php?src=<?php echo $product_image;?>&w=50&h=50&zc=1" alt=""/>
				<?php } else { ?>
					<img src="<?php echo base_url(); ?>phpthumb/phpThumb.php?src=<?php echo base_url('uploads/images/thumbnails/'.$product_image);?>&w=50&h=50&zc=1" alt=""/>
				<?php } ?>
				</td>
				<td><a href="<?php echo site_url($product->slug); ?>" target="_blank" style="padding:0px;"><?php echo $product->name;?></a><!-- <?php echo form_input(array('name'=>'product['.$product->id.'][name]','value'=>form_decode($product->name), 'class'=>'gc_tf3'));?> --></td>
				<td><?php if (!$product->buy_link) { ?><a href="taobao/detail/<?php echo $product->num_iid;?>"><?php } else { ?><a href="<?php echo $product->buy_link; ?>" target="_blank" style="padding:0px;"><?php } ?><?php echo $product->num_iid;?></a></td>
				<td><?php echo form_input(array('name'=>'product['.$product->id.'][sku]','value'=>form_decode($product->sku), 'class'=>'gc_tf2'));?></td>
				<td><?php echo @number_format($product->price/$bnd_to_rmb,2);?><!-- <?php echo form_input(array('name'=>'product['.$product->id.'][price]', 'value'=>set_value('price', $product->price), 'class'=>'gc_tf3'));?> --></td>
				<td><?php echo form_input(array('name'=>'product['.$product->id.'][saleprice]', 'value'=>set_value('saleprice', $product->saleprice), 'class'=>'gc_tf3'));?></td>
				<td><?php echo form_input(array('name'=>'product['.$product->id.'][promoprice]', 'value'=>set_value('promoprice', $product->promoprice), 'class'=>'gc_tf3'));?></td>
				<td style="text-align:center;"><?php echo $product->quantity;?><!-- <?php echo ((bool)$product->track_stock)?form_input(array('name'=>'product['.$product->id.'][quantity]', 'value'=>set_value('quantity', $product->quantity), 'class'=>'gc_tf3')):'N/A';?> --></td>
				<td style="text-align:center;">
					<?php
					$data	= array('name'=>'product['.$product->id.'][enabled]', 'value'=>1, 'checked'=>set_checkbox('product['.$product->id.'][enabled]', 1, (bool)$product->enabled));
					echo form_checkbox($data);
					?>
				</td>
				<td style="text-align:center;">
					<?php
					 	$options = array(
			                  ''	=> "",
			                  'out of stock'	=> "out of stock",
							  'out of style'	=> "out of style",
							  'invalid'			=> "invalid",
							  'to be removed'	=> "to be removed"
			                );

						echo form_dropdown('product['.$product->id.'][reason]', $options, set_value('reason',$product->reason));
					?>
				</td>
				<td style="text-align:center;"><?php echo date("d-m-Y",strtotime($product->created));?></td>
				<td style="text-align:center;"><?php echo date("d-m-Y",strtotime($product->updated));?></td>
				<td style="text-align:center;"><?php echo $user;?></td>
				<td class="gc_cell_right list_buttons">
					<!-- <a href="<?php echo  site_url($this->config->item('admin_folder').'/products/delete/'.$product->id);?>" onclick="return areyousure();"><?php echo lang('delete');?></a> -->
					<a href="<?php echo  site_url($this->config->item('admin_folder').'/products/form/'.$product->id);?>"><?php echo lang('edit');?></a>
					<!-- <a href="<?php echo  site_url($this->config->item('admin_folder').'/products/form/'.$product->id.'/1');?>"><?php echo lang('copy');?></a> -->
					<!-- <a href="<?php echo  site_url($this->config->item('admin_folder').'/products/publish/'.$product->id);?>" onclick="return areyousure2();">Publish</a> -->
					<!-- <a href="<?php echo  site_url($this->config->item('admin_folder').'/products/suspend/'.$product->id);?>" onclick="return areyousure3();">Suspend</a> -->
				</td>
			</tr>
	<?php endforeach; ?>
		</tbody>
	</table>
</form>
<div class="button_set">
	<a href="#" onclick="$('#bulk_form').submit(); return false;">Update</a>
	<!-- <a href="<?php echo site_url($this->config->item('admin_folder').'/products/form');?>"><?php echo lang('add_new_product');?></a> -->
</div>

<?php include('footer.php'); ?>