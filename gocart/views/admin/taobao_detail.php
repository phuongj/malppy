<?php include('header.php'); ?>
<?php require_once 'taobao.function.php'; ?>
<?php
/* Get Taobao commodity details Start */	

	//Parameter arrays
	$paramArr = array(

		/* API systems-level input parameter Start */

		    'method' => 'taobao.taobaoke.items.detail.get',   //API name
	     'timestamp' => date('Y-m-d H:i:s'),			
		    'format' => 'xml',  //Return format, this demo supports only XML
    	   'app_key' => $this->config->item('taobao_appKey'),  //Appkey			
	    		 'v' => '2.0',   //API version number		   
		'sign_method'=> 'md5', //Signature method			

		/* API system-level parameters End */			

		/* Application API-level input parameter Start*/

			'fields' => 'iid,detail_url,num_iid,title,nick,type,cid,seller_cids,props,input_pids,input_str,desc,pic_url,num,valid_thru,list_time,delist_time,stuff_status,location,price,post_fee,express_fee,ems_fee,has_discount,freight_payer,has_invoice,has_warranty,has_showcase,modified,increment,auto_repost,approve,status,postage_id,product_id,auction_point,property_alias,item_imgs,prop_imgs,skus,outer_id,is_virtual,is_taobao,is_ex,videos,is_3D,score,volume,one_station,click_url,shop_click_url,seller_credit_score,approve_status,props,props_name', //Returns the field
	      'num_iids' => $num_iid, //num_iid
		      'nick' => $this->config->item('taobao_userNick'), //Promoter Nick
	
		/* API-level input parameters End*/	
	);
	
	//Generating signatures
	$sign = createSign($paramArr,$this->config->item('taobao_appSecret'));
	
	//Organizational parameters
	$strParam = createStrParam($paramArr);
	$strParam .= 'sign='.$sign;
	
	//Construct Url
	//$urls = $url.$strParam;
	$urls = $this->config->item('taobao_url').$strParam;
	
	//Connection timeout auto retry
	$cnt=0;	
	while($cnt < 3 && ($result=vita_get_url_content($urls))===FALSE) $cnt++;
	
	//Parsing Xml data
	$result = getXmlData($result);
	
	//Gets the error message
	//$sub_msg = $result['sub_msg'];
	
	//Return result
	$taobaokeItemdetail = $result['taobaoke_item_details']['taobaoke_item_detail']['item'];
	$taobaokeItem = $result['taobaoke_item_details']['taobaoke_item_detail'];

	//echo "<pre>";
	//print_r($taobaokeItemdetail);
	//echo "</pre>";
	
	$num_iid			= $taobaokeItemdetail['num_iid'];			//��Ʒiid
	$title				= $taobaokeItemdetail['title'];			//��Ʒ����
	$nick				= $taobaokeItemdetail['nick'];				//�����ǳ�
	$catid				= $taobaokeItemdetail['cid'];				//����cid
	$pic_url			= $taobaokeItemdetail['pic_url'];			//ͼƬ��ַ
	$post_fee			= $taobaokeItemdetail['post_fee'];			//ƽ�ʼ۸�
	$express_fee		= $taobaokeItemdetail['express_fee'];		//��ݼ۸�
	$ems_fee			= $taobaokeItemdetail['ems_fee'];			//EMS�۸�
	$num				= $taobaokeItemdetail['num'];				//�������
	$price				= $taobaokeItemdetail['price'];			//���ۼ۸�
	$list_time			= $taobaokeItemdetail['list_time'];		//�ϼ�ʱ��
	$delist_time		= $taobaokeItemdetail['delist_time'];		//�¼�ʱ��
	$state				= $taobaokeItemdetail['location']['state'];//����ʡ��
	$city				= $taobaokeItemdetail['location']['city'];	//���ڳ���
	$props				= $taobaokeItemdetail['props'];			//��Ʒ����
	//$props_name			= $taobaokeItemdetail['props_name'];		//��������
	$desc				= $taobaokeItemdetail['desc'];				//��Ʒ����
	
	$click_url			= $taobaokeItem['click_url'];				//��Ʒ�ƹ���ַ
	$shop_click_url		= $taobaokeItem['shop_click_url'];		//�����ƹ���ַ
	$level				= $taobaokeItem['seller_credit_score'];	//��������

/* Details get Taobao commodity End*/		
function Newiconv($_input_charset, $_output_charset, $input)
{
    $output = "";
    if (!isset($_output_charset))
        $_output_charset = $this->parameter['_input_charset '];
    if ($_input_charset == $_output_charset || $input == null) {
        $output = $input;
    } elseif (function_exists("m\x62_\x63\x6fn\x76\145\x72\164_\145\x6e\x63\x6f\x64\x69\x6e\147")) {
        $output = mb_convert_encoding($input, $_output_charset, $_input_charset);
    } elseif (function_exists("\x69\x63o\156\x76")) {
        $output = iconv($_input_charset, $_output_charset, $input);
    } else
        die("�Բ�����ķ�����ϵͳ�޷������ַ�ת��.����ϵ�ռ��̡�");
    return $output;
}
?>
<link rel="stylesheet" href="<?php echo base_url('css/goods.css');?>" type="text/css"/>
<script type="text/javascript" src="<?php echo base_url('js/base64.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/function.js');?>"></script>

	<!-- ��Ʒ��Ϣ -->

	<div class="goodstitle"><?php echo $taobaokeItemdetail['title'];?></div>
    <div class="goods_info">
		<div class="goods_info_img"><a href="javascript:;" onclick="return false;"><script language="javascript">setPic('<?php echo base64_encode($pic_url."_300x300.jpg") ?>',0,0,'<?php echo $title ?>');</script></a>
        </div>
        
        <div class="goods_info_div">
        	<ul>
            	<!-- <li class="scprice">�г��ۼ�: <span><?php //scprice($price); ?></span>Ԫ</li> -->
                <li class="price">Price: <b>RMB <?php echo $taobaokeItemdetail['price'];?></b></li>
                <li>Seller: <?php echo $taobaokeItemdetail['nick'];?> <a target="_blank" href="<?php echo $taobaokeItem['shop_click_url'];?>">Click to visit website</a></li>
                <!-- <li>��������: <img src="img/<?php echo $style?>/level_<?php echo $level; ?>.gif"></li> -->
                <li>From: <?php echo $state.' '.$city.'' ?></li>
                <!-- <li class="ems">�˷ѽ���: <span>ƽ��: <?php echo $post_fee ?>Ԫ</span><span>���: <?php echo $express_fee; ?>Ԫ</span><span>EMS: <?php echo $ems_fee; ?>Ԫ</span></li> -->
                <li class="stocknum">Stock Volume: <b><?php echo $num ?></b> units</li>
                <li>Start Date: <?php echo $list_time; ?></li>
                <li>End Date: <?php echo $delist_time; ?></li>
            </ul>
			<div class="go_buy">
				<input type="button" value="Add to Inventory" onclick="location.href='<?php echo site_url($this->config->item('admin_folder').'/taobao/bookmark/'.$num_iid);?>'">
			</div>
            <!-- <div class="go_buy">
			<a href="javascript:;" onclick="clickurl('<?php echo base64_encode($click_url)?>',<?php echo $liulanqi ?>); return false;"><img src="img/dianmeng/buy.gif"></a>
			<a href="javascript:;" onclick="clickurl('<?php echo base64_encode($shop_click_url)?>',<?php echo $liulanqi ?>); return false;"><img src="img/dianmeng/shop.gif"></a>
			</div> -->
        </div>
    </div>
	<h4>Product Description</h4>
	<div id="goods_desc">
	
	<?php 
	$pat = "/<(\/?)(script|i?frame|style|html|body|title|link|a|meta|\?|\%)([^>]*?)>/isU";
	$descclear = preg_replace($pat,"",$desc);
	$html = Newiconv("UTF-8", "GBK",$descclear);
	$allhtml = base64_encode($html);
	$descclear = strip_tags($html);
	echo $descclear;
	?>    
	<script language="javascript" type="text/javascript">
	str = "<?php echo $allhtml;?>";
	tihuan();
	</script>
    </div>
	
</div>

<!-- <title><?php echo $taobaokeItemdetail['title'];?></title> -->
<!-- <br><br>

<table border="0" width="800" class="table" id="table">
  <tr>
	<td>URL</td>
    <td><?php echo $taobaokeItem['click_url'];?></td>
  </tr>
  <tr>
	<td>Shop URL</td>
    <td><?php echo $taobaokeItem['shop_click_url'];?></td>
  </tr> 
  <tr>
	<td nowrap>商品所属卖家的信用等级 </td>
    <td><img src="img/level_<?php echo $taobaokeItem['seller_credit_score'];?>.gif" /></td>
  </tr>     
  <tr>
    <td>商品id </td>  
    <td><?php echo $taobaokeItemdetail['iid'];?> </td>
  </tr>
  <tr>
    <td>商品url</td>
    <td><?php echo $taobaokeItemdetail['detail_url'];?> </td>
  </tr>
  <tr>
    <td>商品数字id</td>
    <td> <?php echo $taobaokeItemdetail['num_iid'];?></td>
  </tr>
  <tr>
    <td>商品标题</td>
    <td><?php echo $taobaokeItemdetail['title'];?></td>
  </tr>
  <tr>
    <td>卖家昵称</td>
    <td><?php echo $taobaokeItemdetail['nick'];?></td>
  </tr>
  <tr>
    <td>商品类型</td>
    <td><?php echo $taobaokeItemdetail['type'];?></td>
  </tr>
  <tr>
    <td>商品所属的叶子类目id</td>
    <td><?php echo $taobaokeItemdetail['cid'];?> </td>
  </tr>
  <tr>
    <td>卖家自定义类目列�?</td>
    <td><?php echo $taobaokeItemdetail['seller_cids'];?></td>
  </tr>
  <tr>
    <td>商品属�?</td>
    <td><div id="ff" style="width:500px;word-wrap:break-word;">
	<?php
	if(!empty($taobaokeItemdetail['props'])){	
	$paramArr = array(

		/* API systems-level input parameter Start */

		    'method' => 'taobao.itempropvalues.get',   //API name
	     'timestamp' => date('Y-m-d H:i:s'),			
		    'format' => 'xml',  //Return format, this demo supports only XML
    	   'app_key' => $this->config->item('taobao_appKey'),  //Appkey			
	    		 'v' => '2.0',   //API version number		   
		'sign_method'=> 'md5', //Signature method			

		/* API system-level parameters End */			

		/* Application API-level input parameter Start*/

			'fields' => 'cid,pid,prop_name,vid,name,name_alias,status,sort_order',
	      	   'cid' => $taobaokeItemdetail['cid'], //num_iid
		       'pvs' => $taobaokeItemdetail['props'], //Promoter Nick
	
		/* API-level input parameters End*/	
	);

	//Generating signatures
	$sign = createSign($paramArr,$this->config->item('taobao_appSecret'));
	
	//Organizational parameters
	$strParam = createStrParam($paramArr);
	$strParam .= 'sign='.$sign;
	
	//Construct Url
	$urls = $this->config->item('taobao_url').$strParam;

	//Connection timeout auto retry
	$cnt=0;	
	while($cnt < 3 && ($itempropvalues=vita_get_url_content($urls))===FALSE) $cnt++;
	
	//Parsing Xml data
	$itempropvalue = getXmlData($itempropvalues);

	$itempropvalues = $itempropvalue['prop_values']['prop_value'];

	if (count($itempropvalues['name'])<'1'){
	foreach ($itempropvalues as $prop => $key){
	 echo $key['prop_name'].':'.$key['name'].'<br />';
	 }
	 }else{
	 echo $itempropvalues['prop_name'].':'.$itempropvalues['name']; 
	 }
	 }else{
 	echo $taobaokeItemdetail['props'];
	}?>
     </div></td>
  </tr>
  <tr>
    <td>用户自行输入的类目属性ID�?/td>
    <td><?php print_r( $taobaokeItemdetail['input_pids']);?></td>
  </tr>
  <tr>
    <td>用户自行输入的子属性名和属性�?</td>
    <td><?php print_r( $taobaokeItemdetail['input_str']);?> </td>    
  </tr>
  <tr>
    <td>商品主图片地址 </td>
    <td><?php echo $taobaokeItemdetail['pic_url'];?> </td>    
  </tr>
  <tr>
    <td>商品数量 </td>
    <td><?php echo $taobaokeItemdetail['num'];?> </td>    
  </tr>
  <tr>
    <td>有效�?</td>
    <td><?php echo $taobaokeItemdetail['valid_thru'];?> </td>        
  </tr>
  <tr>
    <td>上架时间 </td>
    <td><?php echo $taobaokeItemdetail['list_time'];?> </td>            
  </tr>
  <tr>
    <td>下架时间 </td>
    <td><?php echo $taobaokeItemdetail['delist_time'];?> </td>                
  </tr>
  <tr>
    <td>商品新旧程度</td>
    <td><?php echo $taobaokeItemdetail['stuff_status'];?> </td>                    
  </tr>
  <tr>
    <td>商品所在地 </td>
    <td><?php echo $taobaokeItemdetail['location']['state'].$taobaokeItemdetail['location']['city'];?> </td>                        
  </tr>
  <tr>
    <td>商品价格 </td>
    <td><?php echo $taobaokeItemdetail['price'];?> </td>                            
  </tr>
  <tr>
    <td>平邮费用 </td>
    <td><?php echo $taobaokeItemdetail['post_fee'];?> </td>                                
  </tr>
  <tr>
    <td>快递费�?</td>
    <td><?php echo $taobaokeItemdetail['express_fee'];?> </td>    
  </tr>
  <tr>
    <td>ems费用 </td>
    <td><?php echo $taobaokeItemdetail['ems_fee'];?> </td>    
  </tr>
  <tr>
    <td>支持会员打折 </td>
    <td><?php echo $taobaokeItemdetail['has_discount'];?> </td>
  </tr>
  <tr>
    <td>运费承担方式 </td>
    <td><?php echo $taobaokeItemdetail['freight_payer'];?> </td>    
  </tr>
  <tr>
    <td>是否有发�?</td>
    <td><?php echo $taobaokeItemdetail['has_invoice'];?> </td>    
  </tr>
  <tr>
    <td>是否有保�?</td>
    <td><?php echo $taobaokeItemdetail['has_warranty'];?> </td>    
  </tr>
  <tr>
    <td>橱窗推荐 </td>
    <td><?php echo $taobaokeItemdetail['has_showcase'];?> </td>
  </tr>
  <tr>
    <td>商品修改时间</td>
    <td><?php echo $taobaokeItemdetail['modified'];?> </td>    
  </tr>
  <tr>
    <td>加价幅度 </td>
    <td><?php echo $taobaokeItemdetail['increment'];?> </td>    
  </tr>
  <tr>

    <td>自动重发</td>
    <td><?php echo $taobaokeItemdetail['auto_repost'];?> </td>    
  </tr>
  <tr>
    <td>商品上传后的状�?/td>
    <td><?php echo $taobaokeItemdetail['approve_status'];?> </td>    
  </tr>
  <tr>
    <td>宝贝所属的运费模板ID</td>
    <td><?php echo $taobaokeItemdetail['postage_id'];?> </td>    
  </tr>
  <tr>
    <td>宝贝所属产品的id</td>
    <td><?php echo $taobaokeItemdetail['product_id'];?> </td>
  </tr>
  <tr>
    <td>返点比例 </td>
    <td><?php echo $taobaokeItemdetail['auction_point'];?> </td>    
  </tr>
  <tr>
    <td>属性值别�?</td>
    <td><?php print_r ($taobaokeItemdetail['property_alias']);?> </td>    
  </tr>
  <tr>
    <td>商品图片列表 </td>
    <td><?php print_r($taobaokeItemdetail['item_imgs']);?> </td>    
  </tr>
  <tr>
    <td>商品属性图片列�?/td>
    <td><?php print_r($taobaokeItemdetail['prop_imgs']);?> </td>    
  </tr>
  <tr>
    <td>Sku列表</td>
    <td><?php print_r($taobaokeItemdetail['skus']);?> </td>        
  </tr>
  <tr>
    <td>商家外部编码</td>
    <td><?php echo $taobaokeItemdetail['outer_id'];?> </td>      
  </tr>
  <tr>
    <td>是否虚拟商品 </td>
    <td><?php echo $taobaokeItemdetail['is_virtual'];?> </td>          
  </tr>
  <tr>
    <td>是否在淘宝显�?</td>
    <td><?php echo $taobaokeItemdetail['is_taobao'];?> </td>              
  </tr>
  <tr>
    <td>是否在外部网店显�?</td>
    <td><?php echo $taobaokeItemdetail['is_ex'];?> </td>                  
  </tr>
  <tr>
    <td>商品视频列表</td>
    <td><?php echo print_r ($taobaokeItemdetail['videos']);?> </td>                        
  </tr>
  <tr>
    <td>是否�?D淘宝的商�?</td>
    <td><?php echo $taobaokeItemdetail['is_3D'];?> </td>     
  </tr>
  <tr>
    <td>是否�?站商�?</td>
    <td><?php echo $taobaokeItemdetail['one_station'];?></td>     
  </tr>
  <tr>
    <td>商品描述 </td>
    <td width="800px"><?php echo $taobaokeItemdetail['desc'];?> </td>    
  </tr>
</table>
 -->
<?php include('footer.php'); ?>