<?php include('header.php'); ?>
<?php
$atts = array(
 	       'width'      => '1280',
           'height'     => '768',
           'scrollbars' => 'yes',
           'status'     => 'yes',
           'resizable'  => 'yes',
           'screenx'    => '0',
           'screeny'    => '0'
         );
?>
<script type="text/javascript" src="<?php echo base_url('js/base64.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('js/function.js');?>"></script>
<style type="text/css">
.sortable { list-style-type: none; margin: 0; padding: 0; width: 100%; }
.sortable li { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; height: 30px; }
.sortable li>span { position: absolute; margin-left: -1.3em; margin-top:.4em; }
.option_item_form { margin-top: 10px; }
</style>
<script type="text/javascript">
//<![CDATA[

$(document).ready(function() {
	$(".sortable").sortable();
	$(".sortable > span").disableSelection();
	//if the image already exists (phpcheck) enable the selector

	<?php if($id) : ?>
	//options related
	var ct	= $('#option_list').children().size();
	//create_sortable();
	set_accordion();
	
	// set initial count
	option_count = <?php echo count($product_options); ?>;
	
	<?php endif; ?>

	$( ".add_option" ).button().click(function(){
		add_option($(this).attr('rel'));
	});
	$( "#add_buttons" ).buttonset();
	
	photos_sortable();
});

function add_product_image(data,imgid)
{
	//p	= data.split('.');
	
	//var photo = '<?php add_image(time(), "'+p[0]+'.'+p[1]+'", '', '');?>';

	var photo = '<?php add_image("'+imgid+'", "'+data+'", '', '','',1);?>';

	$('#gc_photos').prepend(photo);
	$('#gc_photos').sortable('destroy');
	photos_sortable();
	
	$('.button').button();
	
	var id	= $("#download_"+imgid).attr('rel');
	var filename = $("#download_"+imgid).attr('filename');
		
	$('#download_'+id).after('<span class="img_loader_'+id+'"><img src="<?php echo base_url(); ?>images/loading.gif"></span>');
		
	$.post('<?php echo $this->config->item('admin_folder').'/../../../products/download_image';?>',{id:id,filename:filename,num_iid:<?php echo $num_iid;?>}, function(data) {
			  if (data=='success') {
			  	$('.img_loader_'+id).remove();
			  }
			  $('#download_'+id).html("<span class='ui-button-text'>Downloaded</span>");
	});

	$.post('<?php echo $this->config->item('admin_folder').'/../../../taobao/save/'.$num_iid.'/false';?>', $("#product_form").serialize(), function(data) {});
	
}

function remove_image(img)
{
	if(confirm('Confirm delete?'))
	{
		var id	= img.attr('rel')
		$('#gc_photo_'+id).remove();
	}
}

function add_option(type)
{
	
	if(jQuery.trim($('#option_name').val()) != '')
	{
		//increase option_count by 1
		option_count++;
		
		$('#options_accordion').append('<?php add_option("'+$('#option_name').val()+'", "'+option_count+'", "'+type+'");?>');
		
		
		//eliminate the add button if this is a text based option
		if(type == 'textarea' || type == 'textfield')
		{
			$('#add_item_'+option_count).remove();
			
		}
		
		add_item(type, option_count);
		
		//reset the option_name field
		$('#option_name').val('');
		reset_accordion();
		
	}
	else
	{
		alert('<?php echo lang('alert_must_name_option');?>');
	}
	
}

function add_item(type, id)
{
	
	var count = $('#option_items_'+id+'>li').size()+1;
	
	append_html = '';
	
	if(type!='textfield' && type != 'textarea')
	{
		append_html = append_html + '<li id="value-'+id+'-'+count+'"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span><a onclick="if(confirm(\'<?php echo lang('confirm_remove_value');?>\')) $(\'#value-'+id+'-'+count+'\').remove()" class="ui-state-default ui-corner-all" style="float:right;"><span class="ui-icon ui-icon-circle-minus"></span></a>';
	}
	
	append_html += '<div style="margin:2px"><span><?php echo lang('name');?>: </span> <input class="req gc_tf2" type="text" name="option['+id+'][values]['+count+'][name]" value="" /> '+
	'<span><?php echo lang('price');?>: </span> <input class="req gc_tf2" type="text" name="option['+id+'][values]['+count+'][price]" value="" />';
	
	if(type == 'textfield')
	{
		append_html += ' <span><?php echo lang('limit');?>: </span> <input class="req gc_tf2" type="text" name="option['+id+'][values]['+count+'][limit]" value="" />';
	}

	append_html += '</div> ';
	
	if(type!='textfield' && type != 'textarea')
	{
		append_html += '</li>';
	}
	
	
	$('#option_items_'+id).append(append_html);	
	
	$(".sortable").sortable();
	$(".sortable > span").disableSelection();
	
	
}

function remove_option(id)
{
	if(confirm('<?php echo lang('confirm_remove_option');?>'))
	{
		$('#option-'+id).remove();
		
		option_count --;
		
		reset_accordion();
	}
}

function download_image(img)
{
	if(confirm('Confirm download?'))
	{
		var id	= img.attr('rel');
		var filename = img.attr('filename');		
		
		$('#download_'+id).after('<span class="img_loader_'+id+'"><img src="<?php echo base_url(); ?>images/loading.gif"></span>');
		
		$.post('<?php echo $this->config->item('admin_folder').'/../../../products/download_image';?>',{id:id,filename:filename,num_iid:<?php echo $num_iid;?>}, function(data) {
			  if (data=='success') {
			  	alert('Image downloaded!');
				$('.img_loader_'+id).remove();
			  }
			  $('#download_'+id).html("<span class='ui-button-text'>Downloaded</span>");
		});		
	}
}

function photos_sortable()
{
	$('#gc_photos').sortable({	
		handle : '.gc_thumbnail',
		items: '.gc_photo',
		axis: 'y',
		scroll: true
	});
}

function reset_accordion()
{
	$( "#options_accordion" ).accordion('destroy');
	$('.option_item_form').sortable('destroy');
	set_accordion();
}

function set_accordion(){
	
	var stop = false;
	$( "#options_accordion h3" ).click(function( event ) {
		if ( stop ) {
			event.stopImmediatePropagation();
			event.preventDefault();
			stop = false;
		}
	});
	
	$( "#options_accordion" ).accordion({
		autoHeight: false,
		active: option_count-1,
		header: "> div > h3"
	}).sortable({
		axis: "y",
		handle: "h3",
		stop: function() {
			stop = true;
		}
	});
	

	$('.option_item_form').sortable({
		axis: 'y',
		handle: 'span',
		stop: function() {
			stop = true;
		}
	});
	
	
}
function delete_product_option(id)
{
	//remove the option if it exists. this function is also called by the lightbox when an option is deleted
	$('#options-'+id).remove();
}
//]]>
</script>
<?php 
$_SESSION['archiv_num_iid'] = $num_iid; //for archiv plugin 
@setcookie(archiv_num_iid, $num_iid, time()+36000, "/");
?>
<?php echo form_open($this->config->item('admin_folder').'/taobao/save/'.$num_iid, 'id="product_form"'); ?>
<div class="button_set">
	<!-- <input name="submit" type="submit" value="Approve Product" onclick="$('#product_form').attr('action','<?php echo site_url($this->config->item('admin_folder').'/taobao/insert/'.$num_iid); ?>')" />&nbsp;&nbsp;&nbsp; -->
	<input name="submit" type="submit" value="Save" />
	<?php echo anchor_popup('http://www.malppy.com/cart/preview/'.$id, 'Preview', $atts); ?>
	<!-- <input name="button" type="button" value="Done" onclick="javascript:location.href='<?php echo site_url($this->config->item('admin_folder').'/taobao/inventory'); ?>'" />&nbsp;&nbsp;&nbsp -->
</div>

<div id="gc_tabs">
	<ul>
		<li><a href="#gc_product_info">Details <?php echo $editing_mode;?></a></li>
		<?php if ($editing_mode == 'pending_approval') {?>
		<li><a href="#gc_product_attributes"><?php echo lang('attributes');?></a></li>
		<?php }?>
		<li><a href="#gc_product_categories">Category</a></li>
		<li><a href="#gc_product_seo"><?php echo lang('seo');?></a></li>
		<li><a href="#gc_product_options"><?php echo lang('options');?></a></li>
		<li><a href="#gc_product_image">Images</a></li>		
	</ul>
	
	<div id="gc_product_info">
		
		<div class="gc_field2" style="margin-bottom:15px">
		<label style="margin-bottom:3px">Original Product Name </label><br>
		<b><font style="font-size:14px"><?php echo $original_name; ?></font></b>
		</div>
		<div class="gc_field2">
		<label for="weight">Weight (grams)</label>
		<?php
		$data	= array('id'=>'weight', 'name'=>'weight', 'value'=>set_value('weight', $weight), 'class'=>'gc_tf1');
		echo form_input($data);
		?>
		</div>
		<div class="gc_field">
		<label for="weight">Product Name</label>
		<div class="wysiwyg_textarea">
		<?php
		//$data	= array('id'=>'name', 'name'=>'name', 'value'=>set_value('name', $name), 'class'=>'gc_tf1');
		//echo form_input($data);
		$data	= array('id'=>'name', 'name'=>'name', 'class'=>'tinyMCE', 'style'=>'height:30px', 'value'=>set_value('name', $name));
		echo form_textarea($data);
		?>
		</div>
		</div>
		<div class="button_set wysiwyg_button">
			<input type="button" onclick="toggleEditor('name'); return false;" value="Toggle WYSIWYG" />
		</div>
			
		<div class="gc_field2">
		<label for="description">Product Description</label>
		<div class="wysiwyg_textarea">
		<?php
		if ($description==$original_description) $description = "";
		$data	= array('id'=>'description', 'name'=>'description', 'class'=>'tinyMCE', 'style'=>'height:800px', 'value'=>set_value('description', stripslashes($description)));
		//$data	= array('id'=>'description', 'name'=>'description', 'class'=>'tinyMCE', 'value'=>"");
		echo form_textarea($data);
		?>
		</div>
		</div>
		<div class="button_set wysiwyg_button">
			<input type="button" onclick="toggleEditor('description'); return false;" value="Toggle WYSIWYG" />
		</div>

		<div class="gc_field">
		<label>Short Description</label>
		<div class="wysiwyg_textarea">
		<?php
		//$data	= array('id'=>'excerpt', 'name'=>'excerpt', 'value'=>set_value('excerpt', $excerpt), 'class'=>'gc_tf1', 'style'=>'height:100px');
		//echo form_textarea($data);
		$data	= array('id'=>'excerpt', 'name'=>'excerpt', 'class'=>'tinyMCE', 'style'=>'height:100px', 'value'=>set_value('excerpt', $excerpt));
		echo form_textarea($data);
		?>
		</div>
		</div>
		<div class="button_set wysiwyg_button">
			<input type="button" onclick="toggleEditor('excerpt'); return false;" value="Toggle WYSIWYG" />
		</div>

		<div class="gc_field">
		<label>Info & Care</label>
		<div class="wysiwyg_textarea">
		<?php
		$data	= array('id'=>'info', 'name'=>'info', 'class'=>'tinyMCE', 'style'=>'height:300px', 'value'=>set_value('info', $info));
		echo form_textarea($data);
		?>
		</div>
		</div>
		<div class="button_set wysiwyg_button">
			<input type="button" onclick="toggleEditor('info'); return false;" value="Toggle WYSIWYG" />
		</div>

		<div class="gc_field">
		<label>Specification</label>
		<div class="wysiwyg_textarea">
		<?php
		$data	= array('id'=>'specification', 'name'=>'specification', 'class'=>'tinyMCE', 'style'=>'height:300px', 'value'=>set_value('specification', $specification));
		echo form_textarea($data);
		?>
		</div>
		</div>
		<div class="button_set wysiwyg_button">
			<input type="button" onclick="toggleEditor('specification'); return false;" value="Toggle WYSIWYG" />
		</div>

		<p></p>
		
		<label>Original Text</label>
		<p><iframe src="<?php echo site_url($this->config->item('admin_folder').'/taobao/description/'.$num_iid);?>" style="width:100%; height:800px; border:0px;">
			</iframe>
		<?php //echo stripslashes($original_description); ?></p>
	</div>
	
	<?php if ($editing_mode == 'pending_approval') {?>
	<div id="gc_product_attributes">
		<div class="gc_field2">
		<label for="price"><?php //echo lang('price');?>Cost Price </label>
		<?php echo $price; ?> (RMB) &nbsp&nbsp <?php echo @number_format($price/$bnd_to_rmb,2);?> (<?php echo $this->config->item('currency'); ?>)
		</div>
		<div class="gc_field2">
		<label for="price">Selling price (<?php echo $this->config->item('currency'); ?>)</label>
		<?php
		$data	= array('id'=>'saleprice', 'name'=>'saleprice', 'value'=>set_value('saleprice', $saleprice), 'class'=>'gc_tf1');
		echo form_input($data);
		?>
		</div>
		<!-- <div class="gc_field2">
		<label for="price">Promo Price (<?php echo $this->config->item('currency'); ?>)</label>
		<?php
		$data	= array('id'=>'promoprice', 'name'=>'promoprice', 'value'=>set_value('promoprice', $promoprice), 'class'=>'gc_tf1');
		echo form_input($data);
		?>
		</div> -->
		<input type="hidden" name="promoprice" value="<?php echo $promoprice; ?>" />

		
		
		<input type="hidden" name="track_stock" value="1" />
        
		<!-- <div class="gc_field2">
		<label for="quantity"><?php echo lang('quantity');?> </label>
		<?php
		$data	= array('id'=>'quantity', 'name'=>'quantity', 'value'=>set_value('quantity', $quantity), 'class'=>'gc_tf1');
		echo form_input($data);
		?>&nbsp;&nbsp;<small>Last updated: <?php echo $stock_updated; ?><?php //echo lang('quantity_in_stock_note');?></small>
		</div> -->

		<input type="hidden" name="quantity" value="<?php echo $quantity; ?>" />

		<!-- <div class="gc_field2">
		<label for="slug"><?php echo lang('shippable');?> </label>
		<?php
		$options = array(	 '1'	=> lang('yes')
							,'0'	=> lang('no')
							);
			echo form_dropdown('shippable', $options, set_value('shippable',$shippable));
		?>
		</div> -->
		<!-- <input type="hidden" name="shippable" value="1" />
		
		<div class="gc_field2">
		<label for="slug"><?php echo lang('taxable');?> </label>
		<?php
		$options = array(	 '1'	=> lang('yes')
							,'0'	=> lang('no')
							);
			echo form_dropdown('taxable', $options, set_value('taxable',$taxable));
		?>
		</div> -->
		<input type="hidden" name="taxable" value="1" />

		<!-- <div class="gc_field2">
		<label for="slug">Sale Item? </label>
		<?php
		 	$options = array(	 '1'	=> lang('yes')
								,'0'	=> lang('no')
								);
			echo form_dropdown('sale', $options, set_value('sale',$sale));
		?>
		</div> -->

		<!-- <div class="gc_field2">
		<label for="slug">New Arrival? </label>
		<?php
		 	$options = array(	 '1'	=> lang('yes')
								,'0'	=> lang('no')
								);
			echo form_dropdown('new', $options, set_value('new',$new));
		?>
		</div> -->
		<input type="hidden" name="new" value="1" />
		
	</div>
	<?php } ?>
	<div id="gc_product_image">
		<div class="gc_segment_content">
			<iframe src="<?php echo site_url($this->config->item('admin_folder').'/taobao/product_image_form');?>" style="height:75px; border:0px;">
			</iframe>
			<p>* Please tick checkbox for wanted images</p>
			<div id="gc_photos">
			<?php
			
			foreach($images as $photo_id=>$photo_obj)
			{
				if(!empty($photo_obj))
				{
					$photo = (array)$photo_obj;
					@add_image($photo_id, $photo['filename'], "", "", "", $photo['wanted']);
				}
				
			}
			?>
			</div>
		</div>
	</div>

	<div id="gc_product_seo">
		<div class="gc_field2">
		<label for="slug"><?php //echo lang('slug');?>URL nicename </label>
		<?php
		$data	= array('id'=>'slug', 'name'=>'slug', 'value'=>set_value('slug', $slug), 'class'=>'gc_tf1');
		echo form_input($data);
		?>
		</div>
		<div class="gc_field2">
		<label for="seo_title"><?php echo lang('seo_title');?> </label>
		<?php
		$data	= array('id'=>'seo_title', 'name'=>'seo_title', 'value'=>set_value('seo_title', $seo_title), 'class'=>'gc_tf1');
		echo form_input($data);
		?>
		</div>
		
		<div class="gc_field">
		<label><?php echo lang('meta');?></label> <small><?php echo lang('meta_example');?></small>
		<?php
		$data	= array('id'=>'meta', 'name'=>'meta', 'value'=>set_value('meta', html_entity_decode($meta)), 'class'=>'gc_tf1');
		echo form_textarea($data);
		?>
		</div>
	</div>

	<div id="gc_product_options">
	
		
		<div id="selected_options" class="option_form">
			
				<span id="add_buttons" style="float:left;">
					<input class="gc_tf2" id="option_name" style="width:200px;" type="text" name="option_name" />
					<!-- <button type="button" class="add_option" rel="checklist"><?php echo lang('checklist');?></button>
					<button type="button" class="add_option" rel="radiolist"><?php echo lang('radiolist');?></button> -->
					<button type="button" class="add_option" rel="droplist">Add Option<?php //echo lang('droplist');?></button>
					<!-- <button type="button" class="add_option" rel="textfield"><?php echo lang('textfield');?></button>
					<button type="button" class="add_option" rel="textarea"><?php echo lang('textarea');?></button> -->
				</span>

			<br style="clear:both;"/>
			<div id="options_accordion">
			<?php 
				$count	= 0;
				if(!empty($product_options)):
					//print_r($product_options);
					foreach($product_options as $option):
						//print_r($option);
						$option	= (object)$option;
						
						if(empty($option->required))
						{
							$option->required = false;
						}
					?>
						<div id="option-<?php echo $count;?>">
							<h3><a href="#"><?php echo $option->type.' > '.$option->name; ?> </a></h3>
							
							<div style="text-align: left">
								<?php echo lang('option_name');?>
								
									<a style="float:right;margin-right:20px" onclick="remove_option(<?php echo $count ?>)" class="ui-state-default ui-corner-all" ><span class="ui-icon ui-icon-circle-minus"></span></a>
								
								<input class="input gc_tf2" type="text" name="option[<?php echo $count;?>][name]" value="<?php echo $option->name;?>"/>
								
								<input type="hidden" name="option[<?php echo $count;?>][type]" value="<?php echo $option->type;?>" />
								<input class="checkbox" type="checkbox" name="option[<?php echo $count;?>][required]" value="1" <?php echo ($option->required)?'checked="checked"':'';?>/> <?php echo lang('required');?>
								
								<?php if($option->type!='textarea' && $option->type!='textfield') { ?>
								<button id="add_item_<?php echo $count;?>" type="button" rel="<?php echo $option->type;?>"onclick="add_item($(this).attr('rel'), <?php echo $count;?>);"><?php echo lang('add_item');?></button>
								<?php } ?>
								
								
								<div class="option_item_form">
								<?php if($option->type!='textarea' && $option->type!='textfield') { ?><ul class="sortable" id="option_items_<?php echo $count;?>"><?php } ?>
								<?php if(!empty($option->values))
											$valcount = 0;
											foreach($option->values as $value) : 
												$value = (object)$value;?>
									
										<?php if($option->type!='textarea' && $option->type!='textfield') { ?><li id="value-<?php echo $count;?>-<?php echo $valcount;?>"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span><?php } ?>
										<div  style="margin:2px"><span><?php echo lang('name');?> </span><input class="req gc_tf2" type="text" name="option[<?php echo $count;?>][values][<?php echo $valcount ?>][name]" value="<?php echo $value->name ?>" />
						
										<!-- <span><?php echo lang('value');?> </span><input class="req gc_tf2" type="text" name="option[<?php echo $count;?>][values][<?php echo $valcount ?>][value]" value="<?php echo $value->value ?>" /> -->
										<!-- <span><?php echo lang('weight');?> </span><input class="req gc_tf2" type="text" name="option[<?php echo $count;?>][values][<?php echo $valcount ?>][weight]" value="<?php echo $value->weight ?>" /> -->
										<span><?php echo lang('price');?> </span><input class="req gc_tf2" type="text" name="option[<?php echo $count;?>][values][<?php echo $valcount ?>][price]" value="<?php echo $value->price ?>" />
										<?php if($option->type == 'textfield'):?>
										
										<span><?php echo lang('limit');?> </span><input class="req gc_tf2" type="text" name="option[<?php echo $count;?>][values][<?php echo $valcount ?>][limit]" value="<?php echo $value->limit ?>" />

										<?php endif;?>
										<?php if($option->type!='textarea' && $option->type!='textfield') { ?>
										<a onclick="if(confirm('<?php echo lang('confirm_remove_value');?>')) $('#value-<?php echo $count;?>-<?php echo $valcount;?>').remove()" class="ui-state-default ui-corner-all" style="float:right;"><span class="ui-icon ui-icon-circle-minus"></span></a>
										<?php } ?>
										</div>
										<?php if($option->type!='textarea' && $option->type!='textfield') { ?>
										</li>
										<?php } ?>
										
									
								<?php	$valcount++;
								 		endforeach;  ?>
								 <?php if($option->type!='textarea' && $option->type!='textfield') { ?></ul><?php } ?>
								</div>
								
								
							</div>
						</div>

					<?php 
					
					
					$count++; 
					endforeach;
				endif;
				?>
				
				</div>
		</div>
	</div>
	
	<div id="gc_product_categories">
		<table class="gc_table" cellspacing="0" cellpadding="0">
		    <thead>
				<tr>
					<th class="gc_cell_left" style="text-align:left"><?php echo lang('name');?></th>
					<th class="gc_cell_right"></th>
				</tr>
			</thead>
			<tbody>
				<?php
				define('ADMIN_FOLDER', $this->config->item('admin_folder'));
				function list_categories($cats, $product_categories, $sub='') {
					
					foreach ($cats as $cat):?>
					<tr class="gc_row">
						<td><?php echo  $sub.$cat['category']->name; ?></td>
						<td style="text-align:right">
							<input type="checkbox" name="categories[]" value="<?php echo $cat['category']->id;?>" <?php echo (in_array($cat['category']->id, $product_categories))?'checked="checked"':'';?>/>
						</td>
					</tr>
					<?php
					if (sizeof($cat['children']) > 0)
					{
						$sub2 = str_replace('&rarr;&nbsp;', '&nbsp;', $sub);
							$sub2 .=  '&nbsp;&nbsp;&nbsp;&rarr;&nbsp;';
						list_categories($cat['children'], $product_categories, $sub2);
					}
					endforeach;
				}

				list_categories($categories, $product_categories);
				?>
			</tbody>
		</table>
	</div>
	
</div>

</form>

<?php
function add_image($photo_id, $filename, $alt, $caption, $primary=false, $wanted="")
{	
	ob_start();
	?>
	<!-- <script type="text/javascript" src="<?php echo base_url('js/base64.js');?>"></script> -->
	<!-- <script type="text/javascript" src="<?php echo base_url('js/function.js');?>"></script> -->
	<div class="gc_photo" id="gc_photo_<?php echo $photo_id;?>">
		<table cellspacing="0" cellpadding="0">
			<tr>
				<td style="padding-right:10px;vertical-align:top;" rowspan="2">
					<input type="checkbox" name="images[<?php echo $photo_id;?>][wanted]" value="1" <?php if($wanted) echo 'checked="checked"';?>/>
				</td>
				<td style="width:81px;padding-right:10px;" rowspan="2">
					<input type="hidden" name="images[<?php echo $photo_id;?>][filename]" value="<?php echo $filename;?>"/>
					<!-- <img class="gc_thumbnail" src="<?php echo $filename;?>" width="300"/> -->
					<img class="gc_thumbnail" src="<?php echo base_url(); ?>phpthumb/phpThumb.php?src=<?php echo $filename;?>&w=300&far=1"/>
					
				</td>
				<td style="vertical-align:top">
					<!-- <input type="radio" name="primary_image" value="<?php echo $photo_id;?>" <?php if($primary) echo 'checked="checked"';?>/> <?php echo lang('primary');?> -->
					
					<a onclick="return remove_image($(this));" rel="<?php echo $photo_id;?>" class="button" style="float:left; font-size:9px;">Remove Image</a><br /><br />
					
					<?php if (!file_exists($_SERVER['DOCUMENT_ROOT']."/malppy/uploads/wysiwyg/".$_COOKIE['archiv_num_iid']."/".$photo_id.".jpg")) {?>
					<a onclick="return download_image($(this));" rel="<?php echo $photo_id;?>" filename="<?php echo $filename;?>" id="download_<?php echo $photo_id;?>" class="button" style="float:left; font-size:9px;">Download Image</a>
					<?php } else { ?>
					<a onclick="alert('Image already downloaded!');return false;" rel="<?php echo $photo_id;?>" filename="<?php echo $filename;?>" id="download_<?php echo $photo_id;?>" class="button" style="float:left; font-size:9px;">Downloaded</a>
					<?php } ?>
				</td>
			</tr>
			<tr>
				<td>
					<table>
						<tr>
							<td><?php //echo lang('alt_tag');?></td>
							<td><input type="hidden" name="images[<?php echo $photo_id;?>][alt]" value="<?php //echo $alt;?>" class="gc_tf2"/></td>
						</tr>
						<tr>
							<td><?php //echo lang('caption');?></td>
							<td><input type="hidden" name="images[<?php echo $photo_id;?>][caption]" value="<?php //echo $caption;?>" class="gc_tf2"/></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>
	<?php
	$stuff = ob_get_contents();

	ob_end_clean();
	
	echo replace_newline($stuff);
}

function add_option($name, $option_id, $type)
{
	ob_start();
	?>
	<div id="option-<?php echo $option_id;?>">
		<h3><a href="#"><?php echo $type.' > '.$name; ?></a></h3>
		<div style="text-align: left">
			<?php echo lang('option_name');?>
			<span style="float:right">
			
			<a onclick="remove_option(<?php echo $option_id ?>)" class="ui-state-default ui-corner-all" style="float:right;margin-right:30px;"><span class="ui-icon ui-icon-circle-minus"></span></a></span>
			<input class="input gc_tf1" type="text" name="option[<?php echo $option_id;?>][name]" value="<?php echo $name;?>"/>
			<input type="hidden" name="option[<?php echo $option_id;?>][type]" value="<?php echo $type;?>" />
			<input class="checkbox" type="checkbox" name="option[<?php echo $option_id;?>][required]" value="1"/> <?php echo lang('required');?>
			
	
			<button id="add_item_<?php echo $option_id;?>" type="button" rel="<?php echo $type;?>"onclick="add_item($(this).attr(\'rel\'), <?php echo $option_id;?>);"><?php echo lang('add_item');?></button>
		  
			<div class="option_item_form" >
				<ul class="sortable" id="option_items_<?php echo $option_id;?>">
				
				</ul>
			</div>
		</div>
	</div>
	<?php
	$stuff = ob_get_contents();

	ob_end_clean();
	
	echo replace_newline($stuff);
}

//this makes it easy to use the same code for initial generation of the form as well as javascript additions
function replace_newline($string) {
  return (string)str_replace(array("\r", "\r\n", "\n", "\t"), ' ', $string);
}
?>
<script type="text/javascript">
//<![CDATA[

var option_count	= $('#options_accordion>h3').size();
	
var count = <?php echo $count;?>;

//]]>
</script>
<?php include('footer.php'); ?>
