<?php require('header.php'); ?>

<script type="text/javascript">
function view_inventory(id)
{	
	if (id) {
		location.href='<?php echo  site_url($this->config->item('admin_folder').'/orders/inventory/');?>/'+id;
	}	
}

function inventory_popup(id){

	var win = window.open('<?php echo site_url($this->config->item('admin_folder').'/orders/export_popup/'); ?>/'+id, '_blank', 'width=900,height=600,scrollbars=yes,status=yes,resizable=yes,screenx=0,screeny=0');

	win.onunload =onun; 

    function onun() {
        if(win.location != "about:blank") // This is so that the function 
                                          // doesn't do anything when the 
                                          // window is first opened.
        {
        	window.location.hash = '#tabs-2';
        	location.reload();
        	
        	
        }
    }

}


$(function() {
	$( "#tabs" ).tabs();
});


</script>


<h2>Inventory</h2>

<div id="tabs">
	<ul>
		<li><a href="#tabs-1">Live</a></li>
		<li><a href="#tabs-2">Exports</a></li>
		<li><a href="#tabs-3">Completed Exports</a></li>
		<li><a href="#tabs-4">Shipped Exports</a></li>
	</ul>
	<div id="tabs-1">
		<table class="gc_table">
			<thead>
			<tr>
				<th>Product Name</th><th>SKU</th><th>Arrived/Ordered (Quantity)</th>
			</tr>
			</thead>
			<tbody>
			<?php 
			foreach ($order_summary as $batch){ ?>

				<tr>
				<td><?php echo strip_tags ($batch->name);?></td>
				<td><?php echo $batch->sku;?></td>
				<td>
				
				<?php
				if($batch->arrived_quantity) {
					echo $batch->arrived_quantity;
				}
				else {
					echo '0';
				}
				?>
				/<?php echo $batch->total_quantity;?></td>
				</tr>
				
			
			<?php }
			?>
			</tbody>
		</table>
	</div>
	
	<div id="tabs-2">
		<table class="gc_table">
			<thead>
				<tr>
					<th>Export Number</th>
					<th>No. of products</th>
					<th>No. of seller</th>
					<th>Expected Expenses (RMB)</th>
					<th>Actual Expenses (RMB)</th>
					<th>Weight</th>
					<th>Date</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
			
			<?php 
			
			
			foreach($batches as $b): 
			
				if ($b->status =='open') {
					
 			$info = $this->Order_model->get_batch_summary($b->id);
			$product_cost = $this->Order_model->get_batch_expected_cost($b->id);
			$order_cost = $this->Order_model->get_batch_actual_cost($b->id);
			
				?><tr>
					<td><?php echo $b->batch_number;?></td>
					<td><?php echo $info->unique_products;?></td>
					<td><?php echo $info->unique_sellers;?></td>
					<td><?php echo @number_format($product_cost->expected_cost,2);?></td>
					<td><?php echo @number_format($order_cost,2);?></td>
					<td><?php echo @number_format($info->total_weight,2);?></td>
					<td><?php echo date("d/m/Y h:i a",strtotime($b->created_on));?></td>
					
					<td class="button_set">
					
					<a class="ui-button ui-widget ui-state-default ui-button-text-only ui-corner-left ui-corner-right" onclick="inventory_popup(<?php echo $b->id;?>)" href="javascript:void(0);" role="button">
					View Export</a>
					
					
					<!-- echo anchor_popup('admin/orders/export_popup/'.$b->id, 'View Export', $atts); -->
					
					</td>
			<?php }
			endforeach;?>
					
				</tr>
			</tbody>
		</table>
	</div>
	<div id="tabs-3">
		<table class="gc_table">
			<thead>
				<tr>
					<th>Export Number</th>
					<th>No. of products</th>
					<th>No. of seller</th>
					<th>Expected Expenses (RMB)</th>
					<th>Actual Expenses (RMB)</th>
					<th>Weight</th>
					<th>Date</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
			
			<?php 
			
			
			foreach($batches as $b): 
			
				if ($b->status =='complete') {
					
 			$info = $this->Order_model->get_batch_summary($b->id);
			$product_cost = $this->Order_model->get_batch_expected_cost($b->id);
			$order_cost = $this->Order_model->get_batch_actual_cost($b->id);
			
				?><tr>
					<td><?php echo $b->batch_number;?></td>
					<td><?php echo $info->unique_products;?></td>
					<td><?php echo $info->unique_sellers;?></td>
					<td><?php echo @number_format($product_cost->expected_cost,2);?></td>
					<td><?php echo  @number_format($order_cost,2);?></td>
					<td><?php echo $info->total_weight;?></td>
					<td><?php echo date("d/m/Y h:i a",strtotime($b->created_on));?></td>
					
					<td class="button_set">
					
					<?php 
					
					$atts = array(
							'width'      => '900',
							'height'     => '600',
							'scrollbars' => 'yes',
							'status'     => 'yes',
							'resizable'  => 'yes',
							'screenx'    => '0',
							'screeny'    => '0'
					);
					
					echo anchor_popup('admin/orders/export_popup/'.$b->id, 'View Export', $atts);
					
					?>
					
					</td>
			<?php }
			endforeach;?>
					
				</tr>
			</tbody>
		</table>
	</div>
	<div id="tabs-4">
		<table class="gc_table">
			<thead>
				<tr>
					<th>Export Number</th>
					<th>No. of products</th>
					<th>No. of seller</th>
					<th>Expected Expenses (RMB)</th>
					<th>Actual Expenses (RMB)</th>
					<th>Weight</th>
					<th>Date</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
			
			<?php 
			
			
			foreach($batches as $b): 
			
				if ($b->status =='close') {
					
 			$info = $this->Order_model->get_batch_summary($b->id);
			$product_cost = $this->Order_model->get_batch_expected_cost($b->id);
			$order_cost = $this->Order_model->get_batch_actual_cost($b->id);
			
				?><tr>
					<td><?php echo $b->batch_number;?></td>
					<td><?php echo $info->unique_products;?></td>
					<td><?php echo $info->unique_sellers;?></td>
					<td><?php echo @number_format($product_cost->expected_cost,2);?></td>
					<td><?php echo  @number_format($order_cost,2);?></td>
					<td><?php echo $info->total_weight;?></td>
					<td><?php echo date("d/m/Y h:i a",strtotime($b->created_on));?></td>
					
					<td class="button_set">
					
					<?php 
					
					$atts = array(
							'width'      => '900',
							'height'     => '600',
							'scrollbars' => 'yes',
							'status'     => 'yes',
							'resizable'  => 'yes',
							'screenx'    => '0',
							'screeny'    => '0'
					);
					
					echo anchor_popup('admin/orders/export_popup/'.$b->id, 'View Export', $atts);
					
					?>
					
					</td>
			<?php }
			endforeach;?>
					
				</tr>
			</tbody>
		</table>
	</div>
</div>

<div style="clear:both"></div>
<br>

<?php if ($batch_id) { ?>

<center><h1></h1></center>



<div style="height:80px;"></div>

</form>

<script type="text/javascript">
$(document).ready(function(){	
	// set the datepickers individually to specify the alt fields
	$('.received_on').datepicker({dateFormat:'yy-mm-dd', altField: '#start_top_alt', altFormat: 'yy-mm-dd'});

	$('.buy_item').click(function(){
		this_btn = $(this);
		itemid = $(this).attr('rel');
		if ($("#buy_from_"+itemid).val()=="") {
			alert("Please select buy from which supplier.");
		} else {

			$.ajax('<?php echo site_url($this->config->item('admin_folder').'/orders/purchase_status');?>', { data: {itemid:itemid,supplier_type:$("#buy_from_"+itemid).val()},
    			type: "POST",
    			beforeSend: function() {},
    			error: function() {
        			alert("error"); 
    			},
    			success: function(data) {
					if ($("#buy_from_"+itemid).val()!="Original") {
        				$("#remark_button_"+itemid).show();
					}
					this_btn.hide();
					$("#buy_from_"+itemid).parent().append($("#buy_from_"+itemid).val());
					$("#buy_from_"+itemid).hide();
					//$("#bought_on_"+itemid).html($.now());
					$("#bought_on_"+itemid).html('<?php echo date("Y-m-d"); ?>');
    			}
			});
		}
		return false;
	});

	$('.receive_item').click(function(){
		this_btn = $(this);
		itemid = $(this).attr('rel');
		receive_on = $("#receive_on_"+itemid).val();

		if (receive_on=="") {
			alert("Please select received date.");
		} else {
			$.ajax('<?php echo site_url($this->config->item('admin_folder').'/orders/receive_status');?>', { data: {itemid:itemid,receive_on:receive_on},
    			type: "POST",
    			beforeSend: function() {},
    			error: function() {
        			alert("error"); 
    			},
    			success: function(data) {
					this_btn.hide();
					$("#receive_on_"+itemid).parent().append($("#receive_on_"+itemid).val());
					$("#receive_on_"+itemid).hide();
				}
			});
		}
		return false;
	});
});

$('.add_remark').colorbox({
					width: '550px',
					height: '600px',
					iframe: true
				});

</script>

<?php } ?>

<?php include('footer.php'); ?>