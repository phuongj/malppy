<?php include('header.php'); ?>

<script type="text/javascript">
$(function(){
$("#datepicker1").datepicker({dateFormat: 'mm-dd-yy', altField: '#datepicker1_alt', altFormat: 'yy-mm-dd'});
});
</script>

<?php echo form_open($this->config->item('admin_folder').'/finance/form/'.$id); ?>
<div class="button_set">
<input type="submit" value="Save Record"/>
</div>
<div id="gc_tabs">
	<ul>
		<li><a href="#gc_coupon_attributes">Record</a></li>
	</ul>
	
	<div id="gc_coupon_attributes">
		<div class="gc_field2">
		<label>Credit</label>
			<?php
			$data	= array('id'=>'credit', 'name'=>'credit', 'value'=>set_value('credit', $credit), 'class'=>'gc_tf1');
			echo form_input($data);
			?>
		</div>
		
		<div class="gc_field2">
			<label>Debit</label>
			<?php
			$data	= array('id'=>'debit', 'name'=>'debit', 'value'=>set_value('code', $debit), 'class'=>'gc_tf1');
			echo form_input($data);
			?>
		</div>
		
		<div class="gc_field2">
			<label for="remarks">Remarks</label>
			<?php
				$data	= array('name'=>'remarks', 'value'=>set_value('remarks', $remarks), 'class'=>'gc_tf1');
				echo form_input($data);
			?>
		</div>
		<div class="gc_field2">
			<label for="create_date">Date</label>
			<?php
				$data	= array('id'=>'datepicker1', 'value'=>set_value('create_date', reverse_format($create_date)), 'class'=>'gc_tf1');
				echo form_input($data);
			?>
			<input type="hidden" name="create_date" value="<?php echo set_value('create_date', $create_date) ?>" id="datepicker1_alt" />
		</div>
	</div>
</div>
</form>

<script type="text/javascript">
$(document).ready(function(){
	$("#gc_tabs").tabs();
});

</script>


<?php include('footer.php'); ?>
