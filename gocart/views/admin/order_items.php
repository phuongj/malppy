<table class="gc_table" cellspacing="0" cellpadding="0" style="margin:20px;width:96%">
	<thead>
		<tr>
			<th>Product <?php echo lang('name');?></th>
			<th>Original SKU</th>
			<th>Product Code</th>
			<th style="width:70px;">Quantity</th>
			<th style="text-align:center;">Supplier</th>
			<th style="text-align:center;">Weight</th>			
		</tr>
	</thead>
	<tbody>
		<?php foreach($order->contents as $orderkey=>$product):?>
		<?php $prod = $this->Product_model->get_product($product['id']); ?>
		<?php $total_weight += $prod->weight; ?>
		<tr>
			<td><?php echo strip_tags($product['name']);?></td>
			<td><?php echo $prod->num_iid; ?></td>
			<td><?php echo (trim($product['sku']) != '')?$product['sku']:'';?></td>
			<td style="text-align:center;"><?php echo $product['quantity'];?></td>
			<td style="text-align:center;"><?php echo $prod->seller;?></td>
			<td style="text-align:center;"><?php echo $prod->weight;?></td>
		</tr>
		<?php endforeach;?>
	</tbody>
</table>