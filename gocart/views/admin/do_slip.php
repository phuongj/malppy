		<?php
		
		$products = $this->Order_model->get_items($order->id);

		foreach($products as $prod) {
			$ttl_quantity += $prod['quantity'];

			$oprod[$prod['id']]['id'] = $prod['id'];
			$oprod[$prod['id']]['name'] = strip_tags($prod['name']);
			$oprod[$prod['id']]['price'] = $prod['price'];
			$oprod[$prod['id']]['num_iid'] = $this->Product_model->get_num_iid($prod['id']);
			$oprod[$prod['id']]['sku'] = $prod['sku'];
			$oprod[$prod['id']]['total_quantity'] += $prod['quantity'];
			$oprod[$prod['id']]['total_weight'] += $prod['weight'];
				
		}
		$ttl_item = count($oprod);
		$ttl_order = count($orders);
		?>

<div style="width:960px;font-size:12px; font-family:arial, verdana, sans-serif;margin:0 auto;">
	<div><img src="http://projects2.boxedge.com/malppy/images/logo.png" /></div>
		
	<table style="margin-top:20px;border:1px solid #000; width:100%; font-size:13px;" cellpadding="15" cellspacing="0">
		<tr>
			<td style="width:50%; vertical-align:top;">
				
				<fieldset style="border:1px solid #000;">
					<legend style="padding:0 5px;font-size:13px;"><b>Delivery Details</b></legend>
					
					<table border="0" cellpadding="0" cellspacing="0">
                    	<tr>
                        	<td valign="top" style="font-size:13px; font-family:arial, verdana, sans-serif;">Name:</td>
                            <td style="font-size:13px; font-family:arial, verdana, sans-serif;padding-left:10px;">
							<?php echo $order->ship_firstname.' '.$order->ship_lastname; ?>
							</td>
						</tr>
						<tr>
                        	<td style="height:5px;"></td>
                        </tr>
						<tr>
                        	<td valign="top" style="font-size:13px; font-family:arial, verdana, sans-serif;">Delivery Address:</td>
                        	<td style="font-size:13px; font-family:arial, verdana, sans-serif;padding-left:10px;">
							<?php echo $order->bill_address1;?> <br>
							<?php echo (!empty($order->bill_address2))?$order->bill_address2.' ':'';?><br>
							<?php echo $order->bill_city.', '.$order->bill_zone.' '.$order->bill_zip;?> <br>
							<?php echo $order->bill_country;?>
							</td>
						</tr>
						<tr>
                        	<td style="height:5px;"></td>
                        </tr>
						<tr>
                        	<td valign="top" style="font-size:13px; font-family:arial, verdana, sans-serif;">Order ID:</td>
                        	<td style="font-size:13px; font-family:arial, verdana, sans-serif;padding-left:10px;">
							<?php echo $order->order_number; ?>
							</td>
						</tr>
                    </table>
					
				</fieldset>
			</td>
			
			<td style="width:50%; vertical-align:top;" class="packing">

				<table width="100%" border="0" style="border-color:#000; font-size:13px; border-collapse:collapse;" cellpadding="5" cellspacing="0">
					<tr>
                    	<td style="height:10px;"></td>
                    </tr>
					<tr>
						<td align="center" style="font-size:22px; font-family:arial, verdana, sans-serif;"><b>BSB</b></td>
					</tr>
					<tr>
						<td align="center" style="font-size:24px; font-family:arial, verdana, sans-serif;"><b><?php echo $order->order_number; ?></b></td>
					</tr>
				</table>
			</td>			
		</tr>
		</table>

		<div style="border-top:1px dotted #000;margin-top:40px;height:1px;"></div>
		
		<table width="100%" border="0" cellpadding="0" cellspacing="0" style="margin-top:30px;">
        	<tr>
        		<td>
				<fieldset style="border:1px solid #000;">
					<legend style="padding:0 5px;;font-size:13px;"><b>Order Details</b></legend>
					
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
                    	<tr>
                        	<td style="height:5px;"></td>
                        </tr>
						<tr>
                        	<td>
							<table width="100%" border="0" cellpadding="0" cellspacing="0">
                            	<tr>
                            		<td>
									<table width="50%" border="0" cellpadding="0" cellspacing="0">
                                    	<tr>
                                    		<td valign="top" style="font-size:13px; font-family:arial, verdana, sans-serif;">Name:</td>
                                    		<td style="font-size:13px; font-family:arial, verdana, sans-serif;padding-left:10px;">
											<?php echo $order->ship_firstname.' '.$order->ship_lastname; ?>
											</td>
										</tr>
										<tr>
                        					<td style="height:5px;"></td>
                        				</tr>
										<tr>
                                    		<td valign="top" style="font-size:13px; font-family:arial, verdana, sans-serif;">Delivery Address:</td>
                                    		<td style="font-size:13px; font-family:arial, verdana, sans-serif;padding-left:10px;">
											<?php echo $order->bill_address1;?> <br>
											<?php echo (!empty($order->bill_address2))?$order->bill_address2.' ':'';?><br>
											<?php echo $order->bill_city.', '.$order->bill_zone.' '.$order->bill_zip;?> <br>
											<?php echo $order->bill_country;?>
											</td>
										</tr>
										<tr>
                        					<td style="height:5px;"></td>
                        				</tr>
										<tr>
                                    		<td valign="top" style="font-size:13px; font-family:arial, verdana, sans-serif;">Contact:</td>
                                    		<td style="font-size:13px; font-family:arial, verdana, sans-serif;padding-left:10px;">
											<?php echo $order->ship_phone; ?>
											</td>
										</tr>
                                    </table>
									</td>
									<td width="20"></td>
									<td>
									<table width="50%" border="0" cellpadding="0" cellspacing="0">
                                    	<tr>
                        					<td style="height:20px;"></td>
                        				</tr>
										<tr>
                                    		<td valign="top" style="font-size:13px; font-family:arial, verdana, sans-serif;">Order ID:</td>
                                    		<td style="font-size:13px; font-family:arial, verdana, sans-serif;padding-left:10px;">
											<?php echo $order->order_number; ?>
											</td>
										</tr>
										<tr>
                        					<td style="height:5px;"></td>
                        				</tr>
										<tr>
                                    		<td valign="top" style="font-size:13px; font-family:arial, verdana, sans-serif;">Number of Item:</td>
                                    		<td style="font-size:13px; font-family:arial, verdana, sans-serif;padding-left:10px;">
											<?php echo $ttl_item; ?>
											</td>
										</tr>
										<tr>
                        					<td style="height:5px;"></td>
                        				</tr>
										<tr>
                                    		<td valign="top" style="font-size:13px; font-family:arial, verdana, sans-serif;">Total Quantity:</td>
                                    		<td style="font-size:13px; font-family:arial, verdana, sans-serif;padding-left:10px;">
											<?php echo $ttl_quantity; ?>
											</td>
										</tr>
                                    </table>
									</td>
                            	</tr>
                            </table>
							</td>
                        </tr>
                    </table>
				</fieldset>
				</td>
        	</tr>
        </table>
		
		<table border="0" style="width:100%; margin-top:10px; border-color:#000; font-size:13px; border-collapse:collapse;" cellpadding="5" cellspacing="0">
					<tr>
						<td width="100%%"><h3>Product Details<h3></td>
					</tr>
				</table>
	
		<table border="1" style="width:100%; border-color:#000; font-size:11px; border-collapse:collapse;" cellpadding="5" cellspacing="0">
		<thead>
			<tr>
				<th width="5%" class="packing" style="background-color:#eee">
					No				</th>
				<th width="20%" class="packing" style="background-color:#eee">
					Product				</th>
				<th class="packing" align="center" style="background-color:#eee">
					SKU				</th>
				<th class="packing" align="center" style="background-color:#eee">
					Qty				</th>
				<th class="packing" align="center" style="background-color:#eee">
					Sale Price				</th>
				<th class="packing" align="center" style="background-color:#eee">
					Total				</th>
			</tr>
		</thead>
		
		<?php foreach($oprod as $op) { ?>
		<tr>
			<td class="packing" align="center">
				<?php echo ++$index; ?>
			</td>
			<td class="packing">
				<?php echo $op['name']; ?>			
			</td>
			<td class="packing" align="center"><?php echo $op['num_iid']; ?>
				</td>
			<td class="packing" align="center"><?php echo $op['total_quantity']; ?>
				</td>
			<td class="packing" align="center"><?php echo format_currency(round($op['price']),0); ?>
				</td>
			<td class="packing" align="center"><?php echo format_currency(round($op['total_quantity']*$op['price']),0); ?>
				</td>
		</tr>
		<?php } ?>
		
	</table>
	
	<div style="margin-left:10px;margin-top:50px;"><h3>Delivery Order</h3><br>I acknowledge that to receive the above product with good condition on ____________________ (date).</div>

	<div style="margin-left:10px;margin-top:50px;text-align:right"> __________________________________ &nbsp;<div style="margin-right:200px;padding-top:5px;">Name :</div><div style="margin-right:200px;padding-top:5px;">IC No. : </div></div>

	
</div>


<br class="break"/>