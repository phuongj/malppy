<?php require('header.php'); ?>

<div id="breadcrumb">
	<ul>
		<li><a href="<?php echo site_url($this->config->item('admin_folder').'/delivery');?>">Logistic</a></li>
       	<li class="last"><a href="<?php echo site_url($this->config->item('admin_folder').'/delivery/delivery_status');?>">Pending Delivery</a></li>
    </ul>
</div><!-- End of breadcrumb --> 
<br>

<table class="gc_table" cellspacing="0" cellpadding="0">
    <thead>
		<tr>
			<th class="gc_cell_left">Order ID</th>
			<th>Customer ID</th>
			<th>Ordered On</th>
			<th>Purchase Date</th>
			<!-- <th style="text-align:center;">Grouping</th> -->
			<th class="gc_cell_right"></th>
	    </tr>
		<?php echo $pagination; ?>
	</thead>
 	<tfoot>
    <?php echo $pagination?>
	</tfoot>
    <tbody>
	<?php echo (count($orders) < 1)?'<tr><td style="text-align:center;" colspan="8">'.lang('no_orders') .'</td></tr>':''?>
   	<?php foreach($orders as $order): ?>
	<?php
	foreach($this->Order_model->get_items($order->id) AS $order_item) {
		$total_quantity[$order->id] += $order_item[quantity];
		$customer = $this->Customer_model->get_customer($order->customer_id);
		$customer_code = $customer->code;
		if (!$order->customer_id) $customer_code = $order->bill_lastname.', '.$order->bill_firstname;
	}
	?>
		<tr>
			<td><?php echo $order->order_number; ?></td>
			<td><?php echo $customer_code; ?></td>
			<td style="white-space:nowrap"><?php echo $order->ordered_on; ?></td>
			<td style="white-space:nowrap"><?php echo $order->ordered_on; ?></td>
		
			<!-- <td style="text-align:center;">
				<input type="checkbox" value="0" name="delivery[<?php echo $order->order_number; ?>][group]">
			</td> -->
			<td class="gc_cell_right list_buttons">
			<!-- <a href="<?php echo site_url($this->config->item('admin_folder').'/orders/view/'.$order->id);?>"><?php echo lang('form_view')?></a>&nbsp; -->
			<?php if ($order->status=="Pending Delivery") { ?>
			<!-- <a href="<?php echo site_url($this->config->item('admin_folder').'/orders/taobao/'.$order->id);?>">More</a> -->
			<a class="button" id="button_<?php echo $order->id; ?>" onclick="get_order_detail('<?php echo $order->id; ?>')">More</a>
			<?php } ?>
			</td>
		</tr>
		<tr id="more_<?php echo $order->id; ?>" style="display:none">
			<td style="background-color:#eee" colspan="6" id="table_<?php echo $order->id; ?>"></td>
		</tr>
	<?php endforeach; ?>
	</tbody>
</table>

</form>
<script type="text/javascript">
$(document).ready(function(){
	$('#gc_check_all').click(function(){
		if(this.checked)
		{
			$('.gc_check').attr('checked', 'checked');
		}
		else
		{
			 $(".gc_check").removeAttr("checked"); 
		}
	});
	
	// set the datepickers individually to specify the alt fields
	$('#start_top').datepicker({dateFormat:'mm-dd-yy', altField: '#start_top_alt', altFormat: 'yy-mm-dd'});
	$('#start_bottom').datepicker({dateFormat:'mm-dd-yy', altField: '#start_bottom_alt', altFormat: 'yy-mm-dd'});
	$('#end_top').datepicker({dateFormat:'mm-dd-yy', altField: '#end_top_alt', altFormat: 'yy-mm-dd'});
	$('#end_bottom').datepicker({dateFormat:'mm-dd-yy', altField: '#end_bottom_alt', altFormat: 'yy-mm-dd'});
});

function do_search(val)
{
	$('#search_term').val($('#'+val).val());
	$('#start_date').val($('#start_'+val+'_alt').val());
	$('#end_date').val($('#end_'+val+'_alt').val());
	$('#search_form').submit();
}

function do_export(val)
{
	$('#export_search_term').val($('#'+val).val());
	$('#export_start_date').val($('#start_'+val+'_alt').val());
	$('#export_end_date').val($('#end_'+val+'_alt').val());
	$('#export_form').submit();
}

function submit_form()
{
	if($(".gc_check:checked").length > 0)
	{
		if(confirm('<?php echo lang('confirm_order_delete') ?>'))
		{
			$('#delete_form').submit();
		}
	}
	else
	{
		alert('<?php echo lang('error_no_orders_selected') ?>');
	}
}

function edit_status(id)
{
	$('#status_container_'+id).hide();
	$('#edit_status_'+id).show();
}

function save_status(id)
{
	$.post("<?php echo site_url($this->config->item('admin_folder').'/orders/edit_status'); ?>", { id: id, status: $('#status_form_'+id).val()}, function(data){
		$('#status_'+id).html('<span class="'+data+'">'+$('#status_form_'+id).val()+'</span>');
	});
	
	$('#status_container_'+id).show();
	$('#edit_status_'+id).hide();	
}

function get_order_detail(orderid)
{
	if ($('#button_'+orderid).html() == '<span class="ui-button-text">Hide</span>') {
		$('#more_'+orderid).hide();
		$('#button_'+orderid).html('<span class="ui-button-text">More</span>');
	} else {
		/*$.post('<?php echo site_url($this->config->item('admin_folder').'/orders/delivery');?>/'+orderid,{},function(data){
			$('#table_'+orderid).html(data);
			$('#more_'+orderid).show();
			$('#button_'+orderid).html('<span class="ui-button-text">Hide</span>');
			$('.button_set a').button();
		});*/
		$.ajax('<?php echo site_url($this->config->item('admin_folder').'/orders/delivery');?>/'+orderid, { data: {},
    		type: "POST",
    		beforeSend: function() {
        		$('#button_'+orderid).html('<span class="ui-button-text">Please wait...</span>');
    		},
    		error: function() {
        		alert("error"); 
    		},
    		success: function(data) {
        		$('#table_'+orderid).html(data);
				$('#more_'+orderid).show();
				$('#button_'+orderid).html('<span class="ui-button-text">Hide</span>');
				$('.button_set a').button();

				$('.deliver_button').click(function(){
					var itemid = $(this).attr('itemid');
					var deliver_status = 0;
					
					if(!$(this).attr('checked')) {
						deliver_status = 0;
					} else {
						deliver_status = 1;
					}

					$.post('<?php echo site_url($this->config->item('admin_folder').'/orders/deliver');?>/'+itemid,{deliver_status:deliver_status},function(data){
		
					});
				});
    		}
		});
	}
}
</script>


<?php include('footer.php'); ?>