<?php require('header.php'); 
	
	//set "code" for searches
	if(!$code)
	{
		$code = '';
	}
	else
	{
		$code = '/'.$code;
	}
	function sort_url($by, $sort, $sorder, $code, $admin_folder)
	{
		if ($sort == $by)
		{
			if ($sorder == 'asc')
			{
				$sort	= 'desc';
			}
			else
			{
				$sort	= 'asc';
			}
		}
		else
		{
			$sort	= 'asc';
		}
		$return = site_url($admin_folder.'/orders/index/'.$by.'/'.$sort.'/'.$code);
		return $return;
	}
			

	$pagination = '<tr><td class="gc_pagination" colspan="8"><table class="table_nav" style="width:100%" cellpadding="0" cellspacing="0"><tr><td style="width:0px; text-align:left;">';
 	
 	$pagination .= '</td><td style="text-align:center;">';
 	 	
 	$pagination .= $pages;
 	$pagination .= '</td><td style="width:50px; text-align:right;">';
	$pagination .= '</td></tr></table></td></tr>';
	
	
if ($term)
{
	echo '<p id="searched_for"><div style="width:70%;float:left;"><strong>'.sprintf(lang('search_returned'), intval($total)).'</strong></div></p>';	
}
?>

<div id="breadcrumb">
	<ul>
    	<li><a href="<?php echo site_url($this->config->item('admin_folder').'/Orders');?>">Customer Service</a></li>
    	<li class="last"><a href="<?php echo site_url($this->config->item('admin_folder').'/Orders');?>">Search Order</a></li>
    </ul>
</div><!-- End of breadcrumb --> 
<br>

<?php echo $search_result_str; ?>
<br>

<h2>Search Order</h2>

<?php echo form_open($this->config->item('admin_folder').'/customers/search', array('id'=>'search_form')); ?>
<div id="search_form" style="margin-bottom:10px">
	<input class="gc_tf1" type="text" name="term" id="search_term" value=""/>
	<input type="hidden" name="start_date" id="start_date" value=""/>
	<input type="hidden" name="end_date" id="end_date" value=""/>
	<input type="submit" value="Search"/>
</div>
</form>
<?php echo form_open($this->config->item('admin_folder').'/orders/bulk_export', array('id'=>'bulk_export_form')); ?>

<table class="gc_table" cellspacing="0" cellpadding="0">
    <thead>
		<tr>
			<th class="gc_cell_left"><a href="<?php echo sort_url('order_number', $sort_by, $sortorder, $code, $this->config->item('admin_folder')); ?>"><?php echo lang('order')?> Number</a> <?php if ($sort_by=="order_number" && $sortorder=="desc") { ?><img src="<?php echo base_url('images/sortdown.png');?>" border="0"><?php } ?><?php if ($sort_by=="order_number" && $sortorder=="asc") { ?><img src="<?php echo base_url('images/sortup.png');?>" border="0"><?php } ?></th>
			<th><a href="<?php echo sort_url('ordered_on', $sort_by, $sortorder, $code, $this->config->item('admin_folder')); ?>">Ordered on</a> <?php if ($sort_by=="ordered_on" && $sortorder=="desc") { ?><img src="<?php echo base_url('images/sortdown.png');?>" border="0"><?php } ?><?php if ($sort_by=="ordered_on" && $sortorder=="asc") { ?><img src="<?php echo base_url('images/sortup.png');?>" border="0"><?php } ?></th>
			<th style="text-align:center;">Products</th>
			<th style="text-align:center;">Quantity</th>
			<th>Sales</th>
			<th>Cost</th>
			<th class="gc_cell_right" style="text-align:center;">detail</th>
	    </tr>
		
	</thead>
 	<tfoot>
    <?php //echo $pagination?>
	</tfoot>
    <tbody>
	<?php echo (count($orders) < 1)?'<tr><td style="text-align:center;" colspan="8">'.lang('no_orders') .'</td></tr>':''?>
    <?php foreach($orders as $order): ?>
	<?php
	foreach($this->Order_model->get_items($order->id) AS $order_item) {
		$total_quantity[$order->id] += $order_item[quantity];
		
		$total_cost[$order->id] += $order_item['base_price']*$order_item[quantity];

		++$total_product[$order->id];
	}
	?>
	<tr>
		<td><?php echo $order->order_number; ?></td>
		<td style="white-space:nowrap"><?php echo date('m/d/y h:i a', strtotime($order->ordered_on)); ?></td>
		<td style="text-align:center;"><?php echo $total_product[$order->id]; ?></td>	
		<td style="text-align:center;"><?php echo $total_quantity[$order->id]; ?></td>
		<td>$<?php echo $order->total; ?></td>
		<td>$<?php echo $total_cost[$order->id]; ?></td>			
		<td class="gc_cell_right list_buttons" style="text-align:center;">
			<!-- <a href="<?php echo site_url($this->config->item('admin_folder').'/orders/view/'.$order->id);?>"><?php echo lang('form_view')?></a>&nbsp; -->
			<?php //if ($order->status!="Pending Delivery") { ?>
			<!-- <a href="<?php echo site_url($this->config->item('admin_folder').'/orders/taobao/'.$order->id);?>">More</a> -->
			<a class="button" id="button_<?php echo $order->id; ?>" onclick="get_order_detail('<?php echo $order->id; ?>')">Expand</a>
			<?php //} ?>
		</td>
	</tr>
	<tr id="more_<?php echo $order->id; ?>" style="display:none">
		<td style="background-color:#eee" colspan="8" id="table_<?php echo $order->id; ?>"></td>
	</tr>
    <?php endforeach; ?>
    </tbody>
</table>
</form>

<script type="text/javascript">
$(document).ready(function(){
	
	$('#gc_check_all').click(function(){
		if(this.checked)
		{
			$('.gc_check').attr('checked', 'checked');
		}
		else
		{
			 $(".gc_check").removeAttr("checked"); 
		}
	});
	
	// set the datepickers individually to specify the alt fields
	$('#start_top').datepicker({dateFormat:'mm-dd-yy', altField: '#start_top_alt', altFormat: 'yy-mm-dd'});
	$('#start_bottom').datepicker({dateFormat:'mm-dd-yy', altField: '#start_bottom_alt', altFormat: 'yy-mm-dd'});
	$('#end_top').datepicker({dateFormat:'mm-dd-yy', altField: '#end_top_alt', altFormat: 'yy-mm-dd'});
	$('#end_bottom').datepicker({dateFormat:'mm-dd-yy', altField: '#end_bottom_alt', altFormat: 'yy-mm-dd'});
});

function do_search(val)
{
	$('#search_term').val($('#'+val).val());
	$('#start_date').val($('#start_'+val+'_alt').val());
	$('#end_date').val($('#end_'+val+'_alt').val());
	$('#search_form').submit();
}

function do_export(val)
{
	$('#export_search_term').val($('#'+val).val());
	$('#export_start_date').val($('#start_'+val+'_alt').val());
	$('#export_end_date').val($('#end_'+val+'_alt').val());
	$('#export_form').submit();
}

function submit_form()
{
	if($(".gc_check:checked").length > 0)
	{
		if(confirm('<?php echo lang('confirm_order_delete') ?>'))
		{
			$('#delete_form').submit();
		}
	}
	else
	{
		alert('<?php echo lang('error_no_orders_selected') ?>');
	}
}
</script>

<script type="text/javascript">
function get_order_detail(orderid)
{
	if ($('#button_'+orderid).html() == '<span class="ui-button-text">Hide</span>') {
		$('#more_'+orderid).hide();
		$('#button_'+orderid).html('<span class="ui-button-text">Expand</span>');
	} else {
		$.ajax('<?php echo site_url($this->config->item('admin_folder').'/customers/items');?>/'+orderid, { data: {},
    		type: "POST",
    		beforeSend: function() {
        		$('#button_'+orderid).html('<span class="ui-button-text">Please wait...</span>');
    		},
    		error: function() {
        		alert("error"); 
    		},
    		success: function(data) {
        		$('#table_'+orderid).html(data);
				$('#more_'+orderid).show();
				$('#button_'+orderid).html('<span class="ui-button-text">Hide</span>');
				$('.button_set a').button();

				$('.paid_button').click(function(){
					var itemid = $(this).attr('itemid');
					var paid_status = 0;
					
					if(!$(this).attr('checked')) {
						$('#buy_'+itemid).show();
						paid_status = 0;
					} else {
						if (areyousure()) {
							$('#buy_'+itemid).hide();
							paid_status = 1;
						} else {
							$(this).removeAttr('checked'); 
							paid_status = 0;
						}
					}

					$.post('<?php echo site_url($this->config->item('admin_folder').'/orders/paid');?>/'+itemid,{paid_status:paid_status},function(data){
		
					});
				});

    		}
		});
	}
}
</script>
<?php include('footer.php'); ?>