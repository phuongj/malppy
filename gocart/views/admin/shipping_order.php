<?php echo form_open($this->config->item('admin_folder').'/logistic/new_shipment', array('id'=>'ship_form'));?>
<table class="gc_table" cellspacing="0" cellpadding="0" style="margin:10px;width:50%">
	<thead>
		<tr>
			<th>Order Number</th>
			<th>Weight</th>		
			<?php if ($_REQUEST['new']) { ?>
			<th>Select?</th>
			<?php } ?>
		</tr>
	</thead>
	<tbody>
		<input type="hidden" name="ship[shipno]" value="<?php echo $_REQUEST['shipno']; ?>" />
		<input type="hidden" name="ship[shipped_on]" value="<?php echo $_REQUEST['shipped_on']; ?>" />
		<?php foreach($orders as $order): ?>
		 <tr>
			<td>
				<?php echo $order->order_number;?>
			</td>
			<td>
				<?php echo $order->weight;?>
			</td>
			<?php if ($_REQUEST['new']) { ?>
			<td style="text-align:left;">
				<?php
					$data	= array('name'=>'ship[orders]['.$order->order_number.']', 'value'=>$order->id, 'checked'=>set_checkbox('ship[orders]['.$order->order_number.']', $order->id));
					echo form_checkbox($data);
				?>
			</td>
			<?php } ?>
		</tr> 
		<?php endforeach;?>
		
		
	</tbody>
</table>

<?php if ($_REQUEST['new']) { ?>
<div class="button_set" style="margin-left:405px;margin-top:0px;float:left;">
	<a href="#" onclick="$('#ship_form').submit(); return false;">Confirm Grouping</a>
</div>
<?php } ?>