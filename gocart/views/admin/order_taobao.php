<?php require_once 'taobao.function.php'; ?>
<?php
$atts = array(
			'width'      => '1024',
			'height'     => '768',
			'scrollbars' => 'yes',
			'status'     => 'yes',
			'resizable'  => 'yes',
			'screenx'    => '0',
			'screeny'    => '0'
		);
?>

<table class="gc_table" cellspacing="0" cellpadding="0" style="margin:20px;width:96%">
	<thead>
		<tr>
			<th colspan="2">Product <?php echo lang('name');?></th>
			<th>Original SKU</th>
			<th>Product Code</th>
			<!-- <th>Seller Name</th> -->
			<!-- <th>Cost <?php echo lang('price');?> (BND)</th>
			<th>Cost <?php echo lang('price');?> (RMB)</th> -->
			<th style="width:70px;"><?php echo lang('quantity');?></th>
			<!-- <th>Stock</th> -->
			<th>Seller</th>
			<th>Est. Weight</th>
			<!-- <th>Paid?</th> -->
			<!-- <th style="width:130px;text-align:left;"></th> -->
		</tr>
	</thead>
	<tbody>
		<?php foreach($order->contents as $orderkey=>$product):?>
		<?php $prod = $this->Product_model->get_product($product['id']); ?>
		<?php $total_weight += $prod->weight; ?>
		<?php
			$buy_link = "";
			if ($prod) {
			$paramArr = array(
					'method' => 'taobao.taobaoke.items.detail.get',   //API name
			     'timestamp' => date('Y-m-d H:i:s'),			
				    'format' => 'xml',  //Return format, this demo supports only XML
		    	   'app_key' => $this->config->item('taobao_appKey'),  //Appkey			
			    		 'v' => '2.0',   //API version number		   
				'sign_method'=> 'md5', //Signature method
					'fields' => 'iid,detail_url,num_iid,title,nick,type,cid,seller_cids,props,input_pids,input_str,desc,pic_url,num,valid_thru,list_time,delist_time,stuff_status,location,price,post_fee,express_fee,ems_fee,has_discount,freight_payer,has_invoice,has_warranty,has_showcase,modified,increment,auto_repost,approve,status,postage_id,product_id,auction_point,property_alias,item_imgs,prop_imgs,skus,outer_id,is_virtual,is_taobao,is_ex,videos,is_3D,score,volume,one_station,click_url,shop_click_url,seller_credit_score,approve_status', //Returns the field
			      'num_iids' => $prod->num_iid, //num_iid
				      'nick' => $this->config->item('taobao_userNick'), //Promoter Nick
			);
	
			//Generating signatures
			$sign = createSign($paramArr,$this->config->item('taobao_appSecret'));
	
			//Organizational parameters
			$strParam = createStrParam($paramArr);
			$strParam .= 'sign='.$sign;
	
			//Construct Url
			$urls = $this->config->item('taobao_url').$strParam;
	
			//Connection timeout auto retry
			$cnt=0;	
			while($cnt < 3 && ($result=vita_get_url_content($urls))===FALSE) $cnt++;
	
			//Parsing Xml data
			$result = getXmlData($result);
	
			//Return result
			$taobaokeItemdetail = $result['taobaoke_item_details']['taobaoke_item_detail']['item'];
			$taobaokeItem = $result['taobaoke_item_details']['taobaoke_item_detail'];
			
			$buy_link = mysql_real_escape_string(trim($taobaokeItem['click_url']));
			}
		?>
		<tr>
			<td>
			<img src="<?php echo base_url(); ?>phpthumb/phpThumb.php?src=<?php echo $taobaokeItemdetail[pic_url];?>&w=50&h=50&zc=1" alt="" width="50" height="50" />
			</td>
			<td>
				<?php echo strip_tags($product['name']);?>
				<?php //echo $product['excerpt'];?>
				<?php
				
				// Print options
				if(isset($product['options']))
				{
					foreach($product['options'] as $name=>$value)
					{
						$name = explode('-', $name);
						$name = trim($name[0]);
						if(is_array($value))
						{
							echo '<div>'.$name.':<br/>';
							foreach($value as $item)
							{
								echo '- '.$item.'<br/>';
							}	
							echo "</div>";
						}
						else
						{
							echo '<div>'.$name.': '.$value.'</div>';
						}
					}
				}
				
				if(isset($product['gc_status'])) echo $product['gc_status'];
				?>
			</td>
			<td>
				<?php echo $prod->num_iid;?>
			</td>
			<td>
				<?php echo (trim($product['sku']) != '')?$product['sku']:'';?>
			</td>			
			<!-- <td>
				<?php echo $prod->seller;?>				
			</td> -->
			<!-- <td><?php echo @number_format($prod->price/$bnd_to_rmb,2);?></td>
			<td><?php echo @number_format($prod->price,2);?></td> -->
			<td style="text-align:center;"><?php echo $product['quantity'];?></td>
			<!-- <td style="text-align:center;"><?php if ($prod->quantity>100 && $buy_link) { echo "Yes"; } else { echo "No"; } ?></td> -->
			<td><?php echo $prod->seller;?></td>
			<td style="text-align:center;"><?php echo $prod->weight;?></td>
			<!-- <td>
				<?php
					$data	= array('name'=>'action['.$prod->id.'][paid]', 'class'=>'paid_button', 'itemid'=>$product['itemid'], 'value'=>1, 'checked'=>set_checkbox('action['.$prod->id.'][paid]', 1, (bool) $product['bought']));
					echo form_checkbox($data);
				?>
			</td> -->
			<!-- <td style="text-align:left; width: 160px;">
			
			<script>
    			TOP.ui("sku", {
			      container:".top-widget-sku_<?php echo $product['itemid']; ?>",
			      text:"Buy",
			      itemId:"<?php echo $prod->num_iid;?>",
			      type:"",
			      callback:{
			         addCartSuccess:function(e){console && console.log(e);},
			         error:function(e){console && console.log(e);}
			    }
			    });
			</script>
			<div id="buy_<?php echo $product['itemid']; ?>" class="button_set" nowrap class="gc_tf1" style="text-align:left;white-space:nowrap;<?php if ($product['bought']) { ?>display:none;<?php } ?>">
				<?php if ($buy_link) { ?>
				<div class="top-widget-sku_<?php echo $product['itemid']; ?>" style="float:left;margin-right:5px"></div>
				<?php } ?>

				<?php if ($buy_link) { 
					echo anchor_popup($buy_link, 'Buy', $atts); ?>
				&nbsp;&nbsp;&nbsp;<br><select class="gc_tf1" style="margin-top:5px;">
					<option value="<?php echo $prod->seller;?>	"><?php echo $prod->seller;?>	</option>
				</select>
				<?php } ?>

			</div>
			
			</td> -->
		</tr>
		<?php endforeach;?>
		<tr>
			<td colspan="6">Total Weight</td>
			<td style="text-align:center"><?php echo $total_weight; ?></td>
		</tr>
	</tbody>
</table>

<div class="button_set" style="margin-right:35px;">
	<a href="#" onclick="parent.get_order_detail(<?php echo $orderid; ?>); return false;">Not Pack</a>
	<a href="#" onclick="$('#bulk_form').submit(); return false;">Pack</a>
</div>