<?php require('header.php'); ?>

<div id="breadcrumb">
	<ul>
		<li><a href="<?php echo site_url($this->config->item('admin_folder').'/logistic');?>">Logistic</a></li>
       	<li class="last"><a href="<?php echo site_url($this->config->item('admin_folder').'/logistic/grouping');?>">Grouping</a></li>
    </ul>
</div><!-- End of breadcrumb --> 
<br>

<!-- <div class="button_set">
	<a href="<?php echo site_url($this->config->item('admin_folder').'/logistic/shipment'); ?>">Create New Shipment</a>
</div> -->

<?php echo form_open($this->config->item('admin_folder').'/logistic/bulk_save', array('id'=>'bulk_form'));?>

<table class="gc_table" cellspacing="0" cellpadding="0">
    <thead>
		<tr>
			<th class="gc_cell_left">Shipping Number</th>
			<th>No. of Order ID</th>
			<th>Total Weight (kg)</th>
			<th>Shipped out on</th>
			<th class="gc_cell_right"></th>
	    </tr>
		<?php echo $pagination; ?>
	</thead>
 	<tfoot>
    <?php echo $pagination?>
	</tfoot>
    <tbody>
		<!-- <tr>
			<td>UPS 1123</td>
			<td>5</td>
			<td style="white-space:nowrap">2.2</td>
			<td style="white-space:nowrap">22/10/2012</td>
			<td class="gc_cell_right list_buttons">
					<a id="button_33" href="#" onclick="return get_order_list(33,'');">Expand</a>
			</td>
		</tr>
		<tr id="more_33" style="display:none">
			<td style="background-color:#eee" colspan="6" id="table_33"></td>
		</tr> -->
		<!-- New shipment -->
		<tr>
			<td><?php echo form_input(array('name'=>'shipment[shipid]', 'id'=>'shipno', 'value'=>'', 'class'=>'gc_tf2'));?></td>
			<td>-</td>
			<td>-</td>
			<td><?php echo form_input(array('name'=>'shipment[shipped_on]', 'id'=>'shipped_on', 'value'=>'', 'class'=>'gc_tf2'));?></td>
			<td class="gc_cell_right list_buttons">
				<a id="button_0" href="#" onclick=" get_new_order_list(0);">Group Order</a>
			</td>
		</tr>
		
		<tr id="more_0" style="display:none">
			<td style="background-color:#eee" colspan="6" id="table_0"></td>
		</tr>
		<?php echo (count($shipments) < 1)?'<tr><td style="text-align:center;" colspan="8">No shipment found.</td></tr>':''?>
 	   	<?php foreach($shipments as $shipment): ?>
		<tr>
			<td><?php echo $shipment->shipment_number; ?></td>
			<td><?php echo $shipment->total_order; ?></td>
			<td><?php echo $shipment->total_weight; ?></td>
			<td><?php echo $shipment->shipped_on; ?></td>
			<td class="gc_cell_right list_buttons">
				<a id="button_<?php echo $shipment->id; ?>" href="#" onclick="return get_order_list('<?php echo $shipment->id; ?>');">Expand</a>
				<a href="<?php echo  site_url($this->config->item('admin_folder').'/logistic/delete/'.$shipment->id);?>">Delete</a>
			</td>
		</tr>
		<tr id="more_<?php echo $shipment->id; ?>" style="display:none">
			<td style="background-color:#eee" colspan="6" id="table_<?php echo $shipment->id; ?>"></td>
		</tr>
   		<?php endforeach; ?>

    </tbody>
</table>

</form>
<script type="text/javascript">
$(document).ready(function(){
	
	// set the datepickers individually to specify the alt fields
	$('#shipped_on').datepicker({dateFormat:'mm-dd-yy', altField: '#start_top_alt', altFormat: 'yy-mm-dd'});
});
</script>
<script type="text/javascript">
function get_order_list(orderid)
{
	if ($('#button_'+orderid).html() == '<span class="ui-button-text">Hide</span>') {
		$('#more_'+orderid).hide();
		$('#button_'+orderid).html('<span class="ui-button-text">Expand</span>');
	} else {
		$.ajax('<?php echo site_url($this->config->item('admin_folder').'/orders/shipping');?>/'+orderid, { data: {orderid:orderid},
    		type: "POST",
    		beforeSend: function() {
        		$('#button_'+orderid).html('<span class="ui-button-text">Please wait...</span>');
    		},
    		error: function() {
        		alert("error"); 
    		},
    		success: function(data) {
        		$('#table_'+orderid).html(data);
				$('#more_'+orderid).show();
				$('#button_'+orderid).html('<span class="ui-button-text">Hide</span>');
				$('.button_set a').button();
    		}
		});
	}
}
function get_new_order_list(orderid)
{
	if ($('#button_'+orderid).html() == '<span class="ui-button-text">Cancel</span>') {
		$('#more_'+orderid).hide();
		$('#button_'+orderid).html('<span class="ui-button-text">Group Order</span>');
	} else {

		if ($('#shipno').val()=="") {
			alert("Please enter shipping number before continue.");
			return;
		}

		$.ajax('<?php echo site_url($this->config->item('admin_folder').'/orders/shipping');?>/'+orderid, { data: {shipno:$('#shipno').val(),shipped_on:$('#shipped_on').val(),new:'yes'},
    		type: "POST",
    		beforeSend: function() {
        		$('#button_'+orderid).html('<span class="ui-button-text">Please wait...</span>');
    		},
    		error: function() {
        		alert("error"); 
    		},
    		success: function(data) {
        		$('#table_'+orderid).html(data);
				$('#more_'+orderid).show();
				$('#button_'+orderid).html('<span class="ui-button-text">Cancel</span>');
				$('.button_set a').button();
    		}
		});
	}
}
</script>
<?php include('footer.php'); ?>