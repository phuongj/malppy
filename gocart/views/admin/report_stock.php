<?php require('header.php'); ?>

<div id="breadcrumb">
	<ul>
    	<li><a href="<?php echo site_url($this->config->item('admin_folder').'/reports');?>">Reports</a></li>
        <li class="last"><a href="<?php echo site_url($this->config->item('admin_folder').'/reports_stock');?>">Stock Reports</a></li>
    </ul>
</div><!-- End of breadcrumb --> 

<h2 style="margin:10px 0px 10px 0px; padding:0px;">Live Products</h2>
<div id="stock_container">
	<table class="gc_table" cellspacing="0" cellpadding="0">
	<thead>
		<tr>
			<?php /*<th>ID</th> uncomment this if you want it*/ ?>
			<th class="gc_cell_left">Product Name</th>
			<th width="10%">Original SKU</th>
			<th width="10%">Product Code</th>
			<th width="15%">Available Quantity</th>
			<th width="15%" class="gc_cell_right">Seller</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach($products as $b):?>
		<tr class="gc_row">
			<?php /*<td style="width:16px;"><?php echo  $customer->id; ?></td>*/?>
			<td><?php echo  strip_tags($b->name); ?></td>
			<td><?php echo  $b->num_iid; ?></td>
			<td><?php echo  $b->sku; ?></td>
			<td><?php echo  $b->quantity; ?></td>
			<td class="gc_cell_right"><?php echo $b->seller; ?></a></td>
		</tr>
		<?php endforeach;?>
	</tbody>
</table>
</div>

<h2 style="margin:50px 0px 10px 0px; padding:0px;">Pending Approval Products</h2>
<div id="stock_container">
	<table class="gc_table" cellspacing="0" cellpadding="0">
	<thead>
		<tr>
			<?php /*<th>ID</th> uncomment this if you want it*/ ?>
			<th class="gc_cell_left">Product Name</th>
			<th width="10%">Original SKU</th>
			<th width="10%">Product Code</th>
			<th width="15%">Available Quantity</th>
			<th width="15%" class="gc_cell_right">Seller</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach($taobao as $b):?>
		<tr class="gc_row">
			<?php /*<td style="width:16px;"><?php echo  $customer->id; ?></td>*/?>
			<td><?php echo  strip_tags($b->product_name); ?></td>
			<td><?php echo  $b->num_iid; ?></td>
			<td><?php echo  $b->sku; ?></td>
			<td><?php echo  $b->quantity; ?></td>
			<td class="gc_cell_right"><?php echo $b->seller; ?></a></td>
		</tr>
		<?php endforeach;?>
	</tbody>
</table>
</div>

<script type="text/javascript">
$(document).ready(function(){
	//$.post('<?php echo site_url($this->config->item('admin_folder').'/taobao/auto_stock_update');?>/',{},function(data){});
});
</script>

<?php include('footer.php'); ?>