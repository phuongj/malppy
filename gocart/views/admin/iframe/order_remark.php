<?php include('header.php');?>
<?php
$item = $this->Order_model->get_item($item_id);
$product	= unserialize($item->contents);

?>

<?php if(isset($finished)) : ?>

<script type="text/javascript"> $(function() { parent.location.reload(); }); </script>

<?php else : ?>

<div style="text-align:left">
<form id="remark_form" action="<?php echo site_url($this->config->item('admin_folder').'/orders/buy_remark/'.$item_id.'/'.$supplier_type);?>" method="post" />
<input type="hidden" name="send" value="true">
<input type="hidden" name="item_id" value="<?php echo $item_id; ?>">
<?php if(!empty($errors)) : ?>
<div id="err_box" class="error">
	<?php echo $errors ?>
</div>
<?php endif; ?>
<table cellspacing="10">
<tr>
	<td align="center">
	<?php 
                $this_product = $this->Product_model->get_product($product['id']);
                $product_actual_cost = $this->Product_model->get_product_price($product['id']);
				$this_product->images	= (array)json_decode($this_product->images);
				$this_product->images	= array_values($this_product->images);
				$primary	= $this_product->images[0];

				$user = $this->Product_model->get_admin($product->adminid);
				
				foreach($this_product->images as $image) { if(isset($image->primary)) $primary	= $image; }
                ?>
				<?php if (strpos($primary->filename, 'http') === 0) { ?>
					<img src="<?php echo base_url(); ?>phpthumb/phpThumb.php?src=<?php echo $primary->filename;?>&w=300&h=300&zc=1" alt=""/>
				<?php } else { ?>
					<img src="<?php echo base_url('uploads/images/thumbnails/'.$primary->filename);?>" alt=""/>
				<?php } ?>
	</td>
</tr>
<tr>
	<th align="left"><?php echo $product['name']; ?></th>
</tr>
<tr>
	<td></td>
</tr>
<tr>
	<th align="left">Bought From</th>
</tr>
<tr>
	<td>
	<?php if($edit_mode) { ?>
	
		<input type="text" name="new_supplier" value="<?php 
			if ($supplier_type == 'Original') {
				echo $this_product->seller.'" size="50" class="gc_tf1" readonly="readonly"';
			}
			else {
				echo $new_supplier.'" size="50" class="gc_tf1"';
			}
		?>" 
		/>
		
	<?php } else { ?>
	<?php echo $new_supplier; ?>
	<?php } ?>
	</td>
</tr>
<tr>
	<td></td>
</tr>
<tr>
	<th align="left">Original Price : RMB <?php echo $product_actual_cost;//echo $product['price']; ?></th>
</tr>
<tr>
	<th align="left">
	At a piece of <?php if($edit_mode) { ?><input type="text" name="new_price" value="<?php 
	
	echo $new_price; 
	
	?>" size="20" class="gc_tf1"/><?php } else { ?> : <?php echo $new_price; ?><?php } ?>
	
	</th>
</tr>
<tr>
	<th align="left">For a total of : <?php echo $product['quantity']; ?> item</th>
</tr>
<tr>
	<th align="left">Bought on <?php if($edit_mode) { ?><input type="text" id="bought_on" name="bought_on" value="<?php echo $bought_on; ?>" size="20" class="gc_tf1"/><?php } else { ?> : <?php echo $bought_on; ?><?php } ?></th>
</tr>
<tr>
	<td></td>
</tr>
<tr>
	<?php if($edit_mode) { ?>
	<td><a onclick="$('#remark_form').trigger('submit');" class="button">Update</a></td>
	<?php } else { ?>
	<td><a href="<?php echo site_url($this->config->item('admin_folder').'/orders/buy_remark/'.$item_id.'/'.$supplier_type.'/edit'); ?>" class="button">Edit</a> &nbsp; <a href="<?php echo site_url($this->config->item('admin_folder').'/orders/buy_remark/'.$item_id.'/'.$supplier_type.'/close'); ?>" class="button">Close</a></td>
	<?php } ?>
</tr>

</table>
</form>
</div>
<?php endif; ?>

<script type="text/javascript">
$(document).ready(function(){	
	// set the datepickers individually to specify the alt fields
	$('#bought_on').datepicker({dateFormat:'yy-mm-dd', altField: '#start_top_alt', altFormat: 'yy-mm-dd'});
});
</script>

<?php include('footer.php');