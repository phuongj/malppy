<?php require('header.php'); ?>
<?php require_once 'taobao.function.php'; ?>
<?php
	//top_appkey=21196018&top_parameters=ZXhwaXJlc19pbj04NjQwMCZpZnJhbWU9MSZyMV9leHBpcmVzX2luPTg2NDAwJnIyX2V4cGlyZXNfaW49ODY0MDAmcmVfZXhwaXJlc19pbj04NjQwMCZyZWZyZXNoX3Rva2VuPTYxMDE0MDEzMjFlMTg1ZmUxNTE5NTYxYTMzMDIzODkwNzMxYjhhYzUwMDZkZjJlMTIxNjc3NjEwJnRzPTEzNTE2OTE4ODExODAmdmlzaXRvcl9pZD0xMjE2Nzc2MTAmdmlzaXRvcl9uaWNrPW5hbmN5X3RiODgmdzFfZXhwaXJlc19pbj04NjQwMCZ3Ml9leHBpcmVzX2luPTE4MDA%3D&top_session=6100b01301d0286e973be0ac57cb3c56a1edf69aa97ec17121677610&agreement=true&agreementsign=21196018-22528197-121677610-09F43A797A493ED97F7688B821E31E45&top_sign=cRY0KtPlZD6%2FRPyiSfIpZQ%3D%3D

	$paramArr = array(
	
			/* API systems-level input parameter Start */
	
		    	'method' => 'taobao.trades.sold.get',  //API name
		     	'timestamp' => date('Y-m-d H:i:s'),			
			    'format' => 'xml',  //Return format, this demo supports only XML
	    	   'app_key' => $this->config->item('taobao_appKey'),  //Appkey			
		    		 'v' => '2.0',   //API version number		   
			'sign_method'=> 'md5', //Signature method		
			//'session'=>'61029236e6963479a3da78d7d4c277512a96ab4ca2766fc121677610',
			'session'=>'6102418fa8f8401776e85c6bb0143487a12e7a86949ba12121677610',

			/* API system-level parameters End */				 
	
			/* Application API-level input parameter Start*/
				'fields' => 'seller_nick, buyer_nick, title, type, created, tid, seller_rate, buyer_rate, status, payment, discount_fee, adjust_fee, post_fee, total_fee, pay_time, end_time, modified, consign_time, buyer_obtain_point_fee, point_fee, real_point_fee, received_payment, pic_path, num_iid, num, price, cod_fee, cod_status, shipping_type, receiver_name, receiver_state, receiver_city, receiver_district, receiver_address, receiver_zip, receiver_mobile, receiver_phone,seller_flag,alipay_id,alipay_no,is_lgtype,is_force_wlb,is_brand_sale,buyer_area,has_buyer_message, credit_card_fee, lg_aging_type, lg_aging, step_trade_status,step_paid_fee,mark_desc', //Returns the field
				'nick' => 'nancy_tb88',
				//'q' => 'iphone',
				//'num_iid' => '19980896019',
				//'page_size' => 100,
				'start_time' => '20120901',
				'end_time' => '20121101',
			/* API-level input parameters End*/
	);
	
	//Generating signatures
	$sign = createSign($paramArr,$this->config->item('taobao_appSecret'));
	
	//Organizational parameters
	$strParam = createStrParam($paramArr);
	$strParam .= 'sign='.$sign;
	
	//Construct Url
	//$urls = $url.$strParam;
	$urls = $this->config->item('taobao_url').$strParam;
	
	//Connection timeout auto retry
	$cnt=0;	
	//while($cnt < 3 && ($result=vita_get_url_content($urls))===FALSE) $cnt++;
	
	//Parsing Xml data
	$result = getXmlData($result);
	
	//echo "<pre>";
    //print_r($result);
    //echo "</pre>";

?>
<div id="breadcrumb">
	<ul>
		<li><a href="<?php echo site_url($this->config->item('admin_folder').'/finance');?>">Finance</a></li>
       	<li class="last"><a href="<?php echo site_url($this->config->item('admin_folder').'/finance');?>">Search Result Page</a></li>
    </ul>
</div><!-- End of breadcrumb --> 
<br>

<div class="button_set">
	<a href="<?php echo site_url($this->config->item('admin_folder').'/finance/form'); ?>">Add Record</a>
</div>

<div id="best_sellers">
	
</div>



<script type="text/javascript">

$(document).ready(function(){
	get_best_sellers();
	$('input:button').button();
	$('#best_sellers_start').datepicker({ dateFormat: 'mm-dd-yy', altField: '#best_sellers_start_alt', altFormat: 'yy-mm-dd' });
	$('#best_sellers_end').datepicker({ dateFormat: 'mm-dd-yy', altField: '#best_sellers_end_alt', altFormat: 'yy-mm-dd' });
});

function get_best_sellers()
{
	$.post('<?php echo site_url($this->config->item('admin_folder').'/finance/account');?>',{start:$('#account_start').val(), end:$('#account_end').val()}, function(data){
		$('#best_sellers').html(data);
	});
}
</script>
<?php include('footer.php'); ?>