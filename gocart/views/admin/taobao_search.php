<?php include('header.php'); ?>
<?php require_once 'taobao.function.php'; ?>
<?php include('page.class.php'); ?>
<?php
if (isset($_GET['submit']) || isset($_GET['page'])) { 
	$num_iid=!(isset($_GET['num_iid']))?'0':intval($_GET['num_iid']);
	$cid = !($_GET['cid'])?'0':intval($_GET['cid']);
	$parent_cid = !(isset($_GET['parent_cid']))?'0':intval($_GET['parent_cid']);	   
	$keyword = $_GET['keyword'];
	$start_price= !($_GET['start_price'])?'0.01':intval($_GET['start_price']);
	$end_price= !($_GET['end_price'])?'99999999':intval($_GET['start_price']);
	$auto_send = !(isset($_GET['auto_send']))?'':$_GET['auto_send'];	   //$_GET['auto_send'];
	$area = $_GET['area'];
	$start_credit= !(isset($_GET['start_credit']))?'1heart':$_GET['start_credit'];
	$end_credit= !(isset($_GET['end_credit']))?'5goldencrown':$_GET['end_credit'];	  
	$sort = $_GET['sort'];
	$guarantee =!(isset($_GET['guarantee']))?'':$_GET['guarantee'];//$_GET['guarantee'];
	$start_commissionRate= !(isset($_GET['start_commissionRate']))?'150':intval($_GET['start_commissionRate']);
	$end_commissionRate= !(isset($_GET['end_commissionRate']))?'5000':intval($_GET['end_commissionRate']);
	$start_commissionNum= !(isset($_GET['start_commissionNum']))?'0':intval($_GET['start_commissionNum']);
	$end_commissionNum= !(isset($_GET['end_commissionNum']))?'99999999':intval($_GET['end_commissionNum']);
	$page = !(isset($_GET['page']))?'1':intval($_GET['page']);
	$page_size = !($_GET['page_size'])?'10':intval($_GET['page_size']);

	$paramArr = array(
	
			/* API systems-level input parameter Start */
	
		    	'method' => 'taobao.taobaoke.items.get',  //API name
		     'timestamp' => date('Y-m-d H:i:s'),			
			 //	'timestamp' => '2012-09-14 09:03:15',
			    'format' => 'xml',  //Return format, this demo supports only XML
	    	   'app_key' => $this->config->item('taobao_appKey'),  //Appkey			
		    		 'v' => '2.0',   //API version number		   
			'sign_method'=> 'md5', //Signature method			
	
			/* API system-level parameters End */				 
	
			/* Application API-level input parameter Start*/
	
		    	'fields' =>  'iid,num_iid,title,nick,pic_url,price,click_url,commission,commission_rate,commission_num,commission_volume,shop_click_url,seller_credit_score,volume,item_location',  //Returns the field
		    	  'nick' => $this->config->item('taobao_userNick'),  //Promote its nickname
	   	       'keyword' => $keyword,  //Query keywords	
			   'num_iid' => $num_iid,  //num_iid 
		    	   'cid' => $cid,  //Item class ID 		   
		   'start_price' => $start_price, //Starting price
		     'end_price' => $end_price, //Highest price
			 'auto_send' => $auto_send, //Auto shipping
			 	  'area' => $area, //Location of goods such as: Hangzhou
		  'start_credit' => $start_credit, //Sellers start credit
		    'end_credit' => $end_credit,  //Sellers the highest credit
		   		  'sort' => $sort, //Sort by
			 'guarantee' => $guarantee, //Query whether eliminating sellers
	'start_commissionRate'=>$start_commissionRate, //Starting Commission rate options
	 'end_commissionRate'=> $end_commissionRate, //High Commission rates
	'start_commissionNum'=> $start_commissionNum, //Start Trojan promotion options
	 'end_commissionNum' => $end_commissionNum, //Highest cumulative amount of promotion
			   'page_no' => $page, //Results pages. 1~99
	  	     'page_size' => $page_size , //Number of results to return per page. maximum per-page 40 
					
			/* API-level input parameters End*/
	);
	if ($num_iid) {
		$paramArr = array(

		/* API systems-level input parameter Start */

		    'method' => 'taobao.taobaoke.items.detail.get',   //API name
	     'timestamp' => date('Y-m-d H:i:s'),			
		    'format' => 'xml',  //Return format, this demo supports only XML
    	   'app_key' => $this->config->item('taobao_appKey'),  //Appkey			
	    		 'v' => '2.0',   //API version number		   
		'sign_method'=> 'md5', //Signature method			

		/* API system-level parameters End */			

		/* Application API-level input parameter Start*/

			'fields' => 'iid,detail_url,num_iid,title,nick,type,cid,seller_cids,props,input_pids,input_str,desc,pic_url,num,valid_thru,list_time,delist_time,stuff_status,location,price,post_fee,express_fee,ems_fee,has_discount,freight_payer,has_invoice,has_warranty,has_showcase,modified,increment,auto_repost,approve,status,postage_id,product_id,auction_point,property_alias,item_imgs,prop_imgs,skus,outer_id,is_virtual,is_taobao,is_ex,videos,is_3D,score,volume,one_station,click_url,shop_click_url,seller_credit_score,approve_status,props,props_name', //Returns the field
	      'num_iids' => $num_iid, //num_iid
		      'nick' => $this->config->item('taobao_userNick'), //Promoter Nick
	
		/* API-level input parameters End*/	
		);
	}

	//echo "<pre>";
	//print_r($paramArr);
	//echo "</pre>";
	//Generating signatures
	$sign = createSign($paramArr,$this->config->item('taobao_appSecret'));
	
	//Organizational parameters
	$strParam = createStrParam($paramArr);
	$strParam .= 'sign='.$sign;
	
	//Construct Url
	//$urls = $url.$strParam;
	$urls = $this->config->item('taobao_url').$strParam;
	
	//Connection timeout auto retry
	$cnt=0;	
	while($cnt < 3 && ($result=vita_get_url_content($urls))===FALSE) $cnt++;
	
	//Parsing Xml data
	$result = getXmlData($result);
	
	//print_r($result);
	
	//Gets the error message
	@$sub_msg = $result['sub_msg'];
	
	//print_r($result);

	//Return result
	@$TaobaokeItem = $result['taobaoke_items']['taobaoke_item'];
	@$TaobaokeCount = $result['total_results'];

	if ($num_iid) {
		$taobaokeItemdetail = $result['taobaoke_item_details']['taobaoke_item_detail']['item'];
		$taobaokeItem = $result['taobaoke_item_details']['taobaoke_item_detail'];
	

		$num_iid			= $taobaokeItemdetail['num_iid'];			//商品iid
	$title				= $taobaokeItemdetail['title'];			//商品标题
	$nick				= $taobaokeItemdetail['nick'];				//卖家昵称
	$catid				= $taobaokeItemdetail['cid'];				//分类cid
	$pic_url			= $taobaokeItemdetail['pic_url'];			//图片地址
	$post_fee			= $taobaokeItemdetail['post_fee'];			//平邮价格
	$express_fee		= $taobaokeItemdetail['express_fee'];		//快递价格
	$ems_fee			= $taobaokeItemdetail['ems_fee'];			//EMS价格
	$num				= $taobaokeItemdetail['num'];				//库存数量
	$price				= $taobaokeItemdetail['price'];			//销售价格
	$list_time			= $taobaokeItemdetail['list_time'];		//上架时间
	$delist_time		= $taobaokeItemdetail['delist_time'];		//下架时间
	$state				= $taobaokeItemdetail['location']['state'];//所在省份
	$city				= $taobaokeItemdetail['location']['city'];	//所在城市
	$props				= $taobaokeItemdetail['props'];			//商品属性
	//$props_name			= $taobaokeItemdetail['props_name'];		//属性名称
	$desc				= $taobaokeItemdetail['desc'];				//商品描述
	
	$click_url			= $taobaokeItem['click_url'];				//商品推广网址
	$shop_click_url		= $taobaokeItem['shop_click_url'];		//店铺推广网址
	$level				= $taobaokeItem['seller_credit_score'];	//卖家信誉
	}
	
	/* Details get Taobao commodity End*/		
	function Newiconv($_input_charset, $_output_charset, $input)
	{
	    $output = "";
	    if (!isset($_output_charset))
	        $_output_charset = $this->parameter['_input_charset '];
	    if ($_input_charset == $_output_charset || $input == null) {
	        $output = $input;
	    } elseif (function_exists("m\x62_\x63\x6fn\x76\145\x72\164_\145\x6e\x63\x6f\x64\x69\x6e\147")) {
	        $output = mb_convert_encoding($input, $_output_charset, $_input_charset);
	    } elseif (function_exists("\x69\x63o\156\x76")) {
	        $output = iconv($_input_charset, $_output_charset, $input);
	    } else
	        die("对不起，你的服务器系统无法进行字符转码.请联系空间商。");
	    return $output;
	}
}
?>

<div id="breadcrumb">
	<ul>
    	<li><a href="<?php echo site_url($this->config->item('admin_folder').'/taobao');?>">Taobao</a></li>
        <li class="last"><a href="#">Search Products</a></li>
    </ul>
</div><!-- End of breadcrumb --> 

<form action="taobao" method="get" name="form1" id="form1">
<table class="gc_table" cellspacing="0" cellpadding="0">
	<thead>
	<tr>
      <th class="gc_cell_left">Category ID</td>
      <th>Keyword</td>   
	  <th>Taobao SKU ID</td>
      <th>Start Price</td>
      <th>End Price</td>
	  <th>Area</td>
	  <th>Sort by</td>
      <th>Page Size</td>      
     
      <!-- <th>Auto Delivery</td>                        
      <th class="gc_cell_right">Customer Guarantee</td>       -->
    </tr>
	</thead>
	<tbody>
	<tr class="gc_row">
      <td>
<select name="cid" style="width:200px; ">
<option value="0" selected="selected">All Categories </option>
<?php include('taobao_category_list.php'); ?>
</select>      
      </td>
      <td><input type="text" name="keyword" id="keyword" value=""></td>      
      <td><input type="text" name="num_iid" id="num_iid" value="" style="width:120px;"></td> 
      <td><input type="text" name="start_price" id="start_price" style="width:60px;"></td>
      <td><input type="text" name="end_price" id="end_price" style="width:60px;"></td>      
	  <td>
      <select name="area">
      <option value="" selected> All Area </option>
      <?php include('taobao_area_list.php'); ?>
      </select>      
      </td>
	  <td>
      <select name="sort">
	  <option value=""></option>
	  <option value="price_asc">(Price Low to High) </option>
      <option value="price_desc">(Price High to Low) </option>      
      
      </select>
      </td>
	   <td>
      <select name="page_size">
      <?php 
	  for($i=10;$i<41;$i++){
	  ?>
      <option value="<?php echo $i?>"><?php echo $i?> </option>
      <? }?>
      </select>      
      </td>                    
    </tr> 
    </tbody>
	<thead>
	<tr>

    </tr>
	</thead>
	<tbody>
 	<tr>      
     
      <td colspan="9"><div class="button_set" style="text-align:center;"><input type="submit" name="submit" value="Search"></div>
      </td>                                
    </tr>    
	</tbody>
</table>
</form>

<p>&nbsp;</p>

<?php echo form_open($this->config->item('admin_folder').'/taobao/bulk_import', array('id'=>'taobao_form')); ?>
<table border="0" width="100%" class="table gc_table">
	<thead>
	<tr>
		<th class="gc_cell_left"><input type="checkbox" id="gc_check_all" /></th>
		<th>Image</th>
		<th>Detail</th>
		<th>Product Name</th>        
		<!-- <th>Taobao SKU ID</th> -->
		<th>Seller</th>
		<th>Price (RMB)</th>
		<th>Price (BND)</th>
		<!-- <th>Link</th>   -->          
        <!-- <th>Volume</th> --> 		
		<th>Location</th>
		<th>Product URL</th>   
		<th class="gc_cell_right">Action</th>		     
	</tr>
	</thead>
	<tbody>
	<?php
	if (empty($sub_msg)){
		if (is_array($TaobaokeItem)){
		foreach ($TaobaokeItem as $key => $val) { 
	?>
	<tr>
		<td><input name="product[]" type="checkbox" value="<?php echo $val['num_iid']; ?>" class="gc_check"/></td>
		<td>
			<img src="<?php echo base_url(); ?>phpthumb/phpThumb.php?src=<?php echo $val['pic_url']; ?>&w=75&h=75&far=1" width="75" height="75" />
		</td>
		<td>
			<!-- <a href=item.php?num_iid=<?php echo $val['num_iid'];?> target="_blank">view</a> -->
			<a href="taobao/detail/<?php echo $val['num_iid'];?>">view</a>
		</td>		
		<td>
            <?php echo $val['title'];?>
		</td>
		<!-- <td>
			<a href="javascript:;" title="<?php echo $val['num_iid'];?>">view</a>
		</td>     -->    
		<td>
			<?php echo $val['nick'];?>
		</td>
		<td>
			<?php echo $val['price'];?>
		</td>
		<td>
			<?php echo @number_format($val['price']/$bnd_to_rmb,2);?>
		</td>
		<!-- <td>
			<a href="<?php echo $val['click_url'];?>" target="_blank">view</a>
		</td> -->
 		<!-- <td>
			<?php echo $val['num'];?>
		</td> -->
 		<td>
			<?php echo $val['item_location'];?>
		</td>                        
		<td>
			<a href="<?php echo $val['click_url'];?>" target="_blank">website</a>
		</td>        
		<td>
			<a href="taobao/bookmark/<?php echo $val['num_iid'];?>">Import</a>
		</td>	
	</tr>
	<?php
	}
	} else if ($num_iid){
	?>
		<tr>
			<td></td>
			<td>
			<img src="<?php echo base_url(); ?>phpthumb/phpThumb.php?src=<?php echo $pic_url; ?>&w=75&h=75&far=1" width="75" height="75" />
			</td>
			<td><a href="taobao/detail/<?php echo $num_iid;?>">view</a></td>
			<td><?php echo $title;?></td>
			<td><?php echo $nick; ?></td>
			<td>
				<?php echo $price;?>
			</td>
			<td>
				<?php echo @number_format($price/$bnd_to_rmb,2);?>
			</td>
			<td><?php echo $city;?><?php echo $state;?></td>
			<td><a href="<?php echo $click_url;?>" target="_blank">website</a></td>
			<td>
			<a href="taobao/bookmark/<?php echo $num_iid;?>">Import</a>
			</td>
		</tr>

	<?php

		}else{
		echo '<tr><td colspan="11" class="submsg" align="center">No record found.</td></tr>';
		}
	?>
	<?php if (is_array($TaobaokeItem)){ ?>
	<tr>
      <td colspan="15"> <?php
	// Paging new PageClass (total data per page number, page number, URL component);
	$pages = new PageClass($TaobaokeCount,$page_size,$page,'taobao?'.$_SERVER['QUERY_STRING'].'&page={page}');
	echo $pages->myde_write();
	?>
      </td>
    </tr>    
	<tr>
			<td colspan="15" class="gc_table_tools">
				<div class="gc_product_import">
					<a onclick="submit_form();" class="button">Batch Import</a>
				</div>
			</td>
		</tr>
	<?php } ?>
    <?
	}else{

	echo '<tr><td colspan=15 class="sub_msg">'.$sub_msg.'</td></tr>';		
	}
?>
	

	</tbody>
</table>
<script type="text/javascript">
$(document).ready(function(){
	$('#gc_check_all').click(function(){
		if(this.checked)
		{
			$('.gc_check').attr('checked', 'checked');
		}
		else
		{
			 $(".gc_check").removeAttr("checked"); 
		}
	});
});

function submit_form()
{
	if($(".gc_check:checked").length > 0)
	{
		if(confirm('Confirm import?'))
		{
			$('#taobao_form').submit();
		}
	}
	else
	{
		alert('No item selected.');
	}
}
</script>

<?php include('footer.php'); ?>