<?php require_once 'taobao.function.php'; ?>
<?php
$atts = array(
            'width'      => '1024',
            'height'     => '768',
            'scrollbars' => 'yes',
            'status'     => 'yes',
            'resizable'  => 'yes',
            'screenx'    => '0',
            'screeny'    => '0'
        );
?>

<?php echo form_open($this->config->item('admin_folder').'/orders/bulk_seller_save', array('id'=>'bulk_form'));?>
<table class="gc_table" cellspacing="0" cellpadding="0" style="margin:20px;width:96%">
	<thead>
		<tr>
			<th colspan="2">Product <?php echo lang('name');?></th>
			<th>Original SKU</th>
			<th>Product Code</th>
			<th>Cost <?php echo lang('price');?> (BND)</th>
			<th>Cost <?php echo lang('price');?> (RMB)</th>
			<th style="width:70px;"><?php echo lang('quantity');?></th>
			<th>Stock</th>
			<th>Est. Weight</th>
			<th>Paid?</th>
			<th style="width:130px;text-align:left;"></th>
		</tr>
	</thead>
	<tbody>
		<?php foreach($products as $product):?>
			<?php
			$buy_link = "";
			if ($product) {
			$paramArr = array(
					'method' => 'taobao.taobaoke.items.detail.get',   //API name
			     'timestamp' => date('Y-m-d H:i:s'),			
				    'format' => 'xml',  //Return format, this demo supports only XML
		    	   'app_key' => $this->config->item('taobao_appKey'),  //Appkey			
			    		 'v' => '2.0',   //API version number		   
				'sign_method'=> 'md5', //Signature method
					'fields' => 'iid,detail_url,num_iid,title,nick,type,cid,seller_cids,props,input_pids,input_str,desc,pic_url,num,valid_thru,list_time,delist_time,stuff_status,location,price,post_fee,express_fee,ems_fee,has_discount,freight_payer,has_invoice,has_warranty,has_showcase,modified,increment,auto_repost,approve,status,postage_id,product_id,auction_point,property_alias,item_imgs,prop_imgs,skus,outer_id,is_virtual,is_taobao,is_ex,videos,is_3D,score,volume,one_station,click_url,shop_click_url,seller_credit_score,approve_status', //Returns the field
			      'num_iids' => $product->num_iid, //num_iid
				      'nick' => $this->config->item('taobao_userNick'), //Promoter Nick
			);
	
			//Generating signatures
			$sign = createSign($paramArr,$this->config->item('taobao_appSecret'));
	
			//Organizational parameters
			$strParam = createStrParam($paramArr);
			$strParam .= 'sign='.$sign;
	
			//Construct Url
			$urls = $this->config->item('taobao_url').$strParam;
	
			//Connection timeout auto retry
			$cnt=0;	
			while($cnt < 3 && ($result=vita_get_url_content($urls))===FALSE) $cnt++;
	
			//Parsing Xml data
			$result = getXmlData($result);
	
			//Return result
			$taobaokeItemdetail = $result['taobaoke_item_details']['taobaoke_item_detail']['item'];
			$taobaokeItem = $result['taobaoke_item_details']['taobaoke_item_detail'];
			
			$buy_link = mysql_real_escape_string(trim($taobaokeItem['click_url']));
			}
			?>
			<tr>
				<td>
				<img src="<?php echo base_url(); ?>phpthumb/phpThumb.php?src=<?php echo $taobaokeItemdetail[pic_url];?>&w=50&h=50&zc=1" alt="" width="50" height="50" />
				</td>
				<td>
				<?php echo strip_tags($product->name);?>
				<?php
				
				// Print options
				if(isset($product->options))
				{
					foreach($product->options as $name=>$value)
					{
						$name = explode('-', $name);
						$name = trim($name[0]);
						if(is_array($value))
						{
							echo '<div>'.$name.':<br/>';
							foreach($value as $item)
							{
								echo '- '.$item.'<br/>';
							}	
							echo "</div>";
						}
						else
						{
							echo '<div>'.$name.': '.$value.'</div>';
						}
					}
				}
				?>
			</td>
				<td>
					<?php echo $product->num_iid;?>
				</td>
				<td>
					<?php echo $product->sku;?>
				</td>
				<td><?php echo @number_format($product->price/$bnd_to_rmb,2);?></td>
			<td><?php echo @number_format($product->price,2);?></td>
			<td style="text-align:center;"><?php echo $product->quantity;?></td>
			<td style="text-align:center;"><?php if ($product->quantity>100 && $buy_link) { echo "Yes"; } else { echo "No"; } ?></td>
			<td style="text-align:center;"><?php echo $product->weight;?></td>
			<td>
				<input type="hidden" name="action[<?php echo $product->id; ?>][product_id]" value="<?php echo $product->id; ?>">
				<?php
					$bought = $this->Order_model->get_bought_status($product->id);
					$data	= array('name'=>'action['.$product->id.'][paid]', 'value'=>1, 'checked'=>set_checkbox('action['.$product->id.'][paid]', 1, (bool) $bought));
					echo form_checkbox($data);
				?>
			</td>
			<td style="text-align:left; width: 160px;">
				<div id="buy_<?php echo $product->id; ?>" class="button_set" nowrap class="gc_tf1">
				<?php if ($buy_link) { 
					echo anchor_popup($buy_link, 'Buy', $atts); ?>
				<?php } ?>
				</div>
			</td>
			</tr>
		<?php endforeach;?>
	</tbody>
</table>
</form>

<div class="button_set" style="margin-right:180px;">
	<a href="#" onclick="$('#bulk_form').submit(); return false;">Update</a>
</div>