<?php

//echo "<pre>";
//print_r($order);
//echo "</pre>";
?>

<table class="gc_table" cellspacing="0" cellpadding="0" style="margin:20px;width:96%">
	<thead>
		<tr>
			<th>Product <?php echo lang('name');?></th>
			<th>Product Code</th>
			<th>Exp No</th>
			<th style="width:70px;">Quantity</th>
			<th>Supplier</th>
			<th>Weight</th>
			<th>Bought</th>
			<th>Arrived</th>
			<th>Ship ID</th>
			<th>Delivered</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach($order->contents as $orderkey=>$product):?>
		<?php $prod = $this->Product_model->get_product($product['id']); ?>
		<?php $batch = $this->Order_model->get_batch($order->batch_id); ?>
		<?php $shipment = $this->Order_model->get_shipment_by_order($order->id); ?>	
		<?php $total_weight += $prod->weight; ?>
		<tr>
			<td><?php echo strip_tags($product['name']);?></td>
			<td><?php echo (trim($product['sku']) != '')?$product['sku']:'';?></td>
			<td><?php echo $batch->batch_number; ?></td>
			<td style="text-align:center;"><?php echo $product['quantity'];?></td>
			<td><?php echo $prod->seller;?></td>
			<td style="text-align:center;"><?php echo $prod->weight;?></td>
			<td><?php if($product['bought_on']) { echo date("d-m-Y",strtotime($product['bought_on'])); } ?></td>
			<td><?php if($product['arrive_on']) { echo date("d-m-Y",strtotime($product['arrive_on'])); } ?></td>
			<td><?php if ($shipment) { echo $shipment->shipment_number; } ?></td>
			<td><?php if ($order->is_delivered) { echo "Yes"; } ?></td>
		</tr>
		<?php endforeach;?>
	</tbody>
</table>