<?php require('header.php'); 
	
	//set "code" for searches
	if(!$code)
	{
		$code = '';
	}
	else
	{
		$code = '/'.$code;
	}
	
if ($term)
{

	foreach($orders as $order) {
		$products = $this->Order_model->get_items($order->id);

		foreach($products as $prod) {
			$product_count++;
		}
	}

	$search_result_str = '<p id="searched_for"><div style="width:70%;float:left;"><strong>'.sprintf(lang('search_returned'), intval($product_count)).'</strong></div></p>';
	
}
?>

<div id="breadcrumb">
	<ul>
		<li><a href="<?php echo site_url($this->config->item('admin_folder').'/delivery');?>">Pending Delivery</a></li>
       	<li class="last"><a href="<?php echo site_url($this->config->item('admin_folder').'/delivery/search');?>">Search Result Page</a></li>
    </ul>
</div><!-- End of breadcrumb --> 
<br>

<!-- <?php echo form_open($this->config->item('admin_folder').'/delivery/search', array('id'=>'search_form')); ?>
<div id="search_form" style="margin-bottom:10px">
	<input class="gc_tf1" type="text" name="term" id="term" />
	<input type="hidden" name="search_order" /> 
	<input type="submit" value="Search"/>
</div>
</form> -->
<?php echo form_open($this->config->item('admin_folder').'/delivery/search', array('id'=>'search_form')); ?>
<div id="search_form" style="margin-bottom:10px">
	<label style="width:auto">Enter Order Number </label>
	<input class="gc_tf1" type="text" name="term" id="search_term" value=""/>
	<input type="hidden" name="start_date" id="start_date" value=""/>
	<input type="hidden" name="end_date" id="end_date" value=""/>
	<input type="submit" value="Search"/>
</div>
</form>

<?php echo $search_result_str; ?>
<br><br>

<table class="gc_table" cellspacing="0" cellpadding="0">
    <thead>
		<tr>
			<th class="gc_cell_left">Product Name</th>
			<th>Customer Name</th>
			<th>Order ID</th>
			<th>Original SKU</th>
			<th>Product Code</th>
			<th>Quantity Bought</th>
			<th>Weight</th>
			<th>Date of Purchase</th>
			<th class="gc_cell_right"></th>
	    </tr>
	</thead>
    <tbody>
		
	<?php echo (count($orders) < 1)?'<tr><td style="text-align:center;" colspan="9">No order found.</td></tr>':''?>
    <?php foreach($orders as $order): ?>
	<?php
		$products = $this->Order_model->get_items($order->id);

		foreach($products as $prod):
	?>
	<tr>
		<td><?php echo strip_tags($prod['name']); ?></td>
		<td style="white-space:nowrap"><?php echo $order->bill_lastname.' '.$order->bill_firstname; ?></td>
		<td><a href="<?php echo site_url($this->config->item('admin_folder').'/orders/view/'.$order->id);?>"><?php echo $order->order_number; ?></a></td>
		
		<td><?php echo $this->Product_model->get_num_iid($prod['id']); ?></td>
		<td><?php echo $prod['sku']; ?></td>
		<td style="text-align:center;"><?php echo $prod['quantity']; ?></td>
		<td style="text-align:center;"><?php echo $prod['weight']; ?></td>
		<td style="white-space:nowrap"><?php echo date('m/d/y h:i a', strtotime($order->ordered_on)); ?></td>
		<td class="gc_cell_right list_buttons">
			<!-- <a href="<?php echo site_url($this->config->item('admin_folder').'/orders/view/33');?>">Button</a>&nbsp; -->
		</td>
	</tr>
    <?php endforeach; ?>
	<?php endforeach; ?>
    </tbody>
</table>

</form>
<script type="text/javascript">
function do_search(val)
{
	$('#search_term').val($('#'+val).val());
	$('#start_date').val($('#start_'+val+'_alt').val());
	$('#end_date').val($('#end_'+val+'_alt').val());
	$('#search_form').submit();
}

function do_export(val)
{
	$('#export_search_term').val($('#'+val).val());
	$('#export_start_date').val($('#start_'+val+'_alt').val());
	$('#export_end_date').val($('#end_'+val+'_alt').val());
	$('#export_form').submit();
}

function submit_form()
{
	if($(".gc_check:checked").length > 0)
	{
		if(confirm('<?php echo lang('confirm_order_delete') ?>'))
		{
			$('#delete_form').submit();
		}
	}
	else
	{
		alert('<?php echo lang('error_no_orders_selected') ?>');
	}
}

function edit_status(id)
{
	$('#status_container_'+id).hide();
	$('#edit_status_'+id).show();
}

function save_status(id)
{
	$.post("<?php echo site_url($this->config->item('admin_folder').'/orders/edit_status'); ?>", { id: id, status: $('#status_form_'+id).val()}, function(data){
		$('#status_'+id).html('<span class="'+data+'">'+$('#status_form_'+id).val()+'</span>');
	});
	
	$('#status_container_'+id).show();
	$('#edit_status_'+id).hide();	
}
</script>


<?php include('footer.php'); ?>