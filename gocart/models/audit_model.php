<?php

Class Audit_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}
	
	function log_action($adminid, $action, $table_name="", $row_id="")
	{
		$this->db->insert('audit_trail', array('adminid'=>$adminid, 'action'=>$action, 'table_name'=>$table_name, 'row_id'=>$row_id));
	}
}
?>
