<?php
Class Product_model extends CI_Model
{
		
	// we will store the group discount formula here
	// and apply it to product prices as they are fetched 
	var $group_discount_formula = false;
	
	function __construct()
	{
		parent::__construct();
		
		// check for possible group discount 
		$customer = $this->session->userdata('customer');
		if(isset($customer['group_discount_formula'])) 
		{
			$this->group_discount_formula = $customer['group_discount_formula'];
		}
	}

	function total_products() 
	{
			return $this->db->select('product_id')->from('products')->where('enabled = 1 OR enabled = 0')->count_all_results();
	}

	function total_pending_approval() 
	{
			return $this->db->select('num_iid')->where("is_saved = 0")->from('taobao')->count_all_results();
	}

	function total_saved_list() 
	{
			return $this->db->select('num_iid')->where("is_saved = 1")->from('taobao')->count_all_results();
	}

	function total_suspense() 
	{
			return $this->db->select('product_id')->from('products')->where(array('enabled'=>2))->count_all_results();
	}

	function total_promo() 
	{
			return $this->db->select('product_id')->from('products')->where('enabled = 1 AND promoprice != "0.00"')->count_all_results();
	}

	function get_num_iid($id)
	{
		$result = $this->db->select('num_iid')->where('id', $id)->get('products')->row();
		return $result->num_iid;
	}

	function get_admin($id)
	{
		$result = $this->db->select('lastname')->where('id', $id)->get('admin')->row();
		return $result->lastname;
	}
	
	function get_new_arrivals(){
		
		$result = $this->db->select('*')->from('category_latest_products')->get()->result();
		return $result;
		
	}
	
	function update_new_arrivals () {
		$this->db->truncate('category_latest_products');
		
		$array = array("WM","MN","BB");
		
			foreach ($array as $category_code) {
				$this->db->query('INSERT INTO mp_category_latest_products(name, slug, excerpt, price, saleprice, promoprice, images, product_image, seo_title, created) SELECT name, slug, excerpt, price, saleprice, promoprice, images, product_image, seo_title, created FROM mp_products WHERE sku LIKE "'.$category_code.'%" AND enabled = true ORDER BY created DESC LIMIT 5');
			}
		
	}
	
	function get_latest_products($category_id = false, $limit = false, $offset = false)
	{
		//if we are provided a category_id, then get products according to category
		if ($category_id)
		{
			$result = $this->db->select('category_products.*')->from('category_products')->join('products', 'category_products.product_id=products.id')->where(array('category_id'=>$category_id, 'enabled'=>1, 'new'=>1))->order_by('id', 'DESC')->limit($limit)->offset($offset)->get()->result();
			
			//$this->db->order_by('id', 'DESC');
			//	$result	= $this->db->get_where('category_products', array('enabled'=>1), $limit, $offset);
			//$result	= $result->result();

			$contents	= array();
			$count		= 0;
			foreach ($result as $product)
			{

				$contents[$count]	= $this->get_product($product->product_id);
				$count++;
			}

			return $contents;
		}
		else
		{
			//sort by alphabetically by default
			$this->db->order_by('id', 'DESC');
			$this->db->where(array('enabled'=>1, 'new'=>1));
			$result	= $this->db->get('products');
			//apply group discount
			$return = $result->result();
			if($this->group_discount_formula) 
			{
				foreach($return as &$product) {
					eval('$product->price=$product->price'.$this->group_discount_formula.';');
				}
			}
			return $return;
		}

	}

	function get_products($category_id = false, $limit = false, $offset = false, $search=false, $sort_by='id', $sort_order='DESC', $price_start=false, $price_end=false)
	{
		//if we are provided a category_id, then get products according to category
		if ($category_id)
		{
			//$result = $this->db->select('category_products.*')->from('category_products')->join('products', 'category_products.product_id=products.id')->where(array('category_id'=>$category_id, 'enabled'=>1))->order_by('sequence', 'ASC')->limit($limit)->offset($offset)->get()->result();
			
			$this->db->select('category_products.*')->from('category_products')->join('products', 'category_products.product_id=products.id')->where(array('category_id'=>$category_id, 'enabled'=>1));

			if(!empty($price_start))
			{
				$this->db->where("saleprice >= $price_start");
			}
			
			if(!empty($price_end))
			{
				$this->db->where("saleprice <= $price_end");
			}

			if(!empty($sort_by))
			{
				$this->db->order_by($sort_by, $sort_order);
			} else {
				$this->db->order_by('id', 'DESC');
			}

			//$this->db->order_by('id', 'DESC');
			$this->db->limit($limit)->offset($offset);
			
			$result = $this->db->get()->result();
			

			//$this->db->order_by('sequence', 'ASC');
			//$result	= $this->db->get_where('category_products', array('enabled'=>1,'category_id'=>$category_id), $limit, $offset);
			//$result	= $result->result();

			$contents	= array();
			$count		= 0;
			foreach ($result as $product)
			{

				$contents[$count]	= $this->get_product($product->product_id);
				$count++;
			}

			return $contents;
		}
		else
		{
			//sort by alphabetically by default
			//$this->db->where('enabled = 1 OR enabled = 0')->order_by('id', 'DESC');
			$this->db->where('enabled = 1 OR enabled = 0');

			if($limit>0)
			{
				$this->db->limit($limit, $offset);
			}
			if(!empty($sort_by))
			{
				$this->db->order_by($sort_by, $sort_order);
			}

			$result	= $this->db->get('products');
			//apply group discount
			$return = $result->result();
			if($this->group_discount_formula) 
			{
				foreach($return as &$product) {
					eval('$product->price=$product->price'.$this->group_discount_formula.';');
				}
			}
			return $return;
		}

	}

	function get_products_count($search=false)
	{			
		if ($search)
		{
			if(!empty($search->term))
			{
				//support multiple words
				$term = explode(' ', $search->term);

				foreach($term as $t)
				{
					$not		= '';
					$operator	= 'OR';
					if(substr($t,0,1) == '-')
					{
						$not		= 'NOT ';
						$operator	= 'AND';
						//trim the - sign off
						$t		= substr($t,1,strlen($t));
					}

					$like	= '';
					$like	.= "( `name` ".$not."LIKE '%".$t."%' " ;
					$like	.= $operator." `sku` ".$not."LIKE '%".$t."%'  ";
					$like	.= $operator." `description` ".$not."LIKE '%".$t."%' )";

					$this->db->where($like);
				}	
			}			
		}

		$this->db->where('enabled = 1 OR enabled = 0');
		
		return $this->db->count_all_results('products');
	}

	function get_sales_products($limit = false, $offset = false)
	{
		
			//sort by alphabetically by default
			$this->db->where('enabled = 1 AND promoprice != "0.00"' )->order_by('id', 'DESC');
			$result	= $this->db->get('products');
			//apply group discount
			$return = $result->result();
		
			return $return;
	}

	function get_suspense_products($category_id = false, $limit = false, $offset = false)
	{
		//if we are provided a category_id, then get products according to category
		if ($category_id)
		{
			//$result = $this->db->select('category_products.*')->from('category_products')->join('products', 'category_products.product_id=products.id')->where(array('category_id'=>$category_id, 'enabled'=>1))->order_by('sequence', 'ASC')->limit($limit)->offset($offset)->get()->result();
			
			$result = $this->db->select('category_products.*')->from('category_products')->join('products', 'category_products.product_id=products.id')->where(array('category_id'=>$category_id, 'enabled'=>2))->order_by('id', 'DESC')->limit($limit)->offset($offset)->get()->result();
			

			//$this->db->order_by('sequence', 'ASC');
			//$result	= $this->db->get_where('category_products', array('enabled'=>1,'category_id'=>$category_id), $limit, $offset);
			//$result	= $result->result();

			$contents	= array();
			$count		= 0;
			foreach ($result as $product)
			{

				$contents[$count]	= $this->get_product($product->product_id);
				$count++;
			}

			return $contents;
		}
		else
		{
			//sort by alphabetically by default
			$this->db->where(array('enabled'=>2))->order_by('name', 'ASC');
			$result	= $this->db->get('products');
			//apply group discount
			$return = $result->result();
			if($this->group_discount_formula) 
			{
				foreach($return as &$product) {
					eval('$product->price=$product->price'.$this->group_discount_formula.';');
				}
			}
			return $return;
		}

	}

	function get_promo_products($category_id = false, $limit = false, $offset = false)
	{
		//if we are provided a category_id, then get products according to category
		if ($category_id)
		{
			$result = $this->db->select('category_products.*')->from('category_products')->join('products', 'category_products.product_id=products.id')->where(array('category_id'=>$category_id, 'enabled'=>2))->order_by('id', 'DESC')->limit($limit)->offset($offset)->get()->result();
			
			$contents	= array();
			$count		= 0;
			foreach ($result as $product)
			{

				$contents[$count]	= $this->get_product($product->product_id);
				$count++;
			}

			return $contents;
		}
		else
		{
			//sort by alphabetically by default
			$this->db->where('enabled = 1 AND promoprice != "0.00"')->order_by('name', 'ASC');
			$result	= $this->db->get('products');
			//apply group discount
			$return = $result->result();
			if($this->group_discount_formula) 
			{
				foreach($return as &$product) {
					eval('$product->price=$product->price'.$this->group_discount_formula.';');
				}
			}
			return $return;
		}

	}

	function count_products($id)
	{
		return $this->db->select('product_id')->from('category_products')->join('products', 'category_products.product_id=products.id')->where(array('category_id'=>$id))->count_all_results();
	}

	function count_promo_products($id)
	{
		return $this->db->select('product_id')->from('category_products')->join('products', 'category_products.product_id=products.id')->where('promoprice != "0.00"')->where(array('category_id'=>$id, 'enabled'=>1))->count_all_results();
	}

	function count_taobao_products($id)
	{
		return $this->db->select('product_id')->from('category_products')->join('taobao', 'category_products.product_id=taobao.id')->where(array('category_id'=>$id))->where('is_saved = 0')->count_all_results();
	}

	function get_product($id, $sub=true)
	{
		$result	= $this->db->get_where('products', array('id'=>$id))->row();
		if(!$result)
		{
			return false;
		}
		
		$result->categories = $this->get_product_categories($result->id);
		
		// group discount?
		if($this->group_discount_formula) 
		{
			eval('$result->price=$result->price'.$this->group_discount_formula.';');
		}
		return $result;
	}

	function get_product_categories($id)
	{
		$cats	= $this->db->where('product_id', $id)->get('category_products')->result();
		
		$categories = array();
		foreach ($cats as $c)
		{
			$categories[] = $c->category_id;
		}
		return $categories;
	}

	function get_slug($id)
	{
		return $this->db->get_where('products', array('id'=>$id))->row()->slug;
	}

	function check_slug($str, $id=false)
	{
		$this->db->select('slug');
		$this->db->from('products');
		$this->db->where('slug', $str);
		if ($id)
		{
			$this->db->where('id !=', $id);
		}
		$count = $this->db->count_all_results();

		if ($count > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	function save($product, $options=false, $categories=false)
	{
		if ($product['id'])
		{
			$this->db->where('id', $product['id']);
			$this->db->update('products', $product);

			$id	= $product['id'];
		}
		else
		{
			$this->db->insert('products', $product);
			$id	= $this->db->insert_id();
		}

		//loop through the product options and add them to the db
		if($options !== false)
		{
			$obj =& get_instance();
			$obj->load->model('Option_model');

			// wipe the slate
			$obj->Option_model->clear_options($id);

			// save edited values
			$count = 1;
			foreach ($options as $option)
			{
				$values = $option['values'];
				unset($option['values']);
				$option['product_id'] = $id;
				$option['sequence'] = $count;

				$obj->Option_model->save_option($option, $values);
				$count++;
			}
		}
		
		if($categories !== false)
		{
			if($product['id'])
			{
				//get all the categories that the product is in
				$cats	= $this->get_product_categories($id);
				
				//eliminate categories that products are no longer in
				foreach($cats as $c)
				{
					if(!in_array($c, $categories))
					{
						$this->db->delete('category_products', array('product_id'=>$id,'category_id'=>$c));
					}
				}
				
				//add products to new categories
				foreach($categories as $c)
				{
					if(!in_array($c, $cats))
					{
						$this->db->insert('category_products', array('product_id'=>$id,'category_id'=>$c));
					}
				}
			}
			else
			{
				//new product add them all
				foreach($categories as $c)
				{
					$this->db->insert('category_products', array('product_id'=>$id,'category_id'=>$c));
				}
			}
		}
		
		//return the product id
		return $id;
	}

	function suspend($product)
	{
		if ($product['id'])
		{
			$this->db->where('id', $product['id']);
			$this->db->update('products', $product);

			$id	= $product['id'];
		}

		//return the product id
		return $id;
	}

	function update_stock($product)
	{
		if ($product['num_iid'])
		{
			date_default_timezone_set("Asia/Kuala_Lumpur");
			$this->db->where('num_iid', $product['num_iid']);
			$this->db->update('products', $product);

			$num_iid	= $product['num_iid'];
		}		
		
		return $num_iid;
	}
	
	function publish_product($id)
	{
		if ($id)
		{
			$product['enabled'] = 1;
			$this->db->where('id', $id);
			$this->db->update('products', $product);
		}

	}
	
	function delete_product($id)
	{
		// delete product 
		$this->db->where('id', $id);
		$this->db->delete('products');

		//delete references in the product to category table
		$this->db->where('product_id', $id);
		$this->db->delete('category_products');
		
		// delete coupon reference
		$this->db->where('product_id', $id);
		$this->db->delete('coupons_products');

	}

	function add_product_to_category($product_id, $optionlist_id, $sequence)
	{
		$this->db->insert('product_categories', array('product_id'=>$product_id, 'category_id'=>$category_id, 'sequence'=>$sequence));
	}

	function search_products($term, $limit=false, $offset=false)
	{
		$results		= array();

		//I know this is the round about way of doing things and is not the fastest. but it is thus far the easiest.

		//this one counts the total number for our pagination
		$this->db->like('name', $term);
		$this->db->or_like('description', $term);
		$this->db->or_like('excerpt', $term);
		$this->db->or_like('sku', $term);
		$results['count']	= $this->db->count_all_results('products');

		//this one gets just the ones we need.
		$this->db->like('name', $term);
		$this->db->or_like('description', $term);
		$this->db->or_like('excerpt', $term);
		$this->db->or_like('sku', $term);
		$results['products']	= $this->db->get('products', $limit, $offset)->result();
		return $results;
	}

	// Build a cart-ready product array
	function get_cart_ready_product($id, $quantity=false)
	{
		$db_product			= $this->get_product($id);
		if( ! $db_product)
		{
			return false;
		}
		
		$product = array();
		
		if ($db_product->saleprice == 0.00) { 
			$product['price']	= $db_product->price;
		}
		else
		{
			$product['price']	= $db_product->saleprice;
		}

		if ($db_product->promoprice != 0.00) { 
			$product['price']	= $db_product->promoprice;
		}
		
		$product['base_price'] 		= $product['price']; // price gets modified by options, show the baseline still...
		$product['id']				= $db_product->id;
		$product['name']			= $db_product->name;
		$product['sku']				= $db_product->sku;
		$product['excerpt']			= $db_product->excerpt;
		$product['weight']			= $db_product->weight;
		$product['shippable']	 	= $db_product->shippable;
		$product['taxable']			= $db_product->taxable;
		$product['fixed_quantity']	= $db_product->fixed_quantity;
		$product['track_stock']		= $db_product->track_stock;
		$product['options']			= array();
		
		// Some products have n/a quantity, such as downloadables	
		if (!$quantity || $quantity <= 0 || $db_product->fixed_quantity==1)
		{
			$product['quantity'] = 1;
		} else {
			$product['quantity'] = $quantity;
		}

		
		// attach list of associated downloadables
		$product['file_list']	= $this->Digital_Product_model->get_associations_by_product($id);
		
		return $product;
	}
	
	function get_product_price($id) {
		
		$this->db->select('price')->from('products')->where('id',$id);
		$result = $this->db->get()->row();
		
		return $result->price;
	}
}